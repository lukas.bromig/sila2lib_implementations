import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {
    title: 'BioREACTOR48 (2mag AG)',
    Img: {
        src: require('../../static/img/bioREACTOR48.png').default,
        src_alt: require('../../static/img/under_construction.svg').default,
        alt_text:'Test text',
        href: 'http://localhost:3001/sila2lib_implementations/docs/references/overview',
    },
    description: (
      <>
        The SiLA 2 driver for the BioREACTOR48 (<a href="https://www.2mag.de/en/">2mag AG</a>, Munich, Germany) is based on the SiLA 2 Python implementation v0.1. It
        is currently used in production in the DigInBio to perform to perform cultivations, where it is integrated into a
        liquid handling station. The BioREACTOR driver implements 2 specific features: DeviceService and MotorService.
        Since it uses SiLA 2 Python v0.1, no error handling is propagated via SiLA Errors in
        the current version of these features.
        <a href="https://www.2mag.de/en/">2mag AG</a>

      </>
    ),
  },
  {
    title: 'DO & pH fluorometric readers (PreSens GmbH)',
    Img: {
        src: null,
        src_alt: require('../../static/img/under_construction.svg').default,
        alt_text:'Test text',
        href: 'http://localhost:3001/sila2lib_implementations/docs/references/overview',
    },
    description: (
      <>
        The sensor bar driver is designed for the use with up to 6 sensor bars in parallel
        (<a href="https://www.presens.de/">PreSens GmbH</a>, Regensburg, Germany). We use them to measure pH and DO
        in our miniaturized bioreactors in the bioREACTOR48. The driver, based on SiLA 2 Python v0.1,  is used regularly
        in our demo lab. Apart from the standard features (SiLAService and SimulationController) it consists of the
        following features: CalibrationService, DeviceService, SensorProvider. Since it uses SiLA 2 Python v0.1, no
        error-handling is propagated via SiLA Errors in the current version of these features.
      </>
    ),
  },
  {
    title: 'BlueVary v2(BlueSens)',
    Img: {
        src: null,
        src_alt: require('../../static/img/under_construction.svg').default,
        alt_text:'Test text',
        href: 'http://localhost:3001/sila2lib_implementations/docs/references/overview',
    },
    description: (
      <>
        The BlueVary driver (v2) uses SiLA 2 Python v1.0 and is made up of a CalibrationService, DeviceService, and a
        SensorService. Error-handling has been implemented where appropriate and required. The BlueVary off-gas analytizer
        (<a href="https://www.bluesens.com/">BlueSens GmbH</a>, Herten, Germany) is regularly used to analyze bioreactor
        off-gas in our laboratory.
      </>
    ),
  },
  {
    title: 'RegloICC (Cole-Parmer)',
    Img: {
        src: null,
        src_alt: require('../../static/img/under_construction.svg').default,
        alt_text:'Test text',
        href: 'http://localhost:3001/sila2lib_implementations/docs/references/overview',
    },
    description: (
      <>
      RegloICC (<a href="http://www.ismatec.de/de_d/index.htm">Cole-Parmer GmbH</a>, Wertheim, Germany)
      </>
    ),
  },
  {
    title: 'RegloDC (Reglo)',
    Img: {
        src: null,
        src_alt: require('../../static/img/under_construction.svg').default,
        alt_text:'Test text',
        href: 'http://localhost:3001/sila2lib_implementations/docs/references/overview',
    },
    description: (
      <>
        RegloICC (<a href="http://www.ismatec.de/de_d/index.htm">Cole-Parmer GmbH</a>, Wertheim, Germany)
      </>
    ),
  },
  {
    title: 'Viper SW (Mettler Toledo)',
    Img: {
        src: null,
        src_alt: require('../../static/img/under_construction.svg').default,
        alt_text:'Test text',
        href: 'http://localhost:3001/sila2lib_implementations/docs/references/overview',
    },
    description: (
      <>
        Viper SW (<a href="https://www.mt.com/de/de/home.html">Mettler-Toledo GmbH</a>, Gießen, Germany)
      </>
    ),
  },
  {
    title: 'DASGIP parallel bioreactor system (Eppendorf GmbH)',
    Img: {
        src: null,
        src_alt: require('../../static/img/under_construction.svg').default,
        alt_text:'Test text',
        href: 'http://localhost:3001/sila2lib_implementations/docs/references/overview',
    },
    description: (
      <>
        DASGIP parallel bioreactor system (<a href="https://www.eppendorf.com">Eppendorf SE</a>, Hamburg, Germany)
        --> OPC-UA wrapper
      </>
    ),
   },
   {
    title: 'EL-Flow Prestige (Bronkhorst)',
    Img: {
        src: null,
        src_alt: require('../../static/img/under_construction.svg').default,
        alt_text:'Test text',
        href: 'http://localhost:3001/sila2lib_implementations/docs/references/overview',
    },
    description: (
      <>
        EL-FLOW Prestige (<a href="https://www.bronkhorst.com">Bronkhorst GmbH SE</a>, Kamen, Germany)
      </>
    ),
  },
];

//<!-- <Svg className={styles.featureSvg} alt={title} /> -->
// <img  src={Img.src} alt={Img.alt_text} href={Img.href}/>
// <Svg className={styles.featureSvg} alt={title} />
function Feature({Img, title, description}) {
    if (Img.src != null) {
        return (
        <div className={clsx('col col--4')}>
          <div className="text--center">
            <a href={Img.href}>
                <img  src={Img.src} alt={Img.alt_text} href={Img.href}/>
            </a>
          </div>
          <div className="text--center padding-horiz--md">
            <h3>{title}</h3>
            <p>{description}</p>
          </div>
        </div>
      );
    } else {
    return (
        <div className={clsx('col col--4')}>
          <div className="text--center">
            <a href={Img.href}>
                <Img.src_alt className={styles.featureSvg} alt={title} />
            </a>
          </div>
          <div className="text--center padding-horiz--md">
            <h3>{title}</h3>
            <p>{description}</p>
          </div>
        </div>
      );
    }

}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}

---
sidebar_position: 1
---

# Overview
Here we will describe each driver individually...

| **Device**      | **Kind**             | **SiLA Python branch** | **State** | **Last update (main)** |
|-----------------|----------------------|------------------------|-----------|-----------------------------|
| BioREACTOR48    | mL-Bioreactor system | master (v.1.0)         | in use    |          20.08.2021         |
| BlueVary_v2     | off-gas analytic     | codegenerator-0.3      | in use    |          23.07.2021         |
| DASGIP          | L-Bioreactor system  | master (v.1.0)         | in use    |          26.04.2021         |
| ELFLOW Prestige | flowmeter            | codegenerator-0.3      | testing   |          30.08.2021         |
| Lauda Loop250   | thermostat           | master (v.1.0)         | not used  |          25.10.2020         |
| MT Viper SW     | laboratory balance   | codegenerator-0.3      | in use    |          14.04.2021         |
| Presens         | pH/DO-sensor bars    | codegenerator-0.3      | in use    |          20.08.2021         |
| RegloDC         | peristaltic pump     | codegenerator-0.3      | in use    |          14.04.2021         |
| RegloICC        | peristaltic pump     | master (v.1.0)         | in use    |          12.03.2021         |
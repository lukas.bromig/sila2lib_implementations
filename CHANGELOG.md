## 0.0.1 (2020-11-09)

### Feat

- add support for Raspberry Pi OS Buster
- add git version tags
- implement devices (new sila): 
BlueVary Offgas-analytics
Reglo ICC peristaltic pump
- implement devices (old sila): 
BlueVary Offgas-analytics
DASGIP 4xParallel Reactor System
LAUDA LOOP 250
LAUDA LOOP 500
Presens sensor bars
Reglo ICC peristaltic pump
Flowmeter Bronkhorst
- add serial connection

### Fix


### Docs
- add documentation
- adapt to corporate design

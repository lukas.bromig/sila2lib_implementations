"""
This script is used to control the aeration of a cultivation in a DASGIP 4x bioreactor system with an external
flowmeter.
Author: lukas.bromig@tum.de
"""
from time import sleep
from datetime import datetime

from sila2lib.framework import SiLAFramework_pb2 as silaFW_pb2

from sila2lib_implementations.DASGIP.DASGIPService.DASGIP_Service_client import DASGIP_ServiceClient
from sila2lib_implementations.Flowmeter_ELFLOWPrestige.FlowmeterElflowPrestigeService.FlowmeterElflowPrestigeService_client import FlowmeterElflowPrestigeServiceClient
from sila2lib_implementations.Flowmeter_ELFLOWPrestige.FlowmeterElflowPrestigeService.MeasurementService.gRPC import MeasurementService_pb2
from sila2lib_implementations.Flowmeter_ELFLOWPrestige.FlowmeterElflowPrestigeService.MeasurementService.gRPC import MeasurementService_pb2_grpc


def timestamp_to_datetime(timestamp):
    return datetime(
        year=timestamp.year,
        month=timestamp.month,
        day=timestamp.day,
        hour=timestamp.hour,
        minute=timestamp.minute,
        second=timestamp.second
    )


def lnmin_to_percent(lnmin):
    return round(lnmin*14.2857142857,1)


def percent_to_lnmin(percent):
    return round(percent*0.07,2)

print('Validate settings:')
DASGIP_UNIT = 0     # unit
LOOP_INTERVAL = 3  # SAMPLE_TIME in seconds
FLOW_INCREMENT = lnmin_to_percent(0.14)  # L/min
MAX_FLOW = lnmin_to_percent(1.4)  # L/min
print(f'Flow Increment: {FLOW_INCREMENT}')
print(f'Max Flow: {MAX_FLOW}')


print('\nEstablishing device connection:')
dasgip = DASGIP_ServiceClient(server_ip='127.0.0.1', server_port=50005)
flowmeter = FlowmeterElflowPrestigeServiceClient(server_ip='127.0.0.1', server_port=50004)
protocol = flowmeter.SetCommunicationProtocolAscii().CurrentCommunicationProtocolAscii.value
print(f'Setting flowmeter communication protocol to {protocol}')


# Dasgip commands

# started = dasgip.DeviceServicer_GetRuntimeClock(UnitID=1).CurrentRuntime
# print(started)
inoculated = timestamp_to_datetime(dasgip.DeviceServicer_GetInoculated(UnitID=DASGIP_UNIT).CurrentInoculation)
not_inoculated_default_response = datetime(year=1601, month=1, day=1, hour=0, minute=0, second=0)
# not_inoculated_response = 
# inoculated = datetime(year=2022, month=2, day=3, hour=13, minute=28, second=0)
# print('inoculated: ', inoculated)
# if inoculated == not_inoculated_default_response:
#     print('Default response detected!')
# else:
#     print('Default not detected!')

"""
inoculated_utc = timestamp_to_datetime(dasgip.DeviceServicer_GetInoculatedUTC(UnitID=DASGIP_UNIT).CurrentInoculationUTC)
print('inoculated_utc: ', inoculated_utc)
"""

print('\nGetting initial values from devices:')
do_pv = dasgip.DOServicer_GetPV(UnitID=DASGIP_UNIT).CurrentPV.value
print('do_pv: ', do_pv)

# Flowmeter commands
flow_pv = flowmeter.GetMeasuredValue().CurrentMeasuredValue
print(f'flow_pv: {percent_to_lnmin(flow_pv.value)} ln/min')
flow_unit = flowmeter.GetCapacityUnit().CurrentCapacityUnit.value
print('flow_unit: ', flow_unit)
flow_sp = flowmeter.GetSetpointValue().GetSetpointValue
print(f'flow_sp:  {percent_to_lnmin(flow_sp.value)} ln/min, {flow_sp.value} %')
version = flowmeter.GetFirmwareVersion().CurrentFirmwareVersion.value
print('Firmware_version', version)

"""
t=0
while True:
    if t<= 10:
        # flowmeter.SetSetpointValue(10)
        flowmeter.SetSetpointValue(parameter=MeasurementService_pb2.SetSetpointValue_Parameters(
    SetSetpointValue=silaFW_pb2.Real(value=float(10)))).Status
        print('10: #################################')
    elif t<=30:
        flow_sp_status = flowmeter.SetSetpointValue(parameter=MeasurementService_pb2.SetSetpointValue_Parameters(SetSetpointValue=silaFW_pb2.Real(value=float(20)))).Status
        print('20: #################################')
    elif t<=50:
        flow_sp_status = flowmeter.SetSetpointValue(parameter=MeasurementService_pb2.SetSetpointValue_Parameters(SetSetpointValue=silaFW_pb2.Real(value=float(30)))).Status
        print('30: #################################')
    elif t<=70: 
        flow_sp_status = flowmeter.SetSetpointValue(parameter=MeasurementService_pb2.SetSetpointValue_Parameters(SetSetpointValue=silaFW_pb2.Real(value=float(40)))).Status
        print('40: #################################')
    
    
    print(flowmeter.GetMeasuredValue().CurrentMeasuredValue)
    sleep(.2)
    #  print(flowmeter.GetSetpointValue().GetSetpointValue)
    sleep(.2)
    # print(flowmeter.GetValveOutput().CurrentValveOutput)
    sleep(.2)
    #  print(flowmeter.GetCapacity100().CurrentCapacity100)
    print('_____________________________________________\n')
    sleep(2)
    t=t+2.6
"""



print('\n\n###   1. Resetting flowmeter to 0 flow   ###')
print('____________________________________________\n')
flow_sp_status = flowmeter.SetSetpointValue(parameter=MeasurementService_pb2.SetSetpointValue_Parameters(
    SetSetpointValue=silaFW_pb2.Real(value=float(0)))).Status
print('flow_sp_status: ', flow_sp_status)
for i in range(3):
    sleep(1)
    print(f'Current value: {percent_to_lnmin(flowmeter.GetMeasuredValue().CurrentMeasuredValue.value)}')


print('\n###   2. Wait until inoculated   ###')
print('____________________________________\n')
while True:
    inoculated = timestamp_to_datetime(dasgip.DeviceServicer_GetInoculated(UnitID=DASGIP_UNIT).CurrentInoculation)
    #  inoculated = datetime(year=2022, month=2, day=3, hour=9, minute=15, second=0)
    # How to differentiate between not inoculated and actual inoculation time
    print(f'Inoculated is: {inoculated}')
    if inoculated != not_inoculated_default_response:  # If inoculated, break out of loop
        break
    else:
        print(f'{datetime.now()}: Not inoculated yet!')
              #f'{(datetime.now() - inoculated).total_seconds():.2f} seconds')
        sleep(5)


print('\n###   3. During the first 6h after inoculation: Wait   ###')
print('__________________________________________________________\n')
while (datetime.now()-inoculated).total_seconds() < 900:  #  6*3600:
    print(f'Awaiting spore loss phase (6h)! Time delta is: '
          f'{(datetime.now() - inoculated).total_seconds():.2f} seconds')
    sleep(30)

print('\n###   4. After the first 6 h: Set flow to 8.4 L/h   ###')
print('_______________________________________________________\n')
flow_sp = FLOW_INCREMENT  # 0.14
flow_sp_status = flowmeter.SetSetpointValue(parameter=MeasurementService_pb2.SetSetpointValue_Parameters(
    SetSetpointValue=silaFW_pb2.Real(value=float(flow_sp)))).Status.value
print('Set flow operation: ', flow_sp_status)
print(f'Flow set to: {percent_to_lnmin(flow_sp)} ln/min')

print('\n###   5. After the first 6 h: frequently check DO and adjust flow rate   ###')
print('____________________________________________________________________________\n')
t = 0
while True:
    sleep(LOOP_INTERVAL)
    do_pv = dasgip.DOServicer_GetPV(UnitID=DASGIP_UNIT).CurrentPV.value
    print(f'Current DO: {do_pv}')
    current_flow_pv = flowmeter.GetMeasuredValue().CurrentMeasuredValue
    print(f'Current flow: {percent_to_lnmin(current_flow_pv.value)} ln/min')
    # current_set_flow_sp = flowmeter.GetSetpointValue().GetSetpointValue.value
    if do_pv < 30 and flow_sp <= MAX_FLOW:  # and (datetime.now()-inoculated).total_seconds() >= 3600:
        old_flow_sp = flow_sp
        if (MAX_FLOW - flow_sp) < FLOW_INCREMENT:
            flow_sp = MAX_FLOW
        else:
            flow_sp = flow_sp + FLOW_INCREMENT

        flow_sp_status = flowmeter.SetSetpointValue(parameter=MeasurementService_pb2.SetSetpointValue_Parameters(
            SetSetpointValue=silaFW_pb2.Real(value=float(flow_sp)))).Status.value
        print(f'Set flow status: {flow_sp_status}')
        print(f'Elevated air flow: {old_flow_sp}% --> {flow_sp}%')
        print('---------------- increase flow ------------->\n')

    # Keep alive print message
    if (t % 900) == 0:
        print(f'{datetime.now()}: Current flow setpoint is {percent_to_lnmin(flow_sp)} ln/min\n')
    # sleep(LOOP_INTERVAL)
    t = t + LOOP_INTERVAL


import logging
from time import sleep
import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication


import RegloICC
import sila2lib_implementations.MT_Viper_SW.MT_Viper_SW_Balance_Service.MT_Viper_SW_Balance_Service_client as MTViperClient


logger = logging.getLogger(__name__)
# in g/mL -> 1000 kg/m^3
FLUID_DENSITY = 1
PRE_PUMP_VOLUME = 20

pump_client = RegloICC.Client(address='10.152.248.1', port=50052, insecure=True)

def adjust_pump_settings(pump_client):
    is_channel_addressing = pump_client.DeviceService.GetChannelAddressing(Address=1).ChannelAddressing
    if not is_channel_addressing:
        print(f'{"Channel addressing is deactivated"}')
        channel_addressing_set = pump_client.DeviceService.SetChannelAddressing(
            Address=1, ChannelAddressing=True).ChannelAddressingSet
        print(f'Pump set to channel addressing mode. Response: {channel_addressing_set}')
    is_event_messages = pump_client.DeviceService.GetEventMessages(Address=1).EventMessages
    if is_event_messages:
        event_messages_set = pump_client.DeviceService.SetEventMessages(Address=1, EventMessages=False).EventMessagesSet
        print(f'Event messages are enabled. Disabled event messages: {event_messages_set}')
        is_event_messages = pump_client.DeviceService.GetEventMessages(Address=1).EventMessages
        print(f'{"Event messages disabled" if not is_event_messages else "Event messages not disabled!"}')


# First specify the volume of fluid to dispense for calibration, the dispense time for the specified volume, and
# the pumping direction.


def calibrate_pump(pump_client, balance_client, channel: int, settings: dict):
    for i in range(len(settings['volume'])):
        pump_client.CalibrationService.SetDirectionCalibration(Channel=channel, Direction=settings['direction'])
        pump_client.CalibrationService.SetCalibrationTime(Channel=channel, Time=settings['time'][i])

        maximum_flow_rate_wc = pump_client.ParameterController.GetMaximumFlowRateWithCalibration(
            Channel=channel).MaximumFlowRateWithCalibration
        print(f'Maximum calibration flow rate is: {maximum_flow_rate_wc} ml/min')
        cal_flow_rate = settings['volume'][i] / (settings['time'][i] / 60)  # ml/min
        print(f'Calibration flow rate set to: {cal_flow_rate} ml/min')
        if cal_flow_rate > maximum_flow_rate_wc:
            print('Calibration flow rate is exceeding maximum calibration flowrate!')
            settings['volume'][i] = maximum_flow_rate_wc * (settings['time'][i] / 60)
            print(f'Changed calibration volume to {settings["volume"][i]} '
                  f'at maximum calibration flow rate of {maximum_flow_rate_wc} ')

        pump_client.CalibrationService.SetTargetVolume(Channel=channel, TargetVolume=settings['volume'][i])

        current_run_time_calibration = \
            pump_client.CalibrationService.GetRunTimeCalibration(Channel=channel).CurrentRunTimeCalibration
        current_direction_calibration = \
            pump_client.CalibrationService.GetDirectionCalibration(Channel=channel).CurrentDirectionCalibration
        current_target_volume = pump_client.CalibrationService.GetTargetVolume(Channel=channel).CurrentTargetVolume
        current_calibration_time = pump_client.CalibrationService.GetCalibrationTime(Channel=channel).CurrentCalibrationTime
        print(f'CurrentRunTimeCalibration: {current_run_time_calibration}\n'
              f'CurrentDirectionCalibration: {current_direction_calibration}\n'
              f'CurrentTargetVolume: {current_target_volume}\n'
              f'CurrentCalibrationTime: {current_calibration_time}')

        pump_client.DriveController.StopPump(Channel=channel)
        pump_client.CalibrationService.CancelCalibration(Channel=channel)
        print(f'Current pump status: {pump_client.DeviceService.GetPumpStatus(Address=channel)}')
        pump_client.CalibrationService.StartCalibration(Channel=channel)

        while True:
            current_pump_status = pump_client.DeviceService.GetPumpStatus(Address=channel).CurrentPumpStatus
            if current_pump_status == '+':
                sleep(1)
            else:
                print('Finished pump step of calibration procedure')
                break

        # Measure actual volume
        measured_volume = get_weight(client=balance_client)  # balance_client.balanceService_client.Get_StableWeightValue()
        actual_volume = measured_volume/FLUID_DENSITY
        print(f'Got stable weight of {measured_volume} g from balance ({actual_volume} ml)')

        # Set actual volume
        pump_client.CalibrationService.SetActualVolume(Channel=channel, ActualVolume=actual_volume)
        settings['actual_volume'].append(actual_volume)
        print(f'Successfully calibrated channel {channel}, calibration point {i+1}\n'
              f'__________________________________________________________________\n')
        return settings


def calculate_calibration(velocity: float = 0, volume: float = 0, old_weight: float = 0, new_weight: float = 0):
    delta_weight = old_weight - new_weight
    if delta_weight == 0:
        new_velocity = velocity
    else:
        new_velocity = round(velocity * volume / delta_weight)
    return new_velocity


def get_weight(client):
    """ Get the stable weight value of a laboratory balance of type MT Viper SW using SiLA 2 in g"""
    sleep(1)  # Wait to get stable weight value
    weight = None
    while True:
        try:
            weight = client.balanceService_client.Get_StableWeightValue().StableWeightValue.value
            break
        except Exception as e:
            logging.exception(e)
            logging.error(f'ValueError encountered')
            # Wait 5 seconds before retry
            sleep(5)
            continue
    return weight


def pre_pump(pump_client, channels: list, direction: str, volume: int):
    logger.info('Pre-pumping liquid...')
    _rpm = 200
    _duration = 10
    for channel in channels:
        pump_client.ParameterController.SetRPMMode(Channel=channel)
        flow_rate = pump_client.ParameterController.GetMaximumFlowRateWithCalibration(Channel=channel).MaximumFlowRateWithCalibration
        # The channel is set to Volume at Rate mode, which allows to dispense a set volume with a set flow rate

        if pump_client.ParameterController.GetMode(Channel=channel).CurrentPumpMode != 'O':
            pump_client.ParameterController.SetVolumeRateMode(Channel=channel)
            # The volume to dispense is set according to the setting in the head of the function (in mL)
        pump_client.ParameterController.SetVolume(Channel=channel, Volume=volume)

        # The flow rate is set according to the setting in the head of the function (in mL/min). Must be LESS or
        # EQUAL to the max. flow rate of the channel
        pump_client.ParameterController.SetFlowRate(Channel=channel, SetFlowRate=flow_rate)

        # Gets the current rotation direction of the channel and compares to the desired setting. If the settings
        # do not match, the sets it to counter-clockwise ...
        current_pump_direction = pump_client.DriveController.GetPumpDirection(Channel=channel).PumpDirection
        if current_pump_direction != direction:
            if direction == 'K':
                pump_client.DriveController.SetDirectionCounterClockwise(Channel=channel)
            elif direction == 'J':
                pump_client.DriveController.SetDirectionClockwise(Channel=channel)
            else:
                print('Direction not recognised. Defaulting to clockwise rotation [K]!')
                pump_client.DriveController.SetDirectionClockwise(Channel=channel)

        # Starts pumping. The channel will stop as soon as the volume is dispensed
        pump_client.DriveController.StartPump(Channel=channel)
        print(f'Volume at Rate mode on channel {channel} for volume {volume} mL at rate {flow_rate} mL/min set.')
    while True:
        channel_status = []
        for channel in channels:
            channel_status.append(pump_client.DeviceService.GetPumpStatus(Address=channel).CurrentPumpStatus)
        if '+' in channel_status:
            busy_channels = [i+1 for i, x in enumerate(channel_status) if x == '+']
            print(f'Channel(s) {busy_channels} are still pumping.', end='\r')
            sleep(1)
        else:
            print('Finished pre-pump step of calibration procedure')
            break
    print(pump_client.DeviceService.GetPumpStatus(Address=1).CurrentPumpStatus)


def run():
    pump_client = RegloICC.Client(address='10.152.248.1', port=50052, insecure=True)
    balance_client = MTViperClient.MT_Viper_SW_Balance_ServiceClient(server_ip="10.152.248.1", server_port=50001)

    calibration_settings: dict = {
        # 1: {'direction': 'J', 'time': [60, 120], 'volume': [3, 6]},
        1: {'direction': 'J', 'time': [30], 'volume': [1], 'actual_volume': []},
        2: {'direction': 'J', 'time': [30], 'volume': [1], 'actual_volume': []},
        3: {'direction': 'J', 'time': [30], 'volume': [1], 'actual_volume': []},
        4: {'direction': 'J', 'time': [30], 'volume': [1], 'actual_volume': []},
    }
    # Disable panel on device to avoid user interference
    pump_client.DeviceService.SetDisableInterface(Address=1)

    # Configure settings for remote control
    adjust_pump_settings(pump_client=pump_client)

    # Pre-pump liquid to fill hose
    pre_pump(pump_client=pump_client, channels=list(calibration_settings.keys()), direction='J', volume=PRE_PUMP_VOLUME)

    # Run the calibration
    for channel, settings in calibration_settings.items():
        settings = calibrate_pump(pump_client=pump_client, balance_client=balance_client, channel=channel, settings=settings)
        print(settings)
        calibration_settings[channel] = settings

    # Enable panel on device for user interaction
    pump_client.DeviceService.SetUserInterface(Address=1)

    return calibration_settings

if __name__ == '__main__':
    calibration_settings = run()
    print(calibration_settings)

    # adjust_pump_settings(pump_client=pump_client)
    # pump_client.CalibrationService.CancelCalibration(Channel=1)
    # print(pump_client.DeviceService.GetPumpStatus(Address=1))
    #pump_client.CalibrationService.SetTargetVolume(Channel=1, TargetVolume=3)
    # pump_client.CalibrationService.SetCalibrationTime(Channel=1, Time=60)
    #pump_client.CalibrationService.SetDirectionCalibration(Channel=1, Direction='J')


    # print(pump_client.ParameterController.GetMaximumFlowRate(Channel=1))

    # sleep(2)
    #pump_client.CalibrationService.StartCalibration(Channel=1)
    # pump_client.CalibrationService.SetActualVolume(Channel=1, ActualVolume=10)
    #pump_client.DriveController.StartPump(Channel=1)
    #pump_client.DriveController.StopPump(Channel=1)

#!/bin/bash

# This is an example bash script for the start of the DASGIP server

echo "Executing bash script for DASGIP SiLA 2 Server start-up"

cd /home/pi/Desktop/sila2lib_implementations/sila2lib_implementations/DASGIP/DASGIPService
/home/pi/.local/bin/poetry run python3 DASGIP_Service_server.py -a "10.152.248.151" -p 50100 -n "DIB-DASGIP"

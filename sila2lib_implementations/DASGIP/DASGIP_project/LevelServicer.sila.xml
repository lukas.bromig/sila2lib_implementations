<?xml version="1.0" encoding="utf-8" ?>
<Feature SiLA2Version="1.0" FeatureVersion="1.0" Originator="org.silastandard" Category="examples"
         xmlns="http://www.sila-standard.org"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://www.sila-standard.org https://gitlab.com/SiLA2/sila_features/raw/master/schema/FeatureDefinition.xsd">
    <Identifier>LevelServicer</Identifier>
    <DisplayName>Level Servicer</DisplayName>
    <Description>
        Control a DASGIP level module. Enables read and write operations for various parameters, including level sensor and alarm. 
        By Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 20.05.2019
    </Description>
    <Command>
        <Identifier>GetPV</Identifier>
        <DisplayName>Get PV</DisplayName>
        <Description>Get present value</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentPV</Identifier>
            <DisplayName>Current PV</DisplayName>
            <Description>Current present value</Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetType</Identifier>
        <DisplayName>Get Function Type</DisplayName>
        <Description>Get function type.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentType</Identifier>
            <DisplayName>Current Function Type</DisplayName>
            <Description>Current function type value of the level module.</Description>
            <DataType>
                <Basic>String</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetAvailable</Identifier>
        <DisplayName>Get Function Availability</DisplayName>
        <Description>Get function availability.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentAvailable</Identifier>
            <DisplayName>Current Function Availability</DisplayName>
            <Description>Current function availability value of the level module.</Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetName</Identifier>
        <DisplayName>Get Function Name</DisplayName>
        <Description>Get function name.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentName</Identifier>
            <DisplayName>Current Function Name</DisplayName>
            <Description>Current function name of the level module.</Description>
            <DataType>
                <Basic>String</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetVersion</Identifier>
        <DisplayName>Get Function Version</DisplayName>
        <Description>Get function model version number.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentVersion</Identifier>
            <DisplayName>Current Function Version</DisplayName>
            <Description>Current function model version number of the level module.</Description>
            <DataType>
                <Basic>String</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetSensorPVRaw</Identifier>
        <DisplayName>Get Sensor PVRaw</DisplayName>
        <Description>Get the sensor present raw value. Actual process raw value.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentSensorPVRaw</Identifier>
            <DisplayName>Current Sensor PVRaw</DisplayName>
            <Description>Current sensor present raw value of the level module. Actual process raw value.</Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>SetSensorSlope</Identifier>
        <DisplayName>Set Sensor Slope</DisplayName>
        <Description>
        Set the sensor slope value. Sensor slope calibration parameter.
        </Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Parameter>
            <Identifier>SensorSlope</Identifier>
            <DisplayName>Set Sensor Slope</DisplayName>
            <Description>
                The sensor slope value. Sensor slope calibration parameter.
            </Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>SensorSlopeSet</Identifier>
            <DisplayName>Sensor Slope Set</DisplayName>
            <Description>The set sensor slope value. Sensor slope calibration parameter.</Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetSensorSlope</Identifier>
        <DisplayName>Get Sensor Slope</DisplayName>
        <Description>Get the sensor slope value. Sensor slope calibration parameter.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentSensorSlope</Identifier>
            <DisplayName>Current Sensor Slope</DisplayName>
            <Description>Current sensor slope value of the level module. Sensor slope calibration parameter.</Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>SetAlarmEnabled</Identifier>
        <DisplayName>Set Alarm Enabled</DisplayName>
        <Description>
        Set the alarm enabled value. Enables alarm function.
        </Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Parameter>
            <Identifier>AlarmEnabled</Identifier>
            <DisplayName>Set Alarm Enabled</DisplayName>
            <Description>
                The alarm enabled value. Enables alarm function.
            </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                    <Constraints>
                        <MaximalInclusive>1</MaximalInclusive>
                        <MinimalExclusive>0</MinimalExclusive>
                    </Constraints>
                </Constrained>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>AlarmEnabledSet</Identifier>
            <DisplayName>Alarm Enabled Set</DisplayName>
            <Description>The set alarm enabled value. Enables alarm function. </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Response>
        <DefinedExecutionErrors>
            <Identifier>ParameterInvalidRange</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <Command>
        <Identifier>GetAlarmEnabled</Identifier>
        <DisplayName>Get Alarm enabled</DisplayName>
        <Description>Get the alarm enabled value. Enables alarm function.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentAlarmEnabled</Identifier>
            <DisplayName>Current Alarm Enabled</DisplayName>
            <Description>Current alarm enabled value of the level module. Enables alarm function. </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>SetAlarmAlarmHigh</Identifier>
        <DisplayName>Set Alarm AlarmHigh</DisplayName>
        <Description>
        Set the alarm alarmhigh value. Higher alarm limit.
        </Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Parameter>
            <Identifier>AlarmAlarmHigh</Identifier>
            <DisplayName>Set Alarm AlarmHigh</DisplayName>
            <Description>
                The alarm alarmhigh value. Higher alarm limit.
            </Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>AlarmAlarmHighSet</Identifier>
            <DisplayName>Alarm AlarmHigh Set</DisplayName>
            <Description>The set alarm alarmhigh value. Higher alarm limit. </Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetAlarmAlarmHigh</Identifier>
        <DisplayName>Get Alarm AlarmHigh</DisplayName>
        <Description>Get the alarm alarmhigh value. Higher alarm limit.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentAlarmAlarmHigh</Identifier>
            <DisplayName>Current Alarm AlarmHigh</DisplayName>
            <Description>Current alarm alarmhigh value of the level module. Higher alarm limit. </Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>SetAlarmAlarmLow</Identifier>
        <DisplayName>Set Alarm AlarmLow</DisplayName>
        <Description>
        Set the alarm alarmlow value. Lower alarm limit.
        </Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Parameter>
            <Identifier>AlarmAlarmLow</Identifier>
            <DisplayName>Set Alarm AlarmLow</DisplayName>
            <Description>
                The alarm alarmlow value. Lower alarm limit.
            </Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>AlarmAlarmLowSet</Identifier>
            <DisplayName>Alarm AlarmLow Set</DisplayName>
            <Description>The set alarm alarmlow value. Lower alarm limit. </Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetAlarmAlarmLow</Identifier>
        <DisplayName>Get Alarm AlarmLow</DisplayName>
        <Description>Get the alarm alarmlow value. Lower alarm limit.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentAlarmAlarmLow</Identifier>
            <DisplayName>Current Alarm AlarmLow</DisplayName>
            <Description>Current alarm alarmlow value of the level module. Lower alarm limit. </Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>SetAlarmMode</Identifier>
        <DisplayName>Set Alarm Mode</DisplayName>
        <Description>
        Set the alarm mode value. Alarm mode.
        </Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Parameter>
            <Identifier>AlarmMode</Identifier>
            <DisplayName>Set Alarm Mode</DisplayName>
            <Description>
                The alarm mode value. Alarm mode.
            </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                    <Constraints>
                        <MaximalInclusive>1</MaximalInclusive>
                        <MinimalExclusive>0</MinimalExclusive>
                    </Constraints>
                </Constrained>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>AlarmModeSet</Identifier>
            <DisplayName>Alarm Mode Set</DisplayName>
            <Description>The set alarm mode value. Alarm mode. </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Response>
        <DefinedExecutionErrors>
            <Identifier>ParameterInvalidRange</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <Command>
        <Identifier>GetAlarmMode</Identifier>
        <DisplayName>Get Alarm Mode</DisplayName>
        <Description>Get the alarm mode value. Alarm mode.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentAlarmMode</Identifier>
            <DisplayName>Current Alarm Mode</DisplayName>
            <Description>Current alarm mode value of the level module. Alarm mode. </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetAlarmState</Identifier>
        <DisplayName>Get Alarm State</DisplayName>
        <Description>Get the alarm state value. Alarm state.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentAlarmState</Identifier>
            <DisplayName>Current Alarm State</DisplayName>
            <Description>Current alarm state value of the level module. Alarm state.</Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>SetAlarmDelay</Identifier>
        <DisplayName>Set Alarm Delay</DisplayName>
        <Description>
        Set the alarm delay value. Alarm hysteresis time.
        </Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Parameter>
            <Identifier>AlarmDelay</Identifier>
            <DisplayName>Set Alarm Delay</DisplayName>
            <Description>
                The alarm delay value. Alarm hysteresis time.
            </Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>AlarmDelaySet</Identifier>
            <DisplayName>Alarm Delay Set</DisplayName>
            <Description>The set alarm delay value. Alarm hysteresis time. </Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetAlarmDelay</Identifier>
        <DisplayName>Get Alarm Delay</DisplayName>
        <Description>Get the alarm delay value.Alarm hysteresis time.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentAlarmDelay</Identifier>
            <DisplayName>Current Alarm Delay</DisplayName>
            <Description>Current alarm delay value of the level module. Alarm hysteresis time.</Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>SetAlarmWarnHigh</Identifier>
        <DisplayName>Set Alarm WarnHigh</DisplayName>
        <Description>
        Set the alarm higher warning value. Higher warning limit.
        </Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Parameter>
            <Identifier>AlarmWarnHigh</Identifier>
            <DisplayName>Set Alarm WarnHigh</DisplayName>
            <Description>
                The alarm higher warning value. Higher warning limit.
            </Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>AlarmWarnHighSet</Identifier>
            <DisplayName>Alarm WarnHigh Set</DisplayName>
            <Description>The set alarm higher warning value. Higher warning limit. </Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetAlarmWarnHigh</Identifier>
        <DisplayName>Get Alarm WarnHigh</DisplayName>
        <Description>Get the alarm higher warning value. Higher warning limit.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentAlarmWarnHigh</Identifier>
            <DisplayName>Current Alarm WarnHigh</DisplayName>
            <Description>Current alarm higher warning value of the level module. Higher warning limit.</Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>SetAlarmWarnLow</Identifier>
        <DisplayName>Set Alarm WarnLow</DisplayName>
        <Description>
        Set the alarm lower warning value. Lower warning limit.
        </Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Parameter>
            <Identifier>AlarmWarnLow</Identifier>
            <DisplayName>Set Alarm WarnLow</DisplayName>
            <Description>
                The alarm lower warning value. Lower warning limit.
            </Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>AlarmWarnLowSet</Identifier>
            <DisplayName>Alarm WarnLow Set</DisplayName>
            <Description>The set alarm lower warning value. Lower warning limit. </Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetAlarmWarnLow</Identifier>
        <DisplayName>Get Alarm WarnLow</DisplayName>
        <Description>Get the alarm lower warning value. Lower warning limit.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentAlarmWarnLow</Identifier>
            <DisplayName>Current Alarm WarnLow</DisplayName>
            <Description>Current alarm lower warning value of the level module. Lower warning limit.</Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
    </Command>
    <DefinedExecutionError>
        <Identifier>ParameterInvalidRange</Identifier>
        <DisplayName>Parameter Invalid Range</DisplayName>
        <Description>Invalid range of the input parameter</Description>
    </DefinedExecutionError>
</Feature>

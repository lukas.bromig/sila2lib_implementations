"""
________________________________________________________________________

:PROJECT: SiLA2_python

*Level Servicer*

:details: LevelServicer:
    Control a DASGIP level module. Enables read and write operations for various parameters, including level sensor and
    alarm.
    By Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 20.05.2019

:file:    LevelServicer_real.py
:authors: Lukas Bromig

:date: (creation)          2020-04-16T10:19:14.176899
:date: (last modification) 2020-04-16T10:19:14.176899

.. note:: Code generated by sila2codegenerator 0.2.0

________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "0.0.1"

# import general packages
import logging
import time         # used for observables
import uuid         # used for observables
import grpc         # used for type hinting only

# import SiLA2 library
import sila2lib.framework.SiLAFramework_pb2 as silaFW_pb2

# import gRPC modules for this feature
from .gRPC import LevelServicer_pb2 as LevelServicer_pb2
# from .gRPC import LevelServicer_pb2_grpc as LevelServicer_pb2_grpc

# import default arguments
from .LevelServicer_default_arguments import default_dict


# noinspection PyPep8Naming,PyUnusedLocal
class LevelServicerReal:
    """
    Implementation of the *Level Servicer* in *Real* mode
        This is a DASGIP Service
    """

    def __init__(self, reactors):
        """Class initialiser"""
        self.reactors = reactors
        logging.debug('Started server in mode: {mode}'.format(mode='Real'))

    def _get_command_state(self, command_uuid: str) -> silaFW_pb2.ExecutionInfo:
        """
        Method to fill an ExecutionInfo message from the SiLA server for observable commands

        :param command_uuid: The uuid of the command for which to return the current state

        :return: An execution info object with the current command state
        """

        #: Enumeration of silaFW_pb2.ExecutionInfo.CommandStatus
        command_status = silaFW_pb2.ExecutionInfo.CommandStatus.waiting
        #: Real silaFW_pb2.Real(0...1)
        command_progress = None
        #: Duration silaFW_pb2.Duration(seconds=<seconds>, nanos=<nanos>)
        command_estimated_remaining = None
        #: Duration silaFW_pb2.Duration(seconds=<seconds>, nanos=<nanos>)
        command_lifetime_of_execution = None

        # TODO: check the state of the command with the given uuid and return the correct information

        # just return a default in this example
        return silaFW_pb2.ExecutionInfo(
            commandStatus=command_status,
            progressInfo=(
                command_progress if command_progress is not None else None
            ),
            estimatedRemainingTime=(
                command_estimated_remaining if command_estimated_remaining is not None else None
            ),
            updatedLifetimeOfExecution=(
                command_lifetime_of_execution if command_lifetime_of_execution is not None else None
            )
        )

    def GetPV(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetPV_Responses:
        """
        Executes the unobservable command "Get PV"
            Get present value
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentPV (Current PV): Current present value
        """
    
        # initialise the return value
        return_value = None
    
        # Get command
        node = self.reactors[request.UnitID.value].unit.Level.setter.PV
        CurrentPV = node.get_value()
        return_value = LevelServicer_pb2.GetPV_Responses(CurrentPV=silaFW_pb2.Real(value=CurrentPV))

        return return_value

    def GetType(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetType_Responses:
        """
        Executes the unobservable command "Get Function Type"
            Get function type.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentType (Current Function Type): Current function type value of the level module.
        """
    
        # initialise the return value
        return_value = None
    
        # Get command
        node = self.reactors[request.UnitID.value].unit.Level.sysinfo.Type
        CurrentType = node.get_value()
        return_value = LevelServicer_pb2.GetType_Responses(CurrentType=silaFW_pb2.String(value=CurrentType))

        return return_value

    def GetAvailable(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetAvailable_Responses:
        """
        Executes the unobservable command "Get Function Availability"
            Get function availability.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentAvailable (Current Function Availability): Current function availability value of the level
            module.
        """
    
        # initialise the return value
        return_value = None
    
        # Get command
        node = self.reactors[request.UnitID.value].unit.Level.sysinfo.Available
        CurrentAvailable = node.get_value()
        return_value = LevelServicer_pb2.GetAvailable_Responses(
            CurrentAvailable=silaFW_pb2.Integer(value=CurrentAvailable))

        return return_value

    def GetName(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetName_Responses:
        """
        Executes the unobservable command "Get Function Name"
            Get function name.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentName (Current Function Name): Current function name of the level module.
        """
    
        # initialise the return value
        return_value = None
    
        # Get command
        node = self.reactors[request.UnitID.value].unit.Level.sysinfo.Name
        CurrentName = node.get_value()
        return_value = LevelServicer_pb2.GetName_Responses(CurrentName=silaFW_pb2.String(value=CurrentName))

        return return_value

    def GetVersion(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetVersion_Responses:
        """
        Executes the unobservable command "Get Function Version"
            Get function model version number.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentVersion (Current Function Version): Current function model version number of the level module.
        """
    
        # initialise the return value
        return_value = None
    
        # Get command
        node = self.reactors[request.UnitID.value].unit.Level.sysinfo.Version
        CurrentVersion = node.get_value()
        return_value = LevelServicer_pb2.GetVersion_Responses(CurrentVersion=silaFW_pb2.String(value=CurrentVersion))

        return return_value

    def GetSensorPVRaw(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetSensorPVRaw_Responses:
        """
        Executes the unobservable command "Get Sensor PVRaw"
            Get the sensor present raw value. Actual process raw value.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentSensorPVRaw (Current Sensor PVRaw): Current sensor present raw value of the level module.
            Actual process raw value.
        """
    
        # initialise the return value
        return_value = None
    
        # Get command
        node = self.reactors[request.UnitID.value].unit.Level.sensor.PVRaw
        CurrentSensorPVRaw = node.get_value()
        return_value = LevelServicer_pb2.GetSensorPVRaw_Responses(
            CurrentSensorPVRaw=silaFW_pb2.Real(value=CurrentSensorPVRaw))

        return return_value

    def GetSensorSlope(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetSensorSlope_Responses:
        """
        Executes the unobservable command "Get Sensor Slope"
            Get the sensor slope value. Sensor slope calibration parameter.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentSensorSlope (Current Sensor Slope): Current sensor slope value of the level module.
            Sensor slope calibration parameter.
        """
    
        # initialise the return value
        return_value = None
    
        # Get command
        node = self.reactors[request.UnitID.value].unit.Level.sensor.Slope
        CurrentSensorSlope = node.get_value()
        return_value = LevelServicer_pb2.GetSensorSlope_Responses(
            CurrentSensorSlope=silaFW_pb2.Real(value=CurrentSensorSlope))

        return return_value

    def SetAlarmEnabled(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.SetAlarmEnabled_Responses:
        """
        Executes the unobservable command "Set Alarm Enabled"
            Set the alarm enabled value. Enables alarm function.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
            request.AlarmEnabled (Set Alarm Enabled):
            The alarm enabled value. Enables alarm function.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.AlarmEnabledSet (Alarm Enabled Set): The set alarm enabled value. Enables alarm function.
        """
    
        # initialise the return value
        return_value = None
    
       # Set-command
        self.reactors[request.UnitID.value].unit.Level.alarm.Enabled = request.AlarmEnabled.value

        # Get command
        node = self.reactors[request.UnitID.value].unit.Level.alarm.Enabled
        AlarmEnabledSet = node.get_value()
        return_value = LevelServicer_pb2.SetAlarmEnabled_Responses(
            AlarmEnabledSet=silaFW_pb2.Integer(value=AlarmEnabledSet))

        return return_value

    def GetAlarmEnabled(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetAlarmEnabled_Responses:
        """
        Executes the unobservable command "Get Alarm enabled"
            Get the alarm enabled value. Enables alarm function.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentAlarmEnabled (Current Alarm Enabled): Current alarm enabled value of the level module.
            Enables alarm function.
        """
    
        # initialise the return value
        return_value = None
    
        # Get command
        node = self.reactors[request.UnitID.value].unit.Level.alarm.Enabled
        CurrentAlarmEnabled = node.get_value()
        return_value = LevelServicer_pb2.GetAlarmEnabled_Responses(
            CurrentAlarmEnabled=silaFW_pb2.Integer(value=CurrentAlarmEnabled))

        return return_value

    def SetAlarmAlarmHigh(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.SetAlarmAlarmHigh_Responses:
        """
        Executes the unobservable command "Set Alarm AlarmHigh"
            Set the alarm alarmhigh value. Higher alarm limit.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
            request.AlarmAlarmHigh (Set Alarm AlarmHigh):
            The alarm alarmhigh value. Higher alarm limit.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.AlarmAlarmHighSet (Alarm AlarmHigh Set): The set alarm alarmhigh value. Higher alarm limit.
        """
    
        # initialise the return value
        return_value = None
    
        # Set-command
        self.reactors[request.UnitID.value].unit.Level.alarm.AlarmHigh = request.AlarmAlarmHigh.value

        # Get command
        node = self.reactors[request.UnitID.value].unit.Level.alarm.AlarmHigh
        AlarmAlarmHighSet = node.get_value()
        return_value = LevelServicer_pb2.SetAlarmAlarmHigh_Responses(
            AlarmAlarmHighSet=silaFW_pb2.Real(value=AlarmAlarmHighSet))

        return return_value

    def GetAlarmAlarmHigh(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetAlarmAlarmHigh_Responses:
        """
        Executes the unobservable command "Get Alarm AlarmHigh"
            Get the alarm alarmhigh value. Higher alarm limit.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentAlarmAlarmHigh (Current Alarm AlarmHigh): Current alarm alarmhigh value of the level module.
            Higher alarm limit.
        """
    
        # initialise the return value
        return_value = None
    
        # Get command
        node = self.reactors[request.UnitID.value].unit.Level.alarm.AlarmHigh
        CurrentAlarmAlarmHigh = node.get_value()
        return_value = LevelServicer_pb2.GetAlarmAlarmHigh_Responses(
            CurrentAlarmAlarmHigh=silaFW_pb2.Real(value=CurrentAlarmAlarmHigh))

        return return_value

    def SetAlarmAlarmLow(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.SetAlarmAlarmLow_Responses:
        """
        Executes the unobservable command "Set Alarm AlarmLow"
            Set the alarm alarmlow value. Lower alarm limit.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
            request.AlarmAlarmLow (Set Alarm AlarmLow):
            The alarm alarmlow value. Lower alarm limit.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.AlarmAlarmLowSet (Alarm AlarmLow Set): The set alarm alarmlow value. Lower alarm limit.
        """
    
        # initialise the return value
        return_value = None
    
        # Set-command
        self.reactors[request.UnitID.value].unit.Level.alarm.AlarmLow = request.AlarmAlarmLow.value

        # Get command
        node = self.reactors[request.UnitID.value].unit.Level.alarm.AlarmLow
        AlarmAlarmLowSet = node.get_value()
        return_value = LevelServicer_pb2.SetAlarmAlarmLow_Responses(
            AlarmAlarmLowSet=silaFW_pb2.Real(value=AlarmAlarmLowSet))

        return return_value

    def GetAlarmAlarmLow(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetAlarmAlarmLow_Responses:
        """
        Executes the unobservable command "Get Alarm AlarmLow"
            Get the alarm alarmlow value. Lower alarm limit.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentAlarmAlarmLow (Current Alarm AlarmLow): Current alarm alarmlow value of the level module.
            Lower alarm limit.
        """
    
        # initialise the return value
        return_value = None
    
        # Get command
        node = self.reactors[request.UnitID.value].unit.Level.alarm.AlarmLow
        CurrentAlarmAlarmLow = node.get_value()
        return_value = LevelServicer_pb2.GetAlarmAlarmLow_Responses(
            CurrentAlarmAlarmLow=silaFW_pb2.Real(value=CurrentAlarmAlarmLow))

        return return_value

    def SetAlarmMode(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.SetAlarmMode_Responses:
        """
        Executes the unobservable command "Set Alarm Mode"
            Set the alarm mode value. Alarm mode.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
            request.AlarmMode (Set Alarm Mode):
            The alarm mode value. Alarm mode.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.AlarmModeSet (Alarm Mode Set): The set alarm mode value. Alarm mode.
        """
    
        # initialise the return value
        return_value = None
    
        # Set-command
        self.reactors[request.UnitID.value].unit.Level.alarm.Mode = request.AlarmMode.value

        # Get command
        node = self.reactors[request.UnitID.value].unit.Level.alarm.Mode
        AlarmModeSet = node.get_value()
        return_value = LevelServicer_pb2.SetAlarmMode_Responses(AlarmModeSet=silaFW_pb2.Integer(value=AlarmModeSet))

        return return_value

    def GetAlarmMode(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetAlarmMode_Responses:
        """
        Executes the unobservable command "Get Alarm Mode"
            Get the alarm mode value. Alarm mode.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentAlarmMode (Current Alarm Mode): Current alarm mode value of the level module. Alarm mode.
        """
    
        # initialise the return value
        return_value = None
    
        # Get command
        node = self.reactors[request.UnitID.value].unit.Level.alarm.Mode
        CurrentAlarmMode = node.get_value()
        return_value = LevelServicer_pb2.GetAlarmMode_Responses(
            CurrentAlarmMode=silaFW_pb2.Integer(value=CurrentAlarmMode))

        return return_value

    def GetAlarmState(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetAlarmState_Responses:
        """
        Executes the unobservable command "Get Alarm State"
            Get the alarm state value. Alarm state.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentAlarmState (Current Alarm State): Current alarm state value of the level module. Alarm state.
        """
    
        # initialise the return value
        return_value = None
    
        # Get command
        node = self.reactors[request.UnitID.value].unit.Level.alarm.State
        CurrentAlarmState = node.get_value()
        return_value = LevelServicer_pb2.GetAlarmState_Responses(
            CurrentAlarmState=silaFW_pb2.Integer(value=CurrentAlarmState))

        return return_value

    def SetAlarmDelay(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.SetAlarmDelay_Responses:
        """
        Executes the unobservable command "Set Alarm Delay"
            Set the alarm delay value. Alarm hysteresis time.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
            request.AlarmDelay (Set Alarm Delay):
            The alarm delay value. Alarm hysteresis time.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.AlarmDelaySet (Alarm Delay Set): The set alarm delay value. Alarm hysteresis time.
        """
    
        # initialise the return value
        return_value = None
    
        # Set-command
        self.reactors[request.UnitID.value].unit.Level.alarm.Delay = request.AlarmDelay.value

        # Get command
        node = self.reactors[request.UnitID.value].unit.Level.alarm.Delay
        AlarmDelaySet = node.get_value()
        return_value = LevelServicer_pb2.SetAlarmDelay_Responses(AlarmDelaySet=silaFW_pb2.Real(value=AlarmDelaySet))

        return return_value

    def GetAlarmDelay(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetAlarmDelay_Responses:
        """
        Executes the unobservable command "Get Alarm Delay"
            Get the alarm delay value.Alarm hysteresis time.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentAlarmDelay (Current Alarm Delay): Current alarm delay value of the level module. Alarm
            hysteresis time.
        """
    
        # initialise the return value
        return_value = None
    
        # Get command
        node = self.reactors[request.UnitID.value].unit.Level.alarm.Delay
        CurrentAlarmDelay = node.get_value()
        return_value = LevelServicer_pb2.GetAlarmDelay_Responses(
            CurrentAlarmDelay=silaFW_pb2.Real(value=CurrentAlarmDelay))

        return return_value

    def SetAlarmWarnHigh(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.SetAlarmWarnHigh_Responses:
        """
        Executes the unobservable command "Set Alarm WarnHigh"
            Set the alarm higher warning value. Higher warning limit.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
            request.AlarmWarnHigh (Set Alarm WarnHigh):
            The alarm higher warning value. Higher warning limit.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.AlarmWarnHighSet (Alarm WarnHigh Set): The set alarm higher warning value. Higher warning limit.
        """
    
        # initialise the return value
        return_value = None
    
        # Set-command
        self.reactors[request.UnitID.value].unit.Level.alarm.WarnHigh = request.AlarmWarnHigh.value

        # Get command
        node = self.reactors[request.UnitID.value].unit.Level.alarm.WarnHigh
        AlarmWarnHighSet = node.get_value()
        return_value = LevelServicer_pb2.SetAlarmWarnHigh_Responses(
            AlarmWarnHighSet=silaFW_pb2.Real(value=AlarmWarnHighSet))

        return return_value

    def GetAlarmWarnHigh(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetAlarmWarnHigh_Responses:
        """
        Executes the unobservable command "Get Alarm WarnHigh"
            Get the alarm higher warning value. Higher warning limit.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentAlarmWarnHigh (Current Alarm WarnHigh): Current alarm higher warning value of the level
            module. Higher warning limit.
        """
    
        # initialise the return value
        return_value = None
    
        # Get command
        node = self.reactors[request.UnitID.value].unit.Level.alarm.WarnHigh
        CurrentAlarmWarnHigh = node.get_value()
        return_value = LevelServicer_pb2.GetAlarmWarnHigh_Responses(
            CurrentAlarmWarnHigh=silaFW_pb2.Real(value=CurrentAlarmWarnHigh))

        return return_value

    def SetAlarmWarnLow(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.SetAlarmWarnLow_Responses:
        """
        Executes the unobservable command "Set Alarm WarnLow"
            Set the alarm lower warning value. Lower warning limit.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
            request.AlarmWarnLow (Set Alarm WarnLow):
            The alarm lower warning value. Lower warning limit.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.AlarmWarnLowSet (Alarm WarnLow Set): The set alarm lower warning value. Lower warning limit.
        """
    
        # initialise the return value
        return_value = None
    
        # Set-command
        self.reactors[request.UnitID.value].unit.Level.alarm.WarnLow = request.AlarmWarnLow.value

        # Get command
        node = self.reactors[request.UnitID.value].unit.Level.alarm.WarnLow
        AlarmWarnLowSet = node.get_value()
        return_value = LevelServicer_pb2.SetAlarmWarnLow_Responses(
            AlarmWarnLowSet=silaFW_pb2.Real(value=AlarmWarnLowSet))

        return return_value

    def GetAlarmWarnLow(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetAlarmWarnLow_Responses:
        """
        Executes the unobservable command "Get Alarm WarnLow"
            Get the alarm lower warning value. Lower warning limit.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentAlarmWarnLow (Current Alarm WarnLow): Current alarm lower warning value of the level module.
            Lower warning limit.
        """
    
        # initialise the return value
        return_value = None
    
        # Get command
        node = self.reactors[request.UnitID.value].unit.Level.alarm.WarnLow
        CurrentAlarmWarnLow = node.get_value()
        return_value = LevelServicer_pb2.GetAlarmWarnLow_Responses(
            CurrentAlarmWarnLow=silaFW_pb2.Real(value=CurrentAlarmWarnLow))

        return return_value

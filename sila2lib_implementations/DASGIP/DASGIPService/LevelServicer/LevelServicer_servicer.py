"""
________________________________________________________________________

:PROJECT: SiLA2_python

*Level Servicer*

:details: LevelServicer:
    Control a DASGIP level module. Enables read and write operations for various parameters, including level sensor and
    alarm.
    By Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 20.05.2019
           
:file:    LevelServicer_servicer.py
:authors: Lukas Bromig

:date: (creation)          2020-04-16T10:19:14.137002
:date: (last modification) 2020-04-16T10:19:14.137002

.. note:: Code generated by sila2codegenerator 0.2.0

________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "0.0.1"

# import general packages
import logging
import grpc

# meta packages
from typing import Union

# import SiLA2 library
import sila2lib.framework.SiLAFramework_pb2 as silaFW_pb2
from sila2lib.error_handling.server_err import SiLAError

# import gRPC modules for this feature
from .gRPC import LevelServicer_pb2 as LevelServicer_pb2
from .gRPC import LevelServicer_pb2_grpc as LevelServicer_pb2_grpc

# import simulation and real implementation
from .LevelServicer_simulation import LevelServicerSimulation
from .LevelServicer_real import LevelServicerReal


class LevelServicer(LevelServicer_pb2_grpc.LevelServicerServicer):
    """
    This is a DASGIP Service
    """
    implementation: Union[LevelServicerSimulation, LevelServicerReal]
    simulation_mode: bool

    def __init__(self, reactors, simulation_mode: bool = True):
        """
        Class initialiser.

        :param simulation_mode: Sets whether at initialisation the simulation mode is active or the real mode.
        """
        self.reactors = reactors
        self.simulation_mode = simulation_mode
        if simulation_mode:
            self._inject_implementation(LevelServicerSimulation(self.reactors))
        else:
            self._inject_implementation(LevelServicerReal(self.reactors))

    def _inject_implementation(self,
                               implementation: Union[LevelServicerSimulation,
                                                     LevelServicerReal]
                               ) -> bool:
        """
        Dependency injection of the implementation used.
            Allows to set the class used for simulation/real mode.

        :param implementation: A valid implementation of the DASGIP_ServiceServicer.
        """

        self.implementation = implementation
        return True

    def switch_to_simulation_mode(self):
        """Method that will automatically be called by the server when the simulation mode is requested."""
        self.simulation_mode = True
        self._inject_implementation(LevelServicerSimulation(self.reactors))

    def switch_to_real_mode(self):
        """Method that will automatically be called by the server when the real mode is requested."""
        self.simulation_mode = False
        self._inject_implementation(LevelServicerReal(self.reactors))

    def GetPV(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetPV_Responses:
        """
        Executes the unobservable command "Get PV"
            Get present value
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentPV (Current PV): Current present value
        """
    
        logging.debug(
            "GetPV called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetPV(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetType(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetType_Responses:
        """
        Executes the unobservable command "Get Function Type"
            Get function type.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentType (Current Function Type): Current function type value of the level module.
        """
    
        logging.debug(
            "GetType called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetType(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetAvailable(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetAvailable_Responses:
        """
        Executes the unobservable command "Get Function Availability"
            Get function availability.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentAvailable (Current Function Availability): Current function availability value of the level module.
        """
    
        logging.debug(
            "GetAvailable called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetAvailable(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetName(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetName_Responses:
        """
        Executes the unobservable command "Get Function Name"
            Get function name.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentName (Current Function Name): Current function name of the level module.
        """
    
        logging.debug(
            "GetName called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetName(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetVersion(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetVersion_Responses:
        """
        Executes the unobservable command "Get Function Version"
            Get function model version number.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentVersion (Current Function Version): Current function model version number of the level module.
        """
    
        logging.debug(
            "GetVersion called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetVersion(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetSensorPVRaw(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetSensorPVRaw_Responses:
        """
        Executes the unobservable command "Get Sensor PVRaw"
            Get the sensor present raw value. Actual process raw value.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentSensorPVRaw (Current Sensor PVRaw): Current sensor present raw value of the level module. Actual process raw value.
        """
    
        logging.debug(
            "GetSensorPVRaw called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetSensorPVRaw(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def SetSensorSlope(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.SetSensorSlope_Responses:
        """
        Executes the unobservable command "Set Sensor Slope"
            Set the sensor slope value. Sensor slope calibration parameter.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
            request.SensorSlope (Set Sensor Slope):
            The sensor slope value. Sensor slope calibration parameter.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.SensorSlopeSet (Sensor Slope Set): The set sensor slope value. Sensor slope calibration parameter.
        """
    
        logging.debug(
            "SetSensorSlope called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.SetSensorSlope(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetSensorSlope(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetSensorSlope_Responses:
        """
        Executes the unobservable command "Get Sensor Slope"
            Get the sensor slope value. Sensor slope calibration parameter.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentSensorSlope (Current Sensor Slope): Current sensor slope value of the level module. Sensor slope calibration parameter.
        """
    
        logging.debug(
            "GetSensorSlope called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetSensorSlope(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def SetAlarmEnabled(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.SetAlarmEnabled_Responses:
        """
        Executes the unobservable command "Set Alarm Enabled"
            Set the alarm enabled value. Enables alarm function.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
            request.AlarmEnabled (Set Alarm Enabled):
            The alarm enabled value. Enables alarm function.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.AlarmEnabledSet (Alarm Enabled Set): The set alarm enabled value. Enables alarm function.
        """
    
        logging.debug(
            "SetAlarmEnabled called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.SetAlarmEnabled(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetAlarmEnabled(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetAlarmEnabled_Responses:
        """
        Executes the unobservable command "Get Alarm enabled"
            Get the alarm enabled value. Enables alarm function.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentAlarmEnabled (Current Alarm Enabled): Current alarm enabled value of the level module. Enables alarm function.
        """
    
        logging.debug(
            "GetAlarmEnabled called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetAlarmEnabled(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def SetAlarmAlarmHigh(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.SetAlarmAlarmHigh_Responses:
        """
        Executes the unobservable command "Set Alarm AlarmHigh"
            Set the alarm alarmhigh value. Higher alarm limit.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
            request.AlarmAlarmHigh (Set Alarm AlarmHigh):
            The alarm alarmhigh value. Higher alarm limit.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.AlarmAlarmHighSet (Alarm AlarmHigh Set): The set alarm alarmhigh value. Higher alarm limit.
        """
    
        logging.debug(
            "SetAlarmAlarmHigh called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.SetAlarmAlarmHigh(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetAlarmAlarmHigh(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetAlarmAlarmHigh_Responses:
        """
        Executes the unobservable command "Get Alarm AlarmHigh"
            Get the alarm alarmhigh value. Higher alarm limit.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentAlarmAlarmHigh (Current Alarm AlarmHigh): Current alarm alarmhigh value of the level module. Higher alarm limit.
        """
    
        logging.debug(
            "GetAlarmAlarmHigh called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetAlarmAlarmHigh(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def SetAlarmAlarmLow(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.SetAlarmAlarmLow_Responses:
        """
        Executes the unobservable command "Set Alarm AlarmLow"
            Set the alarm alarmlow value. Lower alarm limit.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
            request.AlarmAlarmLow (Set Alarm AlarmLow):
            The alarm alarmlow value. Lower alarm limit.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.AlarmAlarmLowSet (Alarm AlarmLow Set): The set alarm alarmlow value. Lower alarm limit.
        """
    
        logging.debug(
            "SetAlarmAlarmLow called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.SetAlarmAlarmLow(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetAlarmAlarmLow(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetAlarmAlarmLow_Responses:
        """
        Executes the unobservable command "Get Alarm AlarmLow"
            Get the alarm alarmlow value. Lower alarm limit.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentAlarmAlarmLow (Current Alarm AlarmLow): Current alarm alarmlow value of the level module. Lower alarm limit.
        """
    
        logging.debug(
            "GetAlarmAlarmLow called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetAlarmAlarmLow(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def SetAlarmMode(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.SetAlarmMode_Responses:
        """
        Executes the unobservable command "Set Alarm Mode"
            Set the alarm mode value. Alarm mode.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
            request.AlarmMode (Set Alarm Mode):
            The alarm mode value. Alarm mode.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.AlarmModeSet (Alarm Mode Set): The set alarm mode value. Alarm mode.
        """
    
        logging.debug(
            "SetAlarmMode called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.SetAlarmMode(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetAlarmMode(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetAlarmMode_Responses:
        """
        Executes the unobservable command "Get Alarm Mode"
            Get the alarm mode value. Alarm mode.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentAlarmMode (Current Alarm Mode): Current alarm mode value of the level module. Alarm mode.
        """
    
        logging.debug(
            "GetAlarmMode called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetAlarmMode(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetAlarmState(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetAlarmState_Responses:
        """
        Executes the unobservable command "Get Alarm State"
            Get the alarm state value. Alarm state.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentAlarmState (Current Alarm State): Current alarm state value of the level module. Alarm state.
        """
    
        logging.debug(
            "GetAlarmState called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetAlarmState(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def SetAlarmDelay(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.SetAlarmDelay_Responses:
        """
        Executes the unobservable command "Set Alarm Delay"
            Set the alarm delay value. Alarm hysteresis time.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
            request.AlarmDelay (Set Alarm Delay):
            The alarm delay value. Alarm hysteresis time.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.AlarmDelaySet (Alarm Delay Set): The set alarm delay value. Alarm hysteresis time.
        """
    
        logging.debug(
            "SetAlarmDelay called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.SetAlarmDelay(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetAlarmDelay(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetAlarmDelay_Responses:
        """
        Executes the unobservable command "Get Alarm Delay"
            Get the alarm delay value.Alarm hysteresis time.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentAlarmDelay (Current Alarm Delay): Current alarm delay value of the level module. Alarm hysteresis time.
        """
    
        logging.debug(
            "GetAlarmDelay called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetAlarmDelay(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def SetAlarmWarnHigh(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.SetAlarmWarnHigh_Responses:
        """
        Executes the unobservable command "Set Alarm WarnHigh"
            Set the alarm higher warning value. Higher warning limit.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
            request.AlarmWarnHigh (Set Alarm WarnHigh):
            The alarm higher warning value. Higher warning limit.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.AlarmWarnHighSet (Alarm WarnHigh Set): The set alarm higher warning value. Higher warning limit.
        """
    
        logging.debug(
            "SetAlarmWarnHigh called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.SetAlarmWarnHigh(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetAlarmWarnHigh(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetAlarmWarnHigh_Responses:
        """
        Executes the unobservable command "Get Alarm WarnHigh"
            Get the alarm higher warning value. Higher warning limit.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentAlarmWarnHigh (Current Alarm WarnHigh): Current alarm higher warning value of the level module. Higher warning limit.
        """
    
        logging.debug(
            "GetAlarmWarnHigh called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetAlarmWarnHigh(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def SetAlarmWarnLow(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.SetAlarmWarnLow_Responses:
        """
        Executes the unobservable command "Set Alarm WarnLow"
            Set the alarm lower warning value. Lower warning limit.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
            request.AlarmWarnLow (Set Alarm WarnLow):
            The alarm lower warning value. Lower warning limit.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.AlarmWarnLowSet (Alarm WarnLow Set): The set alarm lower warning value. Lower warning limit.
        """
    
        logging.debug(
            "SetAlarmWarnLow called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.SetAlarmWarnLow(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetAlarmWarnLow(self, request, context: grpc.ServicerContext) \
            -> LevelServicer_pb2.GetAlarmWarnLow_Responses:
        """
        Executes the unobservable command "Get Alarm WarnLow"
            Get the alarm lower warning value. Lower warning limit.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentAlarmWarnLow (Current Alarm WarnLow): Current alarm lower warning value of the level module. Lower warning limit.
        """
    
        logging.debug(
            "GetAlarmWarnLow called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetAlarmWarnLow(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)

    

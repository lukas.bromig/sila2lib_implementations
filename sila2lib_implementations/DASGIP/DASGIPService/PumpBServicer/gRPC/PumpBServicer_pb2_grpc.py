# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

from . import PumpBServicer_pb2 as PumpBServicer__pb2


class PumpBServicerStub(object):
  """Feature: PumpB Servicer

  Control a DASGIP PumpB module. Enables read and write operations for various parameters, including PumpB sensor,
  controller, and alarm.
  By Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 20.05.2019

  """

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.GetPVInt = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/GetPVInt',
        request_serializer=PumpBServicer__pb2.GetPVInt_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.GetPVInt_Responses.FromString,
        )
    self.GetPV = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/GetPV',
        request_serializer=PumpBServicer__pb2.GetPV_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.GetPV_Responses.FromString,
        )
    self.SetSPM = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/SetSPM',
        request_serializer=PumpBServicer__pb2.SetSPM_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.SetSPM_Responses.FromString,
        )
    self.SetSPE = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/SetSPE',
        request_serializer=PumpBServicer__pb2.SetSPE_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.SetSPE_Responses.FromString,
        )
    self.GetSP = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/GetSP',
        request_serializer=PumpBServicer__pb2.GetSP_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.GetSP_Responses.FromString,
        )
    self.GetSPA = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/GetSPA',
        request_serializer=PumpBServicer__pb2.GetSPA_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.GetSPA_Responses.FromString,
        )
    self.GetSPM = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/GetSPM',
        request_serializer=PumpBServicer__pb2.GetSPM_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.GetSPM_Responses.FromString,
        )
    self.GetSPE = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/GetSPE',
        request_serializer=PumpBServicer__pb2.GetSPE_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.GetSPE_Responses.FromString,
        )
    self.GetSPR = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/GetSPR',
        request_serializer=PumpBServicer__pb2.GetSPR_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.GetSPR_Responses.FromString,
        )
    self.GetAccess = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/GetAccess',
        request_serializer=PumpBServicer__pb2.GetAccess_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.GetAccess_Responses.FromString,
        )
    self.SetCmd = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/SetCmd',
        request_serializer=PumpBServicer__pb2.SetCmd_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.SetCmd_Responses.FromString,
        )
    self.GetCmd = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/GetCmd',
        request_serializer=PumpBServicer__pb2.GetCmd_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.GetCmd_Responses.FromString,
        )
    self.SetMode = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/SetMode',
        request_serializer=PumpBServicer__pb2.SetMode_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.SetMode_Responses.FromString,
        )
    self.GetMode = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/GetMode',
        request_serializer=PumpBServicer__pb2.GetMode_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.GetMode_Responses.FromString,
        )
    self.SetSetpointSelect = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/SetSetpointSelect',
        request_serializer=PumpBServicer__pb2.SetSetpointSelect_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.SetSetpointSelect_Responses.FromString,
        )
    self.GetSetpointSelect = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/GetSetpointSelect',
        request_serializer=PumpBServicer__pb2.GetSetpointSelect_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.GetSetpointSelect_Responses.FromString,
        )
    self.GetState = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/GetState',
        request_serializer=PumpBServicer__pb2.GetState_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.GetState_Responses.FromString,
        )
    self.GetType = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/GetType',
        request_serializer=PumpBServicer__pb2.GetType_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.GetType_Responses.FromString,
        )
    self.GetAvailable = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/GetAvailable',
        request_serializer=PumpBServicer__pb2.GetAvailable_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.GetAvailable_Responses.FromString,
        )
    self.GetName = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/GetName',
        request_serializer=PumpBServicer__pb2.GetName_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.GetName_Responses.FromString,
        )
    self.GetVersion = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/GetVersion',
        request_serializer=PumpBServicer__pb2.GetVersion_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.GetVersion_Responses.FromString,
        )
    self.SetActuatorCalibration = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/SetActuatorCalibration',
        request_serializer=PumpBServicer__pb2.SetActuatorCalibration_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.SetActuatorCalibration_Responses.FromString,
        )
    self.GetActuatorCalibration = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/GetActuatorCalibration',
        request_serializer=PumpBServicer__pb2.GetActuatorCalibration_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.GetActuatorCalibration_Responses.FromString,
        )
    self.GetActuatorDirPV = channel.unary_unary(
        '/sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer/GetActuatorDirPV',
        request_serializer=PumpBServicer__pb2.GetActuatorDirPV_Parameters.SerializeToString,
        response_deserializer=PumpBServicer__pb2.GetActuatorDirPV_Responses.FromString,
        )


class PumpBServicerServicer(object):
  """Feature: PumpB Servicer

  Control a DASGIP PumpB module. Enables read and write operations for various parameters, including PumpB sensor,
  controller, and alarm.
  By Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 20.05.2019

  """

  def GetPVInt(self, request, context):
    """Get PVInt 
    Get integrated present value
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetPV(self, request, context):
    """Get PV
    Get present value
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def SetSPM(self, request, context):
    """Set Manual Setpoint

    Set the manual PumpB setpoint.

    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def SetSPE(self, request, context):
    """Set External Setpoint

    Set the external PumpB setpoint.

    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetSP(self, request, context):
    """Get SP
    Get setpoint value
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetSPA(self, request, context):
    """Get SPA
    Get automatic setpoint value
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetSPM(self, request, context):
    """Get SPM
    Get manual setpoint value
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetSPE(self, request, context):
    """Get SPE
    Get external setpoint value
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetSPR(self, request, context):
    """Get SPR
    Get remote setpoint value
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetAccess(self, request, context):
    """Get Access Mode
    Get access mode value. Controller access (Local, Remote).
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def SetCmd(self, request, context):
    """Set Controller Command

    Set the controller command. Controller command (Nothing, Stop, Start).

    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetCmd(self, request, context):
    """Get Controller Command
    Get the controller command. Controller command (Nothing, Stop, Start).
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def SetMode(self, request, context):
    """Set Controller Mode

    Set the controller mode. Controller mode (Manual, Automatic).

    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetMode(self, request, context):
    """Get Controller Mode
    Get the controller mode. Controller mode (Manual, Automatic).
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def SetSetpointSelect(self, request, context):
    """Set SetpointSelect

    Set the selected setpoint that should be used. Setpoint selection (Local, Manual, Internal, Script, External).

    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetSetpointSelect(self, request, context):
    """Get SetpointSelect
    Get the setpoint selection. Controller state (Off, On, Error).
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetState(self, request, context):
    """Get State
    Get controller state. Controller state (Off, On, Error).
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetType(self, request, context):
    """Get Function Type
    Get function type.
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetAvailable(self, request, context):
    """Get Function Availability
    Get function availability.
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetName(self, request, context):
    """Get Function Name
    Get function name.
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetVersion(self, request, context):
    """Get Function Version
    Get function model version number.
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def SetActuatorCalibration(self, request, context):
    """Set Actuator Calibration

    Set the actuator calibration value that should be used. Calibration parameter (at 100% speed).

    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetActuatorCalibration(self, request, context):
    """Get Actuator Calibration
    Get the actuator calibration value that is used. Calibration parameter (at 100% speed).
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetActuatorDirPV(self, request, context):
    """Get Actuator Dir PV
    Get the actuator direction value. Actual pump direction (0=Clockwise, 1=Counterclockwise).
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_PumpBServicerServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'GetPVInt': grpc.unary_unary_rpc_method_handler(
          servicer.GetPVInt,
          request_deserializer=PumpBServicer__pb2.GetPVInt_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.GetPVInt_Responses.SerializeToString,
      ),
      'GetPV': grpc.unary_unary_rpc_method_handler(
          servicer.GetPV,
          request_deserializer=PumpBServicer__pb2.GetPV_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.GetPV_Responses.SerializeToString,
      ),
      'SetSPM': grpc.unary_unary_rpc_method_handler(
          servicer.SetSPM,
          request_deserializer=PumpBServicer__pb2.SetSPM_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.SetSPM_Responses.SerializeToString,
      ),
      'SetSPE': grpc.unary_unary_rpc_method_handler(
          servicer.SetSPE,
          request_deserializer=PumpBServicer__pb2.SetSPE_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.SetSPE_Responses.SerializeToString,
      ),
      'GetSP': grpc.unary_unary_rpc_method_handler(
          servicer.GetSP,
          request_deserializer=PumpBServicer__pb2.GetSP_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.GetSP_Responses.SerializeToString,
      ),
      'GetSPA': grpc.unary_unary_rpc_method_handler(
          servicer.GetSPA,
          request_deserializer=PumpBServicer__pb2.GetSPA_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.GetSPA_Responses.SerializeToString,
      ),
      'GetSPM': grpc.unary_unary_rpc_method_handler(
          servicer.GetSPM,
          request_deserializer=PumpBServicer__pb2.GetSPM_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.GetSPM_Responses.SerializeToString,
      ),
      'GetSPE': grpc.unary_unary_rpc_method_handler(
          servicer.GetSPE,
          request_deserializer=PumpBServicer__pb2.GetSPE_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.GetSPE_Responses.SerializeToString,
      ),
      'GetSPR': grpc.unary_unary_rpc_method_handler(
          servicer.GetSPR,
          request_deserializer=PumpBServicer__pb2.GetSPR_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.GetSPR_Responses.SerializeToString,
      ),
      'GetAccess': grpc.unary_unary_rpc_method_handler(
          servicer.GetAccess,
          request_deserializer=PumpBServicer__pb2.GetAccess_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.GetAccess_Responses.SerializeToString,
      ),
      'SetCmd': grpc.unary_unary_rpc_method_handler(
          servicer.SetCmd,
          request_deserializer=PumpBServicer__pb2.SetCmd_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.SetCmd_Responses.SerializeToString,
      ),
      'GetCmd': grpc.unary_unary_rpc_method_handler(
          servicer.GetCmd,
          request_deserializer=PumpBServicer__pb2.GetCmd_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.GetCmd_Responses.SerializeToString,
      ),
      'SetMode': grpc.unary_unary_rpc_method_handler(
          servicer.SetMode,
          request_deserializer=PumpBServicer__pb2.SetMode_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.SetMode_Responses.SerializeToString,
      ),
      'GetMode': grpc.unary_unary_rpc_method_handler(
          servicer.GetMode,
          request_deserializer=PumpBServicer__pb2.GetMode_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.GetMode_Responses.SerializeToString,
      ),
      'SetSetpointSelect': grpc.unary_unary_rpc_method_handler(
          servicer.SetSetpointSelect,
          request_deserializer=PumpBServicer__pb2.SetSetpointSelect_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.SetSetpointSelect_Responses.SerializeToString,
      ),
      'GetSetpointSelect': grpc.unary_unary_rpc_method_handler(
          servicer.GetSetpointSelect,
          request_deserializer=PumpBServicer__pb2.GetSetpointSelect_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.GetSetpointSelect_Responses.SerializeToString,
      ),
      'GetState': grpc.unary_unary_rpc_method_handler(
          servicer.GetState,
          request_deserializer=PumpBServicer__pb2.GetState_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.GetState_Responses.SerializeToString,
      ),
      'GetType': grpc.unary_unary_rpc_method_handler(
          servicer.GetType,
          request_deserializer=PumpBServicer__pb2.GetType_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.GetType_Responses.SerializeToString,
      ),
      'GetAvailable': grpc.unary_unary_rpc_method_handler(
          servicer.GetAvailable,
          request_deserializer=PumpBServicer__pb2.GetAvailable_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.GetAvailable_Responses.SerializeToString,
      ),
      'GetName': grpc.unary_unary_rpc_method_handler(
          servicer.GetName,
          request_deserializer=PumpBServicer__pb2.GetName_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.GetName_Responses.SerializeToString,
      ),
      'GetVersion': grpc.unary_unary_rpc_method_handler(
          servicer.GetVersion,
          request_deserializer=PumpBServicer__pb2.GetVersion_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.GetVersion_Responses.SerializeToString,
      ),
      'SetActuatorCalibration': grpc.unary_unary_rpc_method_handler(
          servicer.SetActuatorCalibration,
          request_deserializer=PumpBServicer__pb2.SetActuatorCalibration_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.SetActuatorCalibration_Responses.SerializeToString,
      ),
      'GetActuatorCalibration': grpc.unary_unary_rpc_method_handler(
          servicer.GetActuatorCalibration,
          request_deserializer=PumpBServicer__pb2.GetActuatorCalibration_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.GetActuatorCalibration_Responses.SerializeToString,
      ),
      'GetActuatorDirPV': grpc.unary_unary_rpc_method_handler(
          servicer.GetActuatorDirPV,
          request_deserializer=PumpBServicer__pb2.GetActuatorDirPV_Parameters.FromString,
          response_serializer=PumpBServicer__pb2.GetActuatorDirPV_Responses.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'sila2.org.silastandard.examples.pumpbservicer.v1.PumpBServicer', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))

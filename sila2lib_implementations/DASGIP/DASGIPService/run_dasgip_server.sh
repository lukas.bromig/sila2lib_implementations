#!/bin/bash

# This is an example bash script for the start of the DASGIP server

echo "Test"

cd /home/pi/Desktop/sila2lib_implementations/sila2lib_implementations/DASGIP/DASGIPService
/home/pi/.local/bin/poetry run python3 DASGIP_Service_server.py -a "10.152.248.24" -p 50100 -n "DIB-DASGIP"

"""
________________________________________________________________________

:PROJECT: SiLA2_python

*Fluid Definition Service*

:details: FluidDefinitionService:
    Allows full set of the fluid properties as well as the current information of the working fluid.
    By Lukas Bromig and Jose de Jesus Pina, Institute of Biochemical Engineering, Technical University of Munich,
    02.12.2020
           
:file:    FluidDefinitionService_servicer.py
:authors: Lukas Bromig and Jose de Jesus Pina

:date: (creation)          2021-03-19T13:54:01.494952
:date: (last modification) 2021-03-19T13:54:01.494952

.. note:: Code generated by sila2codegenerator 0.2.0

________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "1.0"

# import general packages
import logging
import grpc

# meta packages
from typing import Union

# import SiLA2 library
from sila2lib.error_handling.server_err import SiLAError

# import gRPC modules for this feature
from .gRPC import FluidDefinitionService_pb2 as FluidDefinitionService_pb2
from .gRPC import FluidDefinitionService_pb2_grpc as FluidDefinitionService_pb2_grpc

# import simulation and real implementation
from .FluidDefinitionService_simulation import FluidDefinitionServiceSimulation
from .FluidDefinitionService_real import FluidDefinitionServiceReal


class FluidDefinitionService(FluidDefinitionService_pb2_grpc.FluidDefinitionServiceServicer):
    """
    This is a flowmeter service
    """
    implementation: Union[FluidDefinitionServiceSimulation, FluidDefinitionServiceReal]
    simulation_mode: bool

    def __init__(self, ser, lock, status, simulation_mode: bool = True):
        """
        Class initialiser.

        :param simulation_mode: Sets whether at initialisation the simulation mode is active or the real mode.
        """
        self.lock = lock
        self.ser = ser
        self.status = status
        self.simulation_mode = simulation_mode
        if simulation_mode:
            self._inject_implementation(FluidDefinitionServiceSimulation(status=self.status))
        else:
            self._inject_implementation(FluidDefinitionServiceReal(ser=self.ser, status=self.status))

    def _inject_implementation(self,
                               implementation: Union[FluidDefinitionServiceSimulation,
                                                     FluidDefinitionServiceReal]
                               ) -> bool:
        """
        Dependency injection of the implementation used.
            Allows to set the class used for simulation/real mode.

        :param implementation: A valid implementation of the FlowmeterServiceServicer.
        """

        self.implementation = implementation
        return True

    def switch_to_simulation_mode(self):
        """Method that will automatically be called by the server when the simulation mode is requested."""
        self.simulation_mode = True
        self._inject_implementation(FluidDefinitionServiceSimulation(status=self.status))

    def switch_to_real_mode(self):
        """Method that will automatically be called by the server when the real mode is requested."""
        self.simulation_mode = False
        self._inject_implementation(FluidDefinitionServiceReal(ser=self.ser, status=self.status))

    def GetFluidSetProperties(self, request, context: grpc.ServicerContext) \
            -> FluidDefinitionService_pb2.GetFluidSetProperties_Responses:
        """
        Executes the unobservable command "Get Fluidset properties"
            The current fluidset properties for the calculations performed by the program
    
        :param request: gRPC request containing the parameters passed:
            request.EmptyParameter (Empty Parameter): An empty parameter data type used if no parameter is required.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentFluidsetProperties (Current fluidset properties): The current fluidset properties for the calculations performed by the program
        """
    
        logging.debug(
            "GetFluidSetProperties called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetFluidSetProperties(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def SetFluidsetProperties(self, request, context: grpc.ServicerContext) \
            -> FluidDefinitionService_pb2.SetFluidsetProperties_Responses:
        """
        Executes the unobservable command "Set fluid properties"
            The fluidset properties to work with
    
        :param request: gRPC request containing the parameters passed:
            request.SetFluidProperties (Set Fluid set properties):
            The fluidset properties to work with
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.Status (Status): Command status: Request response of command set fluid properties
            request.IndexPointing (Index pointing):
            Index pointing to the first byte in the send message for which the Set fluid properties applies
        """
    
        logging.debug(
            "SetFluidsetProperties called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.SetFluidsetProperties(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetFluidNumber(self, request, context: grpc.ServicerContext) \
            -> FluidDefinitionService_pb2.GetFluidNumber_Responses:
        """
        Executes the unobservable command "Get fluid number"
            Current fluid ID-Nr.
    
        :param request: gRPC request containing the parameters passed:
            request.EmptyParameter (Empty Parameter): An empty parameter data type used if no parameter is required.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentFuidNumber (Current fluid number): Current fluid ID-Nr
        """
    
        logging.debug(
            "GetFluidNumber called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetFluidNumber(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def SetFluidNumber(self, request, context: grpc.ServicerContext) \
            -> FluidDefinitionService_pb2.SetFluidNumber_Responses:
        """
        Executes the unobservable command "Set Fluid Number"
            Set a number of the fluid to work with
    
        :param request: gRPC request containing the parameters passed:
            request.SetFluidNumber (Set fluid number):
            The number of the fluid to work with
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.Status (Status): Command status: Request response of command set fluid number
            request.IndexPointing (Index pointing):
            Index pointing to the first byte in the send message for which set fluid number applies
        """
    
        logging.debug(
            "SetFluidNumber called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.SetFluidNumber(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetFluidName(self, request, context: grpc.ServicerContext) \
            -> FluidDefinitionService_pb2.GetFluidName_Responses:
        """
        Executes the unobservable command "Get fluid name"
            The name of the current fluid
    
        :param request: gRPC request containing the parameters passed:
            request.EmptyParameter (Empty Parameter): An empty parameter data type used if no parameter is required.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentFluidName (Current fluid name): Current fluid name
        """
    
        logging.debug(
            "GetFluidName called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetFluidName(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def SetFluidName(self, request, context: grpc.ServicerContext) \
            -> FluidDefinitionService_pb2.SetFluidName_Responses:
        """
        Executes the unobservable command "Set Fluid Name"
            Set the name of the fluid to work with
    
        :param request: gRPC request containing the parameters passed:
            request.SetFluidName (Set fluid name):
            The name of the fluid to work with
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.Status (Status): Command status: Request response of command set fluid name
            request.IndexPointing (Index pointing):
            Index pointing to the first byte in the send message for which fluid name applies
        """
    
        logging.debug(
            "SetFluidName called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.SetFluidName(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetHeatCapacity(self, request, context: grpc.ServicerContext) \
            -> FluidDefinitionService_pb2.GetHeatCapacity_Responses:
        """
        Executes the unobservable command "Get heat capacity"
            The heat capacity of the fluid in Joule per K
    
        :param request: gRPC request containing the parameters passed:
            request.EmptyParameter (Empty Parameter): An empty parameter data type used if no parameter is required.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentHeatCapacity (Current heat capacity): Current heat capacity
        """
    
        logging.debug(
            "GetHeatCapacity called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetHeatCapacity(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def SetHeatCapacity(self, request, context: grpc.ServicerContext) \
            -> FluidDefinitionService_pb2.SetHeatCapacity_Responses:
        """
        Executes the unobservable command "Set heat capacity"
            Set the heat capacity of the fluid in Joule Per Kelvin
    
        :param request: gRPC request containing the parameters passed:
            request.SetHeatCapacity (Set heat capacity):
            Set the heat capacity of the fluid in Joule per Kelvin.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.Status (Status): Command status: Request response of command set heat capacity
            request.IndexPointing (Index pointing):
            Index pointing to the first byte in the send message for which set heat capacity applies
        """
    
        logging.debug(
            "SetHeatCapacity called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.SetHeatCapacity(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetViscosity(self, request, context: grpc.ServicerContext) \
            -> FluidDefinitionService_pb2.GetViscosity_Responses:
        """
        Executes the unobservable command "Get viscosity"
            Viscosity of the fluid in cP
    
        :param request: gRPC request containing the parameters passed:
            request.EmptyParameter (Empty Parameter): An empty parameter data type used if no parameter is required.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentViscosity (Current viscosity): Current viscosity in cP
        """
    
        logging.debug(
            "GetViscosity called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetViscosity(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def SetViscosity(self, request, context: grpc.ServicerContext) \
            -> FluidDefinitionService_pb2.SetViscosity_Responses:
        """
        Executes the unobservable command "Set viscosity"
            Set the viscosity of the fluid in cP
    
        :param request: gRPC request containing the parameters passed:
            request.SetViscosity (Set viscosity):
            Set the viscosity of the fluid in cP
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.Status (Status): Command status: Request response of command set viscosity
            request.IndexPointing (Index pointing):
            Index pointing to the first byte in the send message for which set viscosity applies
        """
    
        logging.debug(
            "SetViscosity called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.SetViscosity(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)

    

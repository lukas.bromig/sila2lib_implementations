# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: DeviceController.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


import sila2lib.framework.SiLAFramework_pb2 as SiLAFramework__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='DeviceController.proto',
  package='sila2.biovt.mw.tum.de.examples.devicecontroller.v1',
  syntax='proto3',
  serialized_options=None,
  serialized_pb=_b('\n\x16\x44\x65viceController.proto\x12\x32sila2.biovt.mw.tum.de.examples.devicecontroller.v1\x1a\x13SiLAFramework.proto\"\x16\n\x14GetReport_Parameters\"L\n\x13GetReport_Responses\x12\x35\n\rCurrentReport\x18\x01 \x03(\x0b\x32\x1e.sila2.org.silastandard.String\"\x15\n\x13GetTComp_Parameters\"H\n\x12GetTComp_Responses\x12\x32\n\x0c\x43urrentTComp\x18\x01 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real\"E\n\x13SetTComp_Parameters\x12.\n\x08SetTComp\x18\x01 \x03(\x0b\x32\x1c.sila2.org.silastandard.Real\"H\n\x12SetTComp_Responses\x12\x32\n\x0c\x43urrentTComp\x18\x01 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real\"\x1c\n\x1aGetDynAveraging_Parameters\"W\n\x19GetDynAveraging_Responses\x12:\n\x11\x43urrentDynAverage\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"Q\n\x1aSetDynAveraging_Parameters\x12\x33\n\rSetDynAverage\x18\x01 \x03(\x0b\x32\x1c.sila2.org.silastandard.Real\"W\n\x19SetDynAveraging_Responses\x12:\n\x11\x43urrentDynAverage\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"\x1c\n\x1aSwitchOffDevice_Parameters\"R\n\x19SwitchOffDevice_Responses\x12\x35\n\rCurrentStatus\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.String2\x94\x08\n\x10\x44\x65viceController\x12\xa0\x01\n\tGetReport\x12H.sila2.biovt.mw.tum.de.examples.devicecontroller.v1.GetReport_Parameters\x1aG.sila2.biovt.mw.tum.de.examples.devicecontroller.v1.GetReport_Responses\"\x00\x12\x9d\x01\n\x08GetTComp\x12G.sila2.biovt.mw.tum.de.examples.devicecontroller.v1.GetTComp_Parameters\x1a\x46.sila2.biovt.mw.tum.de.examples.devicecontroller.v1.GetTComp_Responses\"\x00\x12\x9d\x01\n\x08SetTComp\x12G.sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SetTComp_Parameters\x1a\x46.sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SetTComp_Responses\"\x00\x12\xb2\x01\n\x0fGetDynAveraging\x12N.sila2.biovt.mw.tum.de.examples.devicecontroller.v1.GetDynAveraging_Parameters\x1aM.sila2.biovt.mw.tum.de.examples.devicecontroller.v1.GetDynAveraging_Responses\"\x00\x12\xb2\x01\n\x0fSetDynAveraging\x12N.sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SetDynAveraging_Parameters\x1aM.sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SetDynAveraging_Responses\"\x00\x12\xb2\x01\n\x0fSwitchOffDevice\x12N.sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SwitchOffDevice_Parameters\x1aM.sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SwitchOffDevice_Responses\"\x00\x62\x06proto3')
  ,
  dependencies=[SiLAFramework__pb2.DESCRIPTOR,])




_GETREPORT_PARAMETERS = _descriptor.Descriptor(
  name='GetReport_Parameters',
  full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.GetReport_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=99,
  serialized_end=121,
)


_GETREPORT_RESPONSES = _descriptor.Descriptor(
  name='GetReport_Responses',
  full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.GetReport_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentReport', full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.GetReport_Responses.CurrentReport', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=123,
  serialized_end=199,
)


_GETTCOMP_PARAMETERS = _descriptor.Descriptor(
  name='GetTComp_Parameters',
  full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.GetTComp_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=201,
  serialized_end=222,
)


_GETTCOMP_RESPONSES = _descriptor.Descriptor(
  name='GetTComp_Responses',
  full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.GetTComp_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentTComp', full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.GetTComp_Responses.CurrentTComp', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=224,
  serialized_end=296,
)


_SETTCOMP_PARAMETERS = _descriptor.Descriptor(
  name='SetTComp_Parameters',
  full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SetTComp_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='SetTComp', full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SetTComp_Parameters.SetTComp', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=298,
  serialized_end=367,
)


_SETTCOMP_RESPONSES = _descriptor.Descriptor(
  name='SetTComp_Responses',
  full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SetTComp_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentTComp', full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SetTComp_Responses.CurrentTComp', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=369,
  serialized_end=441,
)


_GETDYNAVERAGING_PARAMETERS = _descriptor.Descriptor(
  name='GetDynAveraging_Parameters',
  full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.GetDynAveraging_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=443,
  serialized_end=471,
)


_GETDYNAVERAGING_RESPONSES = _descriptor.Descriptor(
  name='GetDynAveraging_Responses',
  full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.GetDynAveraging_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentDynAverage', full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.GetDynAveraging_Responses.CurrentDynAverage', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=473,
  serialized_end=560,
)


_SETDYNAVERAGING_PARAMETERS = _descriptor.Descriptor(
  name='SetDynAveraging_Parameters',
  full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SetDynAveraging_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='SetDynAverage', full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SetDynAveraging_Parameters.SetDynAverage', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=562,
  serialized_end=643,
)


_SETDYNAVERAGING_RESPONSES = _descriptor.Descriptor(
  name='SetDynAveraging_Responses',
  full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SetDynAveraging_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentDynAverage', full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SetDynAveraging_Responses.CurrentDynAverage', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=645,
  serialized_end=732,
)


_SWITCHOFFDEVICE_PARAMETERS = _descriptor.Descriptor(
  name='SwitchOffDevice_Parameters',
  full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SwitchOffDevice_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=734,
  serialized_end=762,
)


_SWITCHOFFDEVICE_RESPONSES = _descriptor.Descriptor(
  name='SwitchOffDevice_Responses',
  full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SwitchOffDevice_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentStatus', full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SwitchOffDevice_Responses.CurrentStatus', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=764,
  serialized_end=846,
)

_GETREPORT_RESPONSES.fields_by_name['CurrentReport'].message_type = SiLAFramework__pb2._STRING
_GETTCOMP_RESPONSES.fields_by_name['CurrentTComp'].message_type = SiLAFramework__pb2._REAL
_SETTCOMP_PARAMETERS.fields_by_name['SetTComp'].message_type = SiLAFramework__pb2._REAL
_SETTCOMP_RESPONSES.fields_by_name['CurrentTComp'].message_type = SiLAFramework__pb2._REAL
_GETDYNAVERAGING_RESPONSES.fields_by_name['CurrentDynAverage'].message_type = SiLAFramework__pb2._INTEGER
_SETDYNAVERAGING_PARAMETERS.fields_by_name['SetDynAverage'].message_type = SiLAFramework__pb2._REAL
_SETDYNAVERAGING_RESPONSES.fields_by_name['CurrentDynAverage'].message_type = SiLAFramework__pb2._INTEGER
_SWITCHOFFDEVICE_RESPONSES.fields_by_name['CurrentStatus'].message_type = SiLAFramework__pb2._STRING
DESCRIPTOR.message_types_by_name['GetReport_Parameters'] = _GETREPORT_PARAMETERS
DESCRIPTOR.message_types_by_name['GetReport_Responses'] = _GETREPORT_RESPONSES
DESCRIPTOR.message_types_by_name['GetTComp_Parameters'] = _GETTCOMP_PARAMETERS
DESCRIPTOR.message_types_by_name['GetTComp_Responses'] = _GETTCOMP_RESPONSES
DESCRIPTOR.message_types_by_name['SetTComp_Parameters'] = _SETTCOMP_PARAMETERS
DESCRIPTOR.message_types_by_name['SetTComp_Responses'] = _SETTCOMP_RESPONSES
DESCRIPTOR.message_types_by_name['GetDynAveraging_Parameters'] = _GETDYNAVERAGING_PARAMETERS
DESCRIPTOR.message_types_by_name['GetDynAveraging_Responses'] = _GETDYNAVERAGING_RESPONSES
DESCRIPTOR.message_types_by_name['SetDynAveraging_Parameters'] = _SETDYNAVERAGING_PARAMETERS
DESCRIPTOR.message_types_by_name['SetDynAveraging_Responses'] = _SETDYNAVERAGING_RESPONSES
DESCRIPTOR.message_types_by_name['SwitchOffDevice_Parameters'] = _SWITCHOFFDEVICE_PARAMETERS
DESCRIPTOR.message_types_by_name['SwitchOffDevice_Responses'] = _SWITCHOFFDEVICE_RESPONSES
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

GetReport_Parameters = _reflection.GeneratedProtocolMessageType('GetReport_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GETREPORT_PARAMETERS,
  '__module__' : 'DeviceController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.devicecontroller.v1.GetReport_Parameters)
  })
_sym_db.RegisterMessage(GetReport_Parameters)

GetReport_Responses = _reflection.GeneratedProtocolMessageType('GetReport_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GETREPORT_RESPONSES,
  '__module__' : 'DeviceController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.devicecontroller.v1.GetReport_Responses)
  })
_sym_db.RegisterMessage(GetReport_Responses)

GetTComp_Parameters = _reflection.GeneratedProtocolMessageType('GetTComp_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GETTCOMP_PARAMETERS,
  '__module__' : 'DeviceController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.devicecontroller.v1.GetTComp_Parameters)
  })
_sym_db.RegisterMessage(GetTComp_Parameters)

GetTComp_Responses = _reflection.GeneratedProtocolMessageType('GetTComp_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GETTCOMP_RESPONSES,
  '__module__' : 'DeviceController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.devicecontroller.v1.GetTComp_Responses)
  })
_sym_db.RegisterMessage(GetTComp_Responses)

SetTComp_Parameters = _reflection.GeneratedProtocolMessageType('SetTComp_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _SETTCOMP_PARAMETERS,
  '__module__' : 'DeviceController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SetTComp_Parameters)
  })
_sym_db.RegisterMessage(SetTComp_Parameters)

SetTComp_Responses = _reflection.GeneratedProtocolMessageType('SetTComp_Responses', (_message.Message,), {
  'DESCRIPTOR' : _SETTCOMP_RESPONSES,
  '__module__' : 'DeviceController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SetTComp_Responses)
  })
_sym_db.RegisterMessage(SetTComp_Responses)

GetDynAveraging_Parameters = _reflection.GeneratedProtocolMessageType('GetDynAveraging_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GETDYNAVERAGING_PARAMETERS,
  '__module__' : 'DeviceController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.devicecontroller.v1.GetDynAveraging_Parameters)
  })
_sym_db.RegisterMessage(GetDynAveraging_Parameters)

GetDynAveraging_Responses = _reflection.GeneratedProtocolMessageType('GetDynAveraging_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GETDYNAVERAGING_RESPONSES,
  '__module__' : 'DeviceController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.devicecontroller.v1.GetDynAveraging_Responses)
  })
_sym_db.RegisterMessage(GetDynAveraging_Responses)

SetDynAveraging_Parameters = _reflection.GeneratedProtocolMessageType('SetDynAveraging_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _SETDYNAVERAGING_PARAMETERS,
  '__module__' : 'DeviceController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SetDynAveraging_Parameters)
  })
_sym_db.RegisterMessage(SetDynAveraging_Parameters)

SetDynAveraging_Responses = _reflection.GeneratedProtocolMessageType('SetDynAveraging_Responses', (_message.Message,), {
  'DESCRIPTOR' : _SETDYNAVERAGING_RESPONSES,
  '__module__' : 'DeviceController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SetDynAveraging_Responses)
  })
_sym_db.RegisterMessage(SetDynAveraging_Responses)

SwitchOffDevice_Parameters = _reflection.GeneratedProtocolMessageType('SwitchOffDevice_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _SWITCHOFFDEVICE_PARAMETERS,
  '__module__' : 'DeviceController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SwitchOffDevice_Parameters)
  })
_sym_db.RegisterMessage(SwitchOffDevice_Parameters)

SwitchOffDevice_Responses = _reflection.GeneratedProtocolMessageType('SwitchOffDevice_Responses', (_message.Message,), {
  'DESCRIPTOR' : _SWITCHOFFDEVICE_RESPONSES,
  '__module__' : 'DeviceController_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.devicecontroller.v1.SwitchOffDevice_Responses)
  })
_sym_db.RegisterMessage(SwitchOffDevice_Responses)



_DEVICECONTROLLER = _descriptor.ServiceDescriptor(
  name='DeviceController',
  full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.DeviceController',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  serialized_start=849,
  serialized_end=1893,
  methods=[
  _descriptor.MethodDescriptor(
    name='GetReport',
    full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.DeviceController.GetReport',
    index=0,
    containing_service=None,
    input_type=_GETREPORT_PARAMETERS,
    output_type=_GETREPORT_RESPONSES,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='GetTComp',
    full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.DeviceController.GetTComp',
    index=1,
    containing_service=None,
    input_type=_GETTCOMP_PARAMETERS,
    output_type=_GETTCOMP_RESPONSES,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='SetTComp',
    full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.DeviceController.SetTComp',
    index=2,
    containing_service=None,
    input_type=_SETTCOMP_PARAMETERS,
    output_type=_SETTCOMP_RESPONSES,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='GetDynAveraging',
    full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.DeviceController.GetDynAveraging',
    index=3,
    containing_service=None,
    input_type=_GETDYNAVERAGING_PARAMETERS,
    output_type=_GETDYNAVERAGING_RESPONSES,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='SetDynAveraging',
    full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.DeviceController.SetDynAveraging',
    index=4,
    containing_service=None,
    input_type=_SETDYNAVERAGING_PARAMETERS,
    output_type=_SETDYNAVERAGING_RESPONSES,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='SwitchOffDevice',
    full_name='sila2.biovt.mw.tum.de.examples.devicecontroller.v1.DeviceController.SwitchOffDevice',
    index=5,
    containing_service=None,
    input_type=_SWITCHOFFDEVICE_PARAMETERS,
    output_type=_SWITCHOFFDEVICE_RESPONSES,
    serialized_options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_DEVICECONTROLLER)

DESCRIPTOR.services_by_name['DeviceController'] = _DEVICECONTROLLER

# @@protoc_insertion_point(module_scope)

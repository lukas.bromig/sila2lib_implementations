from __future__ import annotations

from typing import TYPE_CHECKING

from sila2.client import SilaClient

from .calibrationservice import CalibrationServiceFeature, SerialConnectionError, SerialResponseError
from .configurationservice import (
    ConfigurationServiceFeature,
    SerialConnectionError,
    SerialResponseError,
    SetBackstepsInvalidInput,
    TubingDiameterInvalidInput,
)
from .deviceservice import DeviceServiceFeature, SerialConnectionError, SerialResponseError
from .drivecontroller import ChannelSettingsError, DriveControllerFeature, SerialConnectionError, SerialResponseError
from .parametercontroller import ParameterControllerFeature, SerialConnectionError, SerialResponseError

if TYPE_CHECKING:

    from .calibrationservice import CalibrationServiceClient
    from .configurationservice import ConfigurationServiceClient
    from .deviceservice import DeviceServiceClient
    from .drivecontroller import DriveControllerClient
    from .parametercontroller import ParameterControllerClient


class Client(SilaClient):

    CalibrationService: CalibrationServiceClient

    ConfigurationService: ConfigurationServiceClient

    DeviceService: DeviceServiceClient

    DriveController: DriveControllerClient

    ParameterController: ParameterControllerClient

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._register_defined_execution_error_class(
            CalibrationServiceFeature.defined_execution_errors["SerialResponseError"], SerialResponseError
        )

        self._register_defined_execution_error_class(
            CalibrationServiceFeature.defined_execution_errors["SerialConnectionError"], SerialConnectionError
        )

        self._register_defined_execution_error_class(
            ConfigurationServiceFeature.defined_execution_errors["TubingDiameterInvalidInput"],
            TubingDiameterInvalidInput,
        )

        self._register_defined_execution_error_class(
            ConfigurationServiceFeature.defined_execution_errors["SetBackstepsInvalidInput"], SetBackstepsInvalidInput
        )

        self._register_defined_execution_error_class(
            ConfigurationServiceFeature.defined_execution_errors["SerialResponseError"], SerialResponseError
        )

        self._register_defined_execution_error_class(
            ConfigurationServiceFeature.defined_execution_errors["SerialConnectionError"], SerialConnectionError
        )

        self._register_defined_execution_error_class(
            DeviceServiceFeature.defined_execution_errors["SerialResponseError"], SerialResponseError
        )

        self._register_defined_execution_error_class(
            DeviceServiceFeature.defined_execution_errors["SerialConnectionError"], SerialConnectionError
        )

        self._register_defined_execution_error_class(
            DriveControllerFeature.defined_execution_errors["SerialResponseError"], SerialResponseError
        )

        self._register_defined_execution_error_class(
            DriveControllerFeature.defined_execution_errors["SerialConnectionError"], SerialConnectionError
        )

        self._register_defined_execution_error_class(
            DriveControllerFeature.defined_execution_errors["ChannelSettingsError"], ChannelSettingsError
        )

        self._register_defined_execution_error_class(
            ParameterControllerFeature.defined_execution_errors["SerialResponseError"], SerialResponseError
        )

        self._register_defined_execution_error_class(
            ParameterControllerFeature.defined_execution_errors["SerialConnectionError"], SerialConnectionError
        )

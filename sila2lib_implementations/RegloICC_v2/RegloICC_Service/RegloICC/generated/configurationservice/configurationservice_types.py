from __future__ import annotations

from typing import NamedTuple


class SetTubingDiameter_Responses(NamedTuple):

    TubingDiameterSet: str
    """
    Tubing Diameter succeeded to Set.
    """


class ResetToDefault_Responses(NamedTuple):

    ResetStatus: str
    """
    Reset of pump succeeded.
    """


class GetTubingDiameter_Responses(NamedTuple):

    CurrentTubingDiameter: float
    """
    Current the inside diameter of the tubing in mm.
    """


class SetBacksteps_Responses(NamedTuple):

    BackstepsSettingSet: str
    """
    Backsteps Setting succeeded to Set.
    """


class GetBacksteps_Responses(NamedTuple):

    CurrentBackstepsSetting: int
    """
    Get the current backsteps  setting.
    """

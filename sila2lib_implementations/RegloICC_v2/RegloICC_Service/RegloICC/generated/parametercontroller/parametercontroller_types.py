from __future__ import annotations

from typing import NamedTuple


class SetFlowRate_Responses(NamedTuple):

    SetFlowRateSet: float
    """
    Set Flow Rate succeeded to Set.
    """


class SetRPMMode_Responses(NamedTuple):

    PumpModeSet: str
    """
    Pump mode succeeded to Set.
    """


class SetFlowRateMode_Responses(NamedTuple):

    PumpModeSet: str
    """
    Pump mode succeeded to Set.
    """


class SetVolumeRateMode_Responses(NamedTuple):

    PumpModeSet: str
    """
    Pump mode succeeded to Set.
    """


class SetVolumeTimeMode_Responses(NamedTuple):

    PumpModeSet: str
    """
    Pump mode succeeded to Set.
    """


class SetVolumePauseMode_Responses(NamedTuple):

    PumpModeSet: str
    """
    Pump mode succeeded to Set.
    """


class SetTimeMode_Responses(NamedTuple):

    PumpModeSet: str
    """
    Pump mode succeeded to Set.
    """


class SetTimePauseMode_Responses(NamedTuple):

    PumpModeSet: str
    """
    Pump mode succeeded to Set.
    """


class SetVolume_Responses(NamedTuple):

    VolumeSet: str
    """
    Volume succeeded to Set
    """


class GetFlowRate_Responses(NamedTuple):

    CurrentFlowRate: float
    """
    Current flow rate of the channel.
    """


class GetMode_Responses(NamedTuple):

    CurrentPumpMode: str
    """
    Current channel or pump mode is retrieved.
    """


class GetVolume_Responses(NamedTuple):

    CurrentVolume: float
    """
    Current setting for volume in mL/min.
    """


class GetMaximumFlowRate_Responses(NamedTuple):

    MaximumFlowRate: float
    """
    Maximum Flow Rate achievable with current settings, mL/min.
    """


class GetMaximumFlowRateWithCalibration_Responses(NamedTuple):

    MaximumFlowRateWithCalibration: float
    """
    Maximum Flow Rate achievable with current settings using calibration, mL/min.
    """


class GetSpeedSettingRPM_Responses(NamedTuple):

    CurrentSpeedSetting: float
    """
    The current speed setting in RPM.
    """


class SetSpeedSettingRPM_Responses(NamedTuple):

    CurrentSpeedSetting: str
    """
    RPM Mode flow rate setting.
    """


class SetCurrentRunTime_Responses(NamedTuple):

    SetCurrentRunTimeSet: str
    """
    Current Run Time succeeded to set.
    """


class GetCurrentRunTime_Responses(NamedTuple):

    CurrentRunTime: int
    """
    Current Run Time of the pump.
    """


class SetPumpingPauseTime_Responses(NamedTuple):

    SetPumpingPauseTimeSet: str
    """
    Pumping pause time succeeded to set.
    """


class GetPumpingPauseTime_Responses(NamedTuple):

    CurrentPumpingPauseTime: float
    """
    Current Pumping Pause Time.
    """


class GetCycleCount_Responses(NamedTuple):

    CurrentCycleCount: float
    """
    Current Cycle Count.
    """


class SetCycleCount_Responses(NamedTuple):

    SetCycleCountSet: str
    """
    Cycle Count succeeded to set.
    """


class GetDispenseTimeMlMin_Responses(NamedTuple):

    CurrentDispenseTime: float
    """
    CurrentDispenseTime at a given volume at a given mL/min flow rate.
    """


class GetDispenseTimeRPM_Responses(NamedTuple):

    CurrentDispenseTime: float
    """
    CurrentDispenseTime at a given volume at a given RPM.
    """


class GetFlowRateAtModes_Responses(NamedTuple):

    CurrentFlowRate: bool
    """
    CurrentDispenseTime at a given volume at a given RPM. : 0=RPM, 1=Flow Rate
    """


class SetFlowRateAtModes_Responses(NamedTuple):

    SetFlowRateSet: str
    """
    RPM flow rate not in RPM or Flow Rate mode succeeded to set
    """

from os.path import dirname, join

from sila2.framework import Feature

ParameterControllerFeature = Feature(open(join(dirname(__file__), "ParameterController.sila.xml")).read())

from __future__ import annotations

from typing import Iterable, Optional

from drivecontroller_types import (
    GetCauseResponse_Responses,
    GetPumpDirection_Responses,
    PausePump_Responses,
    SetDirectionClockwise_Responses,
    SetDirectionCounterClockwise_Responses,
    StartPump_Responses,
    StopPump_Responses,
)

from sila2.client import ClientMetadataInstance

class DriveControllerClient:
    """

            Set and retrieve information regarding the pump drive.
    By Valeryia Sidarava, Institute of Biochemical Engineering, Technical University of Munich, 31.07.2019,
    Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 15.04.2020.
    Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 28.01.2022.

    """

    def StartPump(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> StartPump_Responses:
        """

        Starts the pump out of stand-by mode.

        """
        ...
    def StopPump(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> StopPump_Responses:
        """

        Stops the pump.

        """
        ...
    def PausePump(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> PausePump_Responses:
        """

        Pause pumping (STOP in RPM or flow rate mode).

        """
        ...
    def GetPumpDirection(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetPumpDirection_Responses:
        """
        Get pump direction (J for CW or K for CCW).
        """
        ...
    def SetDirectionClockwise(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetDirectionClockwise_Responses:
        """
        Set the rotation direction of the pump to clockwise
        """
        ...
    def SetDirectionCounterClockwise(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetDirectionCounterClockwise_Responses:
        """
        Set the rotation direction of the pump to counter-clockwise
        """
        ...
    def GetCauseResponse(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetCauseResponse_Responses:
        """
        Get cause of "-" cannot run response = Parameter 1 :
                      C=Cycle count of 0 / R=Max flow rate exceeded or flow rate is set to 0 / V=Max volume exceeded;
                      Limiting value that was exceeded = Parameter 2:
                    C=Value is undefined / R=Max flow (mL/min) / V=Max volume (mL)   .
        """
        ...

from __future__ import annotations

from typing import NamedTuple


class StartPump_Responses(NamedTuple):

    StartStatus: str
    """
    Start of pump succeeded.
    """


class StopPump_Responses(NamedTuple):

    StopStatus: str
    """
    Shut-down of pump succeeded.
    """


class PausePump_Responses(NamedTuple):

    PauseStatus: str
    """
    Pausing of pump succeeded.
    """


class GetPumpDirection_Responses(NamedTuple):

    PumpDirection: str
    """
    Pump direction query. J = clockwise/ K = counter-clockwise. 
    """


class SetDirectionClockwise_Responses(NamedTuple):

    SetDirectionSet: str
    """
    Set Direction succeeded to Set.
    """


class SetDirectionCounterClockwise_Responses(NamedTuple):

    SetDirectionSet: str
    """
    Set Direction succeeded to Set.
    """


class GetCauseResponse_Responses(NamedTuple):

    Cause: str
    """
    Cause displayed.
    """

from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Any, Dict

from sila2.framework import FullyQualifiedIdentifier
from sila2.server import FeatureImplementationBase

from .calibrationservice_types import (
    CancelCalibration_Responses,
    GetCalibrationTime_Responses,
    GetDirectionCalibration_Responses,
    GetRunTimeCalibration_Responses,
    GetTargetVolume_Responses,
    SetActualVolume_Responses,
    SetCalibrationTime_Responses,
    SetDirectionCalibration_Responses,
    SetTargetVolume_Responses,
    StartCalibration_Responses,
)


class CalibrationServiceBase(FeatureImplementationBase, ABC):

    """

            Set and retrieve the control parameter values for calibration of the Reglo ICC pump.
    By Valeryia Sidarava, Institute of Biochemical Engineering, Technical University of Munich, 31.07.2019,
    Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 15.04.2020.
    Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 28.01.2022.

    """

    @abstractmethod
    def StartCalibration(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> StartCalibration_Responses:
        """

        Starts calibration on a channel(s).



        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - StartCalibrationStatus: Calibration succeeded to start.


        """
        pass

    @abstractmethod
    def CancelCalibration(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> CancelCalibration_Responses:
        """

        Cancels calibration.



        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - CancelCalibrationStatus: Calibration succeeded to cancel.


        """
        pass

    @abstractmethod
    def SetTargetVolume(
        self, Channel: int, TargetVolume: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetTargetVolume_Responses:
        """

        Set target volume to pump for calibrating in mL (using Volume Type 1).



        :param Channel:
                The channel (1-4).


        :param TargetVolume:
              The target volume in mL (using Volume Type 1).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - TargetVolumeSet: Target Volume succeeded to Set. Returns the target volume using Volume Type 1.


        """
        pass

    @abstractmethod
    def SetActualVolume(
        self, Channel: int, ActualVolume: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetActualVolume_Responses:
        """

        Set the actual volume measured during calibration in mL (using Volume Type 1).



        :param Channel:
                The channel (1-4).


        :param ActualVolume:
                The actual volume measured during calibration in mL (using Volume Type 1).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - ActualVolumeSet: Actual Volume succeeded to Set. Returns the actual volume in mL (using Volume Type 1).


        """
        pass

    @abstractmethod
    def GetTargetVolume(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetTargetVolume_Responses:
        """
        Get target volume to pump for calibrating in mL.


        :param TargetVolume:
              Get target volume of pump for calibrating in mL (Volume Type 1).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - CurrentTargetVolume: Returns the current target volume to pump for calibrating in mL (using Volume Type 1).


        """
        pass

    @abstractmethod
    def SetDirectionCalibration(
        self, Channel: int, Direction: str, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetDirectionCalibration_Responses:
        """

        Set direction flow for calibration J or K using DIRECTION format.



        :param Channel:
                The channel (1-4).


        :param Direction:
            The direction flow for calibration (J for CW or K fo CCW) using DIRECTION format.


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - SetDirectionCalibrationSet: The direction flow for Calibration succeeded to Set.


        """
        pass

    @abstractmethod
    def GetDirectionCalibration(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetDirectionCalibration_Responses:
        """

            Get direction flow for calibration (J for CW or K for CCW).



        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - CurrentDirectionCalibration: The current direction flow for Calibration (J for CW or K for CCW).


        """
        pass

    @abstractmethod
    def SetCalibrationTime(
        self, Channel: int, Time: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetCalibrationTime_Responses:
        """

        Set the current calibration time using Time Type 2 format (unit 0.1 s, 0-35964000, or 0 to 999 hr) .



        :param Channel:
                    The channel (1-4).


        :param Time:
            Set the current calibration time using Time Type 2 format.


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - SetCalibrationTimeSet: Calibration Time succeeded to Set.


        """
        pass

    @abstractmethod
    def GetCalibrationTime(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetCalibrationTime_Responses:
        """

            Get the current calibration time in 0.1 s (Time Type 1).



        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - CurrentCalibrationTime: The current calibration time in 0.1 s (Time Type 1).


        """
        pass

    @abstractmethod
    def GetRunTimeCalibration(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetRunTimeCalibration_Responses:
        """

            Get the channel run time since last calibration in 0.1 s (Time Type 2).



        :param Channel:
                    The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - CurrentRunTimeCalibration:
                Channel run time since last calibration in 0.1 s (Time Type 2).



        """
        pass

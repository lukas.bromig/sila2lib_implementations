from __future__ import annotations

from typing import NamedTuple


class StartCalibration_Responses(NamedTuple):

    StartCalibrationStatus: str
    """
    Calibration succeeded to start.
    """


class CancelCalibration_Responses(NamedTuple):

    CancelCalibrationStatus: str
    """
    Calibration succeeded to cancel.
    """


class SetTargetVolume_Responses(NamedTuple):

    TargetVolumeSet: float
    """
    Target Volume succeeded to Set. Returns the target volume using Volume Type 1.
    """


class SetActualVolume_Responses(NamedTuple):

    ActualVolumeSet: float
    """
    Actual Volume succeeded to Set. Returns the actual volume in mL (using Volume Type 1).
    """


class GetTargetVolume_Responses(NamedTuple):

    CurrentTargetVolume: float
    """
    Returns the current target volume to pump for calibrating in mL (using Volume Type 1).
    """


class SetDirectionCalibration_Responses(NamedTuple):

    SetDirectionCalibrationSet: str
    """
    The direction flow for Calibration succeeded to Set.
    """


class GetDirectionCalibration_Responses(NamedTuple):

    CurrentDirectionCalibration: str
    """
    The current direction flow for Calibration (J for CW or K for CCW).
    """


class SetCalibrationTime_Responses(NamedTuple):

    SetCalibrationTimeSet: str
    """
    Calibration Time succeeded to Set.
    """


class GetCalibrationTime_Responses(NamedTuple):

    CurrentCalibrationTime: int
    """
    The current calibration time in 0.1 s (Time Type 1).
    """


class GetRunTimeCalibration_Responses(NamedTuple):

    CurrentRunTimeCalibration: int
    """
    
                Channel run time since last calibration in 0.1 s (Time Type 2).
            
    """

<?xml version="1.0" encoding="utf-8" ?>
<Feature SiLA2Version="1.0" FeatureVersion="1.0" Originator="org.silastandard" Category="examples"
         xmlns="http://www.sila-standard.org"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://www.sila-standard.org https://gitlab.com/SiLA2/sila_features/raw/master/schema/FeatureDefinition.xsd">
    <Identifier>CalibrationService</Identifier>
    <DisplayName>Calibration Service</DisplayName>
    <Description>
		Set and retrieve the control parameter values for calibration of the Reglo ICC pump.
        By Valeryia Sidarava, Institute of Biochemical Engineering, Technical University of Munich, 31.07.2019,
        Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 15.04.2020.
        Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 28.01.2022.
    </Description>
    <Command>
        <Identifier>StartCalibration</Identifier>
        <DisplayName>Start Calibration on a Channel</DisplayName>
        <Description>
        Starts calibration on a channel(s).
        </Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>Channel</Identifier>
            <DisplayName>Channel</DisplayName>
            <Description>
                The channel (1-4).
            </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                    <Constraints>
                        <MaximalInclusive>4</MaximalInclusive>
                        <MinimalInclusive>1</MinimalInclusive>
                    </Constraints>
                </Constrained>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>StartCalibrationStatus</Identifier>
            <DisplayName>Start Calibration Status</DisplayName>
            <Description>Calibration succeeded to start.</Description>
            <DataType>
                <Basic>String</Basic>
            </DataType>
        </Response>
        <DefinedExecutionErrors>
            <Identifier>SerialResponseError</Identifier>
            <Identifier>SerialConnectionError</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <Command>
        <Identifier>CancelCalibration</Identifier>
        <DisplayName>Cancel Calibration on a Channel</DisplayName>
        <Description>
        Cancels calibration.
        </Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>Channel</Identifier>
            <DisplayName>Channel</DisplayName>
            <Description>
                The channel (1-4).
            </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                    <Constraints>
                        <MaximalInclusive>4</MaximalInclusive>
                        <MinimalInclusive>1</MinimalInclusive>
                    </Constraints>
                </Constrained>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CancelCalibrationStatus</Identifier>
            <DisplayName>Cancel Calibration Status</DisplayName>
            <Description>Calibration succeeded to cancel.</Description>
            <DataType>
                <Basic>String</Basic>
            </DataType>
        </Response>
        <DefinedExecutionErrors>
            <Identifier>SerialResponseError</Identifier>
            <Identifier>SerialConnectionError</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <Command>
        <Identifier>SetTargetVolume</Identifier>
        <DisplayName>Set Target Volume Value</DisplayName>
        <Description>
        Set target volume to pump for calibrating in mL (using Volume Type 1).
        </Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>Channel</Identifier>
            <DisplayName>Channel</DisplayName>
            <Description>
                The channel (1-4).
            </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                    <Constraints>
                        <MaximalInclusive>4</MaximalInclusive>
                        <MinimalInclusive>1</MinimalInclusive>
                    </Constraints>
                </Constrained>
            </DataType>
        </Parameter>
        <Parameter>
            <Identifier>TargetVolume</Identifier>
            <DisplayName>Target Volume</DisplayName>
            <Description>
              The target volume in mL (using Volume Type 1).
            </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Real</Basic>
                    </DataType>
                    <Constraints>
                    </Constraints>
                </Constrained>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>TargetVolumeSet</Identifier>
            <DisplayName>Target Volume Set</DisplayName>
            <Description>Target Volume succeeded to Set. Returns the target volume using Volume Type 1.</Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
        <DefinedExecutionErrors>
            <Identifier>SerialResponseError</Identifier>
            <Identifier>SerialConnectionError</Identifier>
        </DefinedExecutionErrors>
	</Command>
    <Command>
        <Identifier>SetActualVolume</Identifier>
        <DisplayName>Set Actual Volume Value</DisplayName>
        <Description>
        Set the actual volume measured during calibration in mL (using Volume Type 1).
        </Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>Channel</Identifier>
            <DisplayName>Channel</DisplayName>
            <Description>
                The channel (1-4).
            </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                    <Constraints>
                        <MaximalInclusive>4</MaximalInclusive>
                        <MinimalInclusive>1</MinimalInclusive>
                    </Constraints>
                </Constrained>
            </DataType>
        </Parameter>
        <Parameter>
            <Identifier>ActualVolume</Identifier>
            <DisplayName>Actual Volume</DisplayName>
            <Description>
                The actual volume measured during calibration in mL (using Volume Type 1).
            </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Real</Basic>
                    </DataType>
                    <Constraints>
                    </Constraints>
                </Constrained>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>ActualVolumeSet</Identifier>
            <DisplayName>Actual Volume Set</DisplayName>
            <Description>Actual Volume succeeded to Set. Returns the actual volume in mL (using Volume Type 1).</Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
        <DefinedExecutionErrors>
            <Identifier>SerialResponseError</Identifier>
            <Identifier>SerialConnectionError</Identifier>
        </DefinedExecutionErrors>
    </Command>
	<Command>
        <Identifier>GetTargetVolume</Identifier>
        <DisplayName>Get Target Volume</DisplayName>
        <Description>Get target volume to pump for calibrating in mL.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>Channel</Identifier>
            <DisplayName>Channel</DisplayName>
            <Description>
                The channel (1-4).
            </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                    <Constraints>
                        <MaximalInclusive>4</MaximalInclusive>
                        <MinimalInclusive>1</MinimalInclusive>
                    </Constraints>
                </Constrained>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentTargetVolume</Identifier>
            <DisplayName>Current Target Volume</DisplayName>
            <Description>Returns the current target volume to pump for calibrating in mL (using Volume Type 1).</Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
        <DefinedExecutionErrors>
            <Identifier>SerialResponseError</Identifier>
            <Identifier>SerialConnectionError</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <Command>
        <Identifier>SetDirectionCalibration</Identifier>
        <DisplayName>Set Direction Flow for Calibration</DisplayName>
        <Description>
        Set direction flow for calibration J or K using DIRECTION format.
        </Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>Channel</Identifier>
            <DisplayName>Channel</DisplayName>
            <Description>
                The channel (1-4).
            </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                    <Constraints>
                        <MaximalInclusive>4</MaximalInclusive>
                        <MinimalInclusive>1</MinimalInclusive>
                    </Constraints>
                </Constrained>
            </DataType>
        </Parameter>
        <Parameter>
            <Identifier>Direction</Identifier>
            <DisplayName>Direction</DisplayName>
            <Description>
            The direction flow for calibration (J for CW or K fo CCW) using DIRECTION format.
            </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>String</Basic>
                    </DataType>
                    <Constraints>
                        <Pattern>^[jJkK]{1}</Pattern>
                    </Constraints>
                </Constrained>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>SetDirectionCalibrationSet</Identifier>
            <DisplayName>Set Direction for Calibration Set</DisplayName>
            <Description>The direction flow for Calibration succeeded to Set.</Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>String</Basic>
                    </DataType>
                    <Constraints>
                        <Pattern>^[JK]{1}</Pattern>
                    </Constraints>
                </Constrained>
            </DataType>
        </Response>
        <DefinedExecutionErrors>
            <Identifier>SerialResponseError</Identifier>
            <Identifier>SerialConnectionError</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <Command>
        <Identifier>GetDirectionCalibration</Identifier>
        <DisplayName>Get Direction Flow for Calibration</DisplayName>
        <Description>
            Get direction flow for calibration (J for CW or K for CCW).
        </Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>Channel</Identifier>
            <DisplayName>Channel</DisplayName>
            <Description>
                The channel (1-4).
            </Description>
            <DataType>
            <Constrained>
                <DataType>
                    <Basic>Integer</Basic>
                </DataType>
                <Constraints>
                    <MaximalInclusive>4</MaximalInclusive>
                    <MinimalInclusive>1</MinimalInclusive>
                </Constraints>
            </Constrained>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentDirectionCalibration</Identifier>
            <DisplayName>Current Direction for Calibration</DisplayName>
            <Description>The current direction flow for Calibration (J for CW or K for CCW).</Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>String</Basic>
                    </DataType>
                    <Constraints>
                        <Pattern>^[JK]{1}</Pattern>
                    </Constraints>
                </Constrained>
            </DataType>
        </Response>
        <DefinedExecutionErrors>
            <Identifier>SerialResponseError</Identifier>
            <Identifier>SerialConnectionError</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <Command>
        <Identifier>SetCalibrationTime</Identifier>
        <DisplayName>Set Calibration Time</DisplayName>
        <Description>
        Set the current calibration time using Time Type 2 format (unit 0.1 s, 0-35964000, or 0 to 999 hr) .
        </Description>
        <Observable>No</Observable>
        <Parameter>
                <Identifier>Channel</Identifier>
                <DisplayName>Channel</DisplayName>
                <Description>
                    The channel (1-4).
                </Description>
                <DataType>
                    <Constrained>
                        <DataType>
                            <Basic>Integer</Basic>
                        </DataType>
                        <Constraints>
                            <MaximalInclusive>4</MaximalInclusive>
                            <MinimalInclusive>1</MinimalInclusive>
                        </Constraints>
                    </Constrained>
                </DataType>
            </Parameter>
        <Parameter>
            <Identifier>Time</Identifier>
            <DisplayName>Time</DisplayName>
            <Description>
            Set the current calibration time using Time Type 2 format.
            </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                    <Constraints>
                        <MinimalInclusive>0</MinimalInclusive>
                        <MaximalInclusive>35964000</MaximalInclusive>
                    </Constraints>
                </Constrained>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>SetCalibrationTimeSet</Identifier>
            <DisplayName>Set Calibration Time Set</DisplayName>
            <Description>Calibration Time succeeded to Set.</Description>
            <DataType>
                <Basic>String</Basic>
            </DataType>
        </Response>
        <DefinedExecutionErrors>
            <Identifier>SerialResponseError</Identifier>
            <Identifier>SerialConnectionError</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <Command>
        <Identifier>GetCalibrationTime</Identifier>
        <DisplayName>Get Calibration Time</DisplayName>
        <Description>
            Get the current calibration time in 0.1 s (Time Type 1).
        </Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>Channel</Identifier>
            <DisplayName>Channel</DisplayName>
            <Description>
                The channel (1-4).
            </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                    <Constraints>
                        <MaximalInclusive>4</MaximalInclusive>
                        <MinimalInclusive>1</MinimalInclusive>
                    </Constraints>
                </Constrained>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentCalibrationTime</Identifier>
            <DisplayName>Current Calibration Time</DisplayName>
            <Description>The current calibration time in 0.1 s (Time Type 1).</Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                    <Constraints>
                        <MinimalInclusive>0</MinimalInclusive>
                        <MaximalInclusive>35964000</MaximalInclusive>
                    </Constraints>
                </Constrained>
            </DataType>
        </Response>
        <DefinedExecutionErrors>
            <Identifier>SerialResponseError</Identifier>
            <Identifier>SerialConnectionError</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <Command>
        <Identifier>GetRunTimeCalibration</Identifier>
        <DisplayName>Get Run Time since last calibration</DisplayName>
        <Description>
            Get the channel run time since last calibration in 0.1 s (Time Type 2).
        </Description>
        <Observable>No</Observable>
        <Parameter>
                <Identifier>Channel</Identifier>
                <DisplayName>Channel</DisplayName>
                <Description>
                    The channel (1-4).
                </Description>
                <DataType>
                    <Constrained>
                        <DataType>
                            <Basic>Integer</Basic>
                        </DataType>
                        <Constraints>
                            <MaximalInclusive>4</MaximalInclusive>
                            <MinimalInclusive>1</MinimalInclusive>
                        </Constraints>
                    </Constrained>
                </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentRunTimeCalibration</Identifier>
            <DisplayName>Current Run Time since Calibration</DisplayName>
            <Description>
                Channel run time since last calibration in 0.1 s (Time Type 2).
            </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                    <Constraints>
                        <MinimalInclusive>0</MinimalInclusive>
                        <MaximalInclusive>35964000</MaximalInclusive>
                    </Constraints>
                </Constrained>
            </DataType>
        </Response>
        <DefinedExecutionErrors>
            <Identifier>SerialResponseError</Identifier>
            <Identifier>SerialConnectionError</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <DefinedExecutionError>
        <Identifier>SerialResponseError</Identifier>
        <DisplayName>Serial Response Error</DisplayName>
        <Description>The serial response could not be parsed. Device errors are not defined.</Description>
    </DefinedExecutionError>
    <DefinedExecutionError>
        <Identifier>SerialConnectionError</Identifier>
        <DisplayName>Serial Connection Error</DisplayName>
        <Description>Serial connection is not available. Make sure the serial cable is connected and the right serial port used!</Description>
    </DefinedExecutionError>
</Feature>

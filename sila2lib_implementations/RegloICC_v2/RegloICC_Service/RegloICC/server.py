import logging
import sys

from sila2.server import SilaServer

from .feature_implementations.calibrationservice_impl import CalibrationServiceImpl
from .feature_implementations.configurationservice_impl import ConfigurationServiceImpl
from .feature_implementations.deviceservice_impl import DeviceServiceImpl
from .feature_implementations.drivecontroller_impl import DriveControllerImpl
from .feature_implementations.parametercontroller_impl import ParameterControllerImpl
from .generated.calibrationservice import CalibrationServiceFeature
from .generated.configurationservice import ConfigurationServiceFeature
from .generated.deviceservice import DeviceServiceFeature
from .generated.drivecontroller import DriveControllerFeature
from .generated.parametercontroller import ParameterControllerFeature

from .feature_implementations.serial_connector import SharedProperties  # serial connector
from .generated.deviceservice import SerialConnectionError


class Server(SilaServer):
    def __init__(self, device_id: str, device_command: str = '1xS\r\n', simulation_mode: bool = True):
        super().__init__(
            server_name="RegloICCService",
            server_type="Pump",
            server_version="1.0",
            server_description="This is a RegloICC Service",
            server_vendor_url="https://www.epe.ed.tum.de/biovt/startseite/",
        )
        self.properties = SharedProperties(
            device_id=device_id,
            device_command=device_command,
            simulation_mode=simulation_mode,
            baudrate=9600
        )
        logging.info(f'Server started in {"simulation" if self.properties.simulation_mode == True else "real"} mode.')
        try:
            self.properties.connect_serial()
        except (SerialConnectionError, Exception) as e:
            logging.error(e)
            self.stop()
            sys.exit(1)

        self.calibrationservice = CalibrationServiceImpl(self.properties)
        self.configurationservice = ConfigurationServiceImpl(self.properties)
        self.deviceservice = DeviceServiceImpl(self.properties)
        self.drivecontroller = DriveControllerImpl(self.properties)
        self.parametercontroller = ParameterControllerImpl(self.properties)

        self.set_feature_implementation(CalibrationServiceFeature, self.calibrationservice)
        self.set_feature_implementation(ConfigurationServiceFeature, self.configurationservice)
        self.set_feature_implementation(DeviceServiceFeature, self.deviceservice)
        self.set_feature_implementation(DriveControllerFeature, self.drivecontroller)
        self.set_feature_implementation(ParameterControllerFeature, self.parametercontroller)

import logging
from threading import Lock
from contextlib import contextmanager # used for locking

from serial import Serial, PARITY_NONE, STOPBITS_ONE, EIGHTBITS
from serial.tools import list_ports

from ..generated.deviceservice import SerialConnectionError


class SerialConnector:
    def __init__(self,
                 baudrate: int = 9600,
                 parity: str = PARITY_NONE,
                 stopbits: int = STOPBITS_ONE,
                 bytesize: int = EIGHTBITS,
                 timeout: float = 1
                 ):
        self.baudrate = baudrate
        self.parity = parity
        self.stopbits = stopbits
        self.bytesize = bytesize
        self.timeout = timeout
        self.ser = None

    def connect(self, device_id: str, id_command: str):
        port_list: list = list_ports.comports(include_links=True)
        port = None
        serial_port = None
        self.ser = None

        if port_list == []:
            raise Exception('No serial ports found on this system. Make sure your device is connected!')
        else:
            for port in port_list:
                print(port)
                if port[0] == '/dev/ttyAMA0':
                    pass
                else:
                    serial_port = Serial(port=port[0], baudrate=self.baudrate, parity=self.parity,
                                         stopbits=self.stopbits, bytesize=self.bytesize, timeout=self.timeout)
                    try:
                        serial_port.write(str.encode(id_command))
                        read = str(bytes.decode(serial_port.readline().rstrip()))
                        if read == device_id:
                            logging.info(f'Connected to device ({device_id}) on serial port {port[0]}.')
                            self.ser = serial_port
                            return self.ser
                        else:
                            serial_port.close()
                    except Exception as e:
                        raise SerialConnectionError
            if self.ser is None:
                raise Exception(f'No device found matching the id \"{device_id}\"!')


class SharedProperties:
    def __init__(self, device_id: str, device_command: str, simulation_mode: bool, baudrate: int = 19200):
        self.device_id = device_id
        self.device_command = device_command
        self.serial_connector = SerialConnector(baudrate=baudrate)
        self.simulation_mode = simulation_mode
        self.lock = Lock()

    def connect_serial(self):
        if not self.simulation_mode:
            self.serial_connector.connect(device_id=self.device_id, id_command=self.device_command)
        else:
            self.serial_connector.ser = None

    @contextmanager
    def acquire_timeout_lock(self,  timeout):
        result = self.lock.acquire(timeout=timeout)
        yield result
        if result:
            self.lock.release()


@contextmanager
def acquire_timeout_lock(lock, timeout):
    result = lock.acquire(timeout=timeout)
    yield result
    if result:
        lock.release()

serial_connector = SerialConnector()
lock = Lock()
simulation_mode = True



if __name__ == '__main__':
    serial_connector = SerialConnector()
    ser = serial_connector.connect(device_id='85 CO2_30753 O2_30700 HUM_30279 :I,BD', id_command='&i\r\n')

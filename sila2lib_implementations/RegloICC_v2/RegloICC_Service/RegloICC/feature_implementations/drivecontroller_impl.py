from __future__ import annotations

import logging
import serial
from typing import Any, Dict

from sila2.framework import FullyQualifiedIdentifier

from ..generated.drivecontroller import (
    DriveControllerBase,
    GetCauseResponse_Responses,
    GetPumpDirection_Responses,
    PausePump_Responses,
    SetDirectionClockwise_Responses,
    SetDirectionCounterClockwise_Responses,
    StartPump_Responses,
    StopPump_Responses,
)
from .serial_connector import SharedProperties
from ..generated.drivecontroller import (
    SerialConnectionError,
    SerialResponseError,
    ChannelSettingsError
)


class DriveControllerImpl(DriveControllerBase):
    def __init__(self, properties: SharedProperties):
        self.properties = properties

    def StartPump(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> StartPump_Responses:
        logging.debug(
            "StartPump called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}H\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read == '-':
                        raise ChannelSettingsError
                    elif '*' not in read:
                        print('ERROR in read', read)
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing StartPump command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Starting channel %s" % Channel)
            return StartPump_Responses(
                StartStatus=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def StopPump(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> StopPump_Responses:
        logging.debug(
            "StopPump called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}I\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing StopPump command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Stopping channel %s" % Channel)
            return StopPump_Responses(
                StopStatus=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def PausePump(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> PausePump_Responses:
        logging.debug(
            "PausePump called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}xI\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing PausePump command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Pausing channel %s" % Channel)
            return PausePump_Responses(
                PauseStatus=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetPumpDirection(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetPumpDirection_Responses:
        logging.debug(
            "GetPumpDirection called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}xD\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'J'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read not in ['J', 'K']:
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetPumpDirection command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Channel/pump %s rotation direction is : %s" % (Channel, read))
            return GetPumpDirection_Responses(
                PumpDirection=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetDirectionClockwise(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetDirectionClockwise_Responses:
        logging.debug(
            "SetDirectionClockwise called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}J\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    print('HERE Read: ', read)
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetDirectionClockwise command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Setting of clockwise rotation direction of channel %s: %s" % (Channel, read))
            return SetDirectionClockwise_Responses(
                SetDirectionSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetDirectionCounterClockwise(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetDirectionCounterClockwise_Responses:
        logging.debug(
            "SetDirectionCounterClockwise called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}K\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetDirectionCounterClockwise command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Setting of counter-clockwise rotation direction of channel %s: %s" % (Channel, read))
            return SetDirectionCounterClockwise_Responses(
                SetDirectionSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetCauseResponse(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetCauseResponse_Responses:
        logging.debug(
            "GetCauseResponse called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}xe\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: C, C'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetCauseResponse command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Cause of negative response on channel %s: %s" % (Channel, read))
            return GetCauseResponse_Responses(
                Cause=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

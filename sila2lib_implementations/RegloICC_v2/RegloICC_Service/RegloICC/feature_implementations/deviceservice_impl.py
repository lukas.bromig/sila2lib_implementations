from __future__ import annotations

import logging
import serial
from typing import Any, Dict

from sila2.framework import FullyQualifiedIdentifier

from ..generated.deviceservice import (
    GetVersionSoftware_Responses,
    DeviceServiceBase,
    GetChannelAddressing_Responses,
    GetChannelNumber_Responses,
    GetChannelTotalVolume_Responses,
    GetEventMessages_Responses,
    GetFootSwitchStatus_Responses,
    GetHeadModel_Responses,
    GetLanguage_Responses,
    GetPauseTime_Responses,
    GetPumpStatus_Responses,
    GetRevolutions_Responses,
    GetRollersNumber_Responses,
    GetRollerStepsHigh_Responses,
    GetRollerStepsLow_Responses,
    GetRSV_Responses,
    GetSerialNumber_Responses,
    GetSerialProtocol_Responses,
    GetTimeSetting_Responses,
    GetTotalTime_Responses,
    GetTotalVolume_Responses,
    GetVersionType_Responses,
    ResetRSVTable_Responses,
    SaveSetRoller_Responses,
    SaveSettings_Responses,
    SetChannelAddressing_Responses,
    SetChannelNumber_Responses,
    SetDisableInterface_Responses,
    SetDisplayLetters_Responses,
    SetDisplayNumbers_Responses,
    SetEventMessages_Responses,
    SetHeadModel_Responses,
    SetLanguage_Responses,
    SetNonFactoryRSV_Responses,
    SetPauseTime_Responses,
    SetPauseTimeH_Responses,
    SetPauseTimeM_Responses,
    SetPumpAddress_Responses,
    SetPumpName_Responses,
    SetPumpSerialNumber_Responses,
    SetRollersNumber_Responses,
    SetRollerStepsHigh_Responses,
    SetRollerStepsLow_Responses,
    SetRSV_Responses,
    SetRSVReset_Responses,
    SetRunTimeH_Responses,
    SetRunTimeM_Responses,
    SetTimeSetting_Responses,
    SetUserInterface_Responses,
)
from .serial_connector import SharedProperties
from ..generated.deviceservice import SerialConnectionError, SerialResponseError


class DeviceServiceImpl(DeviceServiceBase):
    def __init__(self, properties: SharedProperties):
        self.properties = properties

    def SetPumpAddress(
        self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetPumpAddress_Responses:
        logging.debug(
            "SetPumpAddress called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'@{Address}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetPumpAddress command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('Pump address set to %s: %s' % (Address, read))
            return SetPumpAddress_Responses(
                PumpAddressSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetLanguage(
        self, Address: int, Language: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetLanguage_Responses:
        logging.debug(
            "SetLanguage called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Address}L{Language}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetLanguage command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('Pump %s language set to %s: %s' % (Address, Language, read))
            return SetLanguage_Responses(
                LanguageSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetLanguage(self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> GetLanguage_Responses:
        logging.debug(
            "GetLanguage called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Address}L\r\n')
        try:
            if self.properties.simulation_mode:
                read = 1
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if len(read) != 1 and read.isdecimal():
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetLanguage command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('Current Language is: %s' % read)
            return GetLanguage_Responses(
                CurrentLanguage=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetPumpStatus(self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> GetPumpStatus_Responses:
        logging.debug(
            "GetPumpStatus called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Address}E\r\n')
        try:
            if self.properties.simulation_mode:
                read = '+'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '+' and read != '-':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetPumpStatus command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Current pump status is: %s  (+ running; - stopped)" % read)
            return GetPumpStatus_Responses(
                CurrentPumpStatus=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetVersionType(
        self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetVersionType_Responses:
        logging.debug(
            "GetVersionType called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Address}#\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'abc-123-456'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    # Todo: Catch unfeasible response
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetVersionType command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Pump model, software version and head model type code are: %s" % read)
            return GetVersionType_Responses(
                CurrentVersionType=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetVersionSoftware(
        self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetVersionSoftware_Responses:
        logging.debug(
            "GetVersionSoftware called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Address}(\r\n')
        try:
            if self.properties.simulation_mode:
                read = '9999'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if len(read) != 4 and not read.isdecimal():
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetVersionSoftware command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Current software version is: %s" % read)
            return GetVersionSoftware_Responses(
                CurrentVersionSoftware=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetSerialNumber(
        self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetSerialNumber_Responses:
        logging.debug(
            "GetSerialNumber called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Address}xS\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'MySimulatedSerialNumber'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if len(read) <= 0 or len(read) > 64:
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetSerialNumber command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Serial number is: %s" % read)
            return GetSerialNumber_Responses(
                SerialNumber=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetChannelAddressing(
        self, Address: int, ChannelAddressing: bool, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetChannelAddressing_Responses:
        logging.debug(
            "SetChannelAddressing called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Address}~{int(ChannelAddressing)}\r\n')
        try:
            if self.properties.simulation_mode:
                read = ChannelAddressing
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                    read = True
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetChannelAddressing command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Setting of channel Addressing to %s : %s" % (ChannelAddressing, read))
            return SetChannelAddressing_Responses(
                ChannelAddressingSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetChannelAddressing(
        self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetChannelAddressing_Responses:
        logging.debug(
            "GetChannelAddressing called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Address}~\r\n')
        try:
            if self.properties.simulation_mode:
                read = True
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if len(read) >= 0 and not read.isdecimal():
                        raise SerialResponseError
                    read = bool(int(read))
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetChannelAddressing command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            if read is False:
                meaning = 'disabled'
            else:
                meaning = 'enabled'
            logging.info(f'Channel addressing set to: {read}, {meaning}')
            return GetChannelAddressing_Responses(
                ChannelAddressing=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetEventMessages(
        self, Address: int, EventMessages: bool, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetEventMessages_Responses:
        logging.debug(
            "SetEventMessages called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Address}xE{int(EventMessages)}\r\n')
        try:
            if self.properties.simulation_mode:
                read = EventMessages
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        #   if len(read) >= 0 and not read.isdecimal():
                        raise SerialResponseError
                    else:
                        read = True
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetEventMessages command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            if read is False:
                meaning = 'disabled'
            else:
                meaning = 'enabled'
            logging.info(f'Channel addressing set to: {read}, {meaning}')
            return SetEventMessages_Responses(
                EventMessagesSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetEventMessages(
        self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetEventMessages_Responses:
        logging.debug(
            "GetEventMessages called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Address}xE\r\n')
        try:
            if self.properties.simulation_mode:
                read = True
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if len(read) >= 0 and not read.isdecimal():
                        raise SerialResponseError
                    read = bool(int(read))
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetEventMessages command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            if read is False:
                meaning = 'disabled'
            else:
                meaning = 'enabled'
            logging.debug(f"Event messages are: {meaning}")
            return GetEventMessages_Responses(
                EventMessages=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetSerialProtocol(
        self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetSerialProtocol_Responses:
        logging.debug(
            "GetSerialProtocol called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Address}x!\r\n')
        try:
            if self.properties.simulation_mode:
                read = 9999
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if len(read) > 4 or not read.isdecimal():
                        raise SerialResponseError
                    read = int(read)
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetSerialProtocol command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Integer representing the version of the serial protocol is: %s" % read)
            return GetSerialProtocol_Responses(
                SerialProtocol=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetPumpName(
        self, Address: int, PumpName: str, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetPumpName_Responses:
        logging.debug(
            "SetPumpName called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Address}xN{PumpName}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if not '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetPumpName command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('Pump name set to %s: %s' % (PumpName, read))
            return SetPumpName_Responses(
                PumpNameSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetChannelNumber(
        self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetChannelNumber_Responses:
        logging.debug(
            "GetChannelNumber called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Address}xA\r\n')
        try:
            if self.properties.simulation_mode:
                read = 4
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if len(read) <= 0 or len(read) > 4 or not read.isdecimal():
                        raise SerialResponseError
                    read = int(read)
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetChannelNumber command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Pump %s has: %s channels" % (Address, read))
            return GetChannelNumber_Responses(
                ChannelNumber=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetChannelNumber(
        self, Address: int, ChannelNumber: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetChannelNumber_Responses:
        logging.debug(
            "SetChannelNumber called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = '{:0>4}'.format(ChannelNumber)
        command = str.encode(f'{Address}xA{value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetChannelNumber command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('Channel number set to %s: %s' % (value, read))
            return SetChannelNumber_Responses(
                ChannelNumberSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetRevolutions(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetRevolutions_Responses:
        logging.debug(
            "GetRevolutions called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}xC\r\n')
        try:
            if self.properties.simulation_mode:
                read = 4294967295
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if len(read) != 10 or not read.isdecimal():
                        raise SerialResponseError
                    read = int(read)
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetRevolutions command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Total number of revolutions since last reset is: %s" % read)
            return GetRevolutions_Responses(
                Revolutions=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetChannelTotalVolume(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetChannelTotalVolume_Responses:
        logging.debug(
            "GetChannelTotalVolume called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}xG\r\n')
        try:
            if self.properties.simulation_mode:
                read = 4294967295
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if len(read) != 10 or not read.isdecimal():
                        raise SerialResponseError
                    read = int(read)
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetChannelTotalVolume command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Channel total volume pumped since last reset is: %s mL" % read)
            return GetChannelTotalVolume_Responses(
                ChannelTotalVolume=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetTotalTime(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> GetTotalTime_Responses:
        logging.debug(
            "GetTotalTime called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}xJ\r\n')
        try:
            if self.properties.simulation_mode:
                read = 9999
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    # Todo: Figure out the return type of this command and implement reasonable response validation
                    # if len(read) != 10 or not read.isdecimal():
                    #     raise SerialResponseError
                    read = int(read)
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetTotalTime command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Total time pumped for channel %s since last reset is: %s seconds" % (Channel, int(read)))
            return GetTotalTime_Responses(
                TotalTime=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetHeadModel(self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> GetHeadModel_Responses:
        logging.debug(
            "GetTotalTime called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Address})\r\n')
        try:
            if self.properties.simulation_mode:
                read = 9999
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if len(read) < 1 or len(read) > 4 or not read.isdecimal():
                        raise SerialResponseError
                    read = int(read)
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetTotalTime command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Pump head model type code for pump %s is: %s " % (Address, read))
            return GetHeadModel_Responses(
                HeadModel=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetHeadModel(
        self, Address: int, HeadModel: str, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetHeadModel_Responses:
        logging.debug(
            "SetHeadModel called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = '{:0>4}'.format(int(HeadModel))
        command = str.encode(f'{Address}){value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetHeadModel command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('Head model set to %s: %s' % (value, read))
            return SetHeadModel_Responses(
                HeadModelSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetUserInterface(
        self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetUserInterface_Responses:
        logging.debug(
            "SetUserInterface called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Address}A\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetUserInterface command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('User interface set: %s' % read)
            return SetUserInterface_Responses(
                UserInterfaceSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetDisableInterface(
        self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetDisableInterface_Responses:
        logging.debug(
            "SetDisableInterface called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Address}B\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetDisableInterface command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('User interface disabled: %s' % read)
            return SetDisableInterface_Responses(
                DisableInterfaceSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetDisplayNumbers(
        self, Address: int, DisplayNumbers: str, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetDisplayNumbers_Responses:
        logging.debug(
            "SetDisplayNumbers called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = '{:0>16}'.format(DisplayNumbers)
        command = str.encode(f'{Address}D{value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetDisplayNumbers command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('\' %s displayed \': %s' % (value, read))
            return SetDisplayNumbers_Responses(
                DisplayNumbersSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetDisplayLetters(
        self, Address: int, DisplayLetters: str, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetDisplayLetters_Responses:
        logging.debug(
            "SetDisplayLetters called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = '{:0>16}'.format(DisplayLetters)
        command = str.encode(f'{Address}DA{value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetDisplayLetters command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('\' %s displayed \': %s' % (value, read))
            return SetDisplayLetters_Responses(
                DisplayLettersSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetTimeSetting(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetTimeSetting_Responses:
        logging.debug(
            "GetTimeSetting called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}V\r\n')
        try:
            if self.properties.simulation_mode:
                read = float(9999)
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if len(read) == 0 or len(read) > 4 or not read.isdecimal():
                        raise SerialResponseError
                    read = float(read)
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetTimeSetting command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.error("Error getting current setting for pump time")
            return GetTimeSetting_Responses(
                TimeSetting=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetTimeSetting(
        self, Channel: int, TimeSetting: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetTimeSetting_Responses:
        logging.debug(
            "SetTimeSetting called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = int(float(TimeSetting)*10)            # eliminates '.'
        value = '{:0>4}'.format(value)                # formats the input to 4 digits
        command = str.encode(f'{Channel}V{value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetTimeSetting command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Setting pump time on channel %s is: %s seconds" % (Channel, read))
            return SetTimeSetting_Responses(
                TimeSettingSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetRunTimeM(
        self, Channel: int, RunTimeM: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetRunTimeM_Responses:
        logging.debug(
            "SetRunTimeM called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = '{:0>3}'.format(RunTimeM)  # formats the input to 3 digits
        command = str.encode(f'{Channel}VM{value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetRunTimeM command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Setting run time on channel %s is: %s minutes" % (Channel, read))
            return SetRunTimeM_Responses(
                RunTimeMSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetRunTimeH(
        self, Channel: int, RunTimeH: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetRunTimeH_Responses:
        logging.debug(
            "SetRunTimeH called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = '{:0>3}'.format(RunTimeH)            # formats the input to 3 digits
        command = str.encode(f'{Channel}VH{value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetRunTimeH command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Setting run time on channel %s is: %s minutes" % (Channel, read))
            return SetRunTimeH_Responses(
                RunTimeHSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetRollerStepsLow(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetRollerStepsLow_Responses:
        logging.debug(
            "GetRollerStepsLow called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}U\r\n')
        try:
            if self.properties.simulation_mode:
                read = 999999
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if len(read) != 6 or not read.isdecimal():
                        raise SerialResponseError
                    read = int(read)
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetRollerStepsLow command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Current setting for volume to pump in low order roller steps for channel %s is: %s " % (
                Channel, int(read)))
            return GetRollerStepsLow_Responses(
                RollerStepsLow=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetRollerStepsLow(
        self, Channel: int, RollerStepsLow: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetRollerStepsLow_Responses:
        logging.debug(
            "SetRollerStepsLow called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = '{:0>5}'.format(RollerStepsLow)  # formats input to 5 digits
        command = str.encode(f'{Channel}U{value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetRollerStepsLow command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Setting volume to pump in low order roller steps of %s for channel %s is: %s " % (
                value, Channel, read))
            return SetRollerStepsLow_Responses(
                RollerStepsLowSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetRollerStepsHigh(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetRollerStepsHigh_Responses:
        logging.debug(
            "GetRollerStepsHigh called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}u\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if len(read) != 6 or not read.isdecimal():
                        raise SerialResponseError
                    read = int(read)
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetRollerStepsHigh command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Current setting for volume to pump in high order roller steps for channel %s is: %s " % (
                Channel, int(read)))
            return GetRollerStepsHigh_Responses(
                RollerStepsHigh=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetRollerStepsHigh(
        self, Channel: int, RollerSteps: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetRollerStepsHigh_Responses:
        logging.debug(
            "SetRollerStepsHigh called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = '{:0>5}'.format(RollerSteps)  # formats input to 5 digits
        command = str.encode(f'{Channel}u{value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetRollerStepsHigh command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Setting volume to pump in high order roller steps of %s for channel %s is: %s " % (
                command, Channel, read))
            return SetRollerStepsHigh_Responses(
                RollerStepsHighSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetRSV(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> GetRSV_Responses:
        logging.debug(
            "GetRSV called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}r\r\n')
        try:
            if self.properties.simulation_mode:
                read = '9999E-2'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    read = float(read[:4]) * 0.001 * (
                                10 ** (float(read[5:])))  # translates the answer of the pump to 'human' number in nL
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetRSV command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug(
                "The pump's current setting for roller step volume (RSV) for channel %s is: %s nL" % (Channel, read))
            return GetRSV_Responses(
                RSV=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetRSV(self, Channel: int, RSV: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> SetRSV_Responses:
        logging.debug(
            "SetRSV called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = format(RSV, '.4E')                # formats input to scientific
        value = value[0:1]+value[2:5]+value[7:8]+value[9:]      # extracts 4 digits and +-E in pump input format
        command = str.encode(f'{Channel}r{value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetRSV command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Setting roller step volume (RSV) for channel %s to %s nL: %s" % (Channel, value, read))
            return SetRSV_Responses(
                RSVSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetRSVReset(self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> SetRSVReset_Responses:
        logging.debug(
            "SetRSVReset called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Address}000000\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetRSVReset command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Resetting the pump to discard calibration data: %s " % read)
            return SetRSVReset_Responses(
                RSVResetSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def ResetRSVTable(self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> ResetRSVTable_Responses:
        logging.debug(
            "ResetRSVTable called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Address}xu\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing ResetRSVTable command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Resetting roller step value table to default: %s " % read)
            return ResetRSVTable_Responses(
                ResetRSVTableSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetNonFactoryRSV(
        self,
        Channel: int,
        RollerCount: int,
        TubingID: str,
        NonFactoryRSV: float,
        *,
        metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetNonFactoryRSV_Responses:
        logging.debug(
            "SetNonFactoryRSV called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        allowed_tube_diameters = ['0.13', '0.19', '0.25', '0.38', '0.44', '0.51', '0.57', '0.64', '0.76', '0.89',
                                  '0.95', '1.02', '1.09', '1.14', '1.22', '1.3', '1.42', '1.52', '1.65', '1.75', '1.85',
                                  '2.06', '2.29', '2.54', '2.79', '3.17']
        if RollerCount not in [6, 8, 12]:
            logging.debug('Roller count must be 6, 8, or 12')
        if TubingID not in allowed_tube_diameters:
            logging.debug('Wrong tube ID, see manual p.16 (of 36)')

        tube = str(int(float(format(TubingID, '.2f'))*100))
        rollers = str(RollerCount)
        value = format(NonFactoryRSV, '.4E')                    # formats in scientific notation
        value = value[0:1]+value[2:5]+value[7:8]+value[9:]      # extracts 4 digits and +-E for pump input format
        command = str.encode(f'{Channel}xt{rollers[:4]}|{tube[:4]}|{value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetNonFactoryRSV command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Setting non-factory roller step volume: %s " % read)
            return SetNonFactoryRSV_Responses(
                SetNonFactoryRSVSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetPauseTime(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> GetPauseTime_Responses:
        logging.debug(
            "GetPauseTime called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}T\r\n')
        try:
            if self.properties.simulation_mode:
                read = 9999.0
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if len(read) <= 0 or len(read) > 4 or not read.isdecimal():
                        raise SerialResponseError
                    read = float(read)*0.1
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetPauseTime command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Current setting for pause time on channel %s is: %s seconds" % (Channel, read))
            return GetPauseTime_Responses(
                PauseTime=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetPauseTime(
        self, Channel: int, PauseTime: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetPauseTime_Responses:
        logging.debug(
            "SetPauseTime called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = int(float(PauseTime)*10)               # eliminates '.'
        value = '{:0>4}'.format(value)                  # formats input to 4 digits
        command = str.encode(f'{Channel}T{value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetPauseTime command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('Setting of pause time to %s s on channel %s: %s ' % (value, Channel, read))
            return SetPauseTime_Responses(
                PauseTimeSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetPauseTimeM(
        self, Channel: int, PauseTimeM: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetPauseTimeM_Responses:
        logging.debug(
            "SetPauseTimeM called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = int(float(PauseTimeM))                  # eliminates '.'
        value = '{:0>3}'.format(value)                  # formats input to 3 digits
        command = str.encode(f'{Channel}TM{value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetPauseTimeM command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('Setting of pause time to %s minutes on channel %s: %s ' % (value, Channel, read))
            return SetPauseTimeM_Responses(
                PauseTimeMSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetPauseTimeH(
        self, Channel: int, PauseTimeH: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetPauseTimeH_Responses:
        logging.debug(
            "SetPauseTimeH called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = int(float(PauseTimeH))                  # eliminates '.'
        value = '{:0>3}'.format(value)                  # formats input to 3 digits
        command = str.encode(f'{Channel}TH{value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetPauseTimeH command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('Setting of pause time to %s hours on channel %s: %s ' % (value, Channel, read))
            return SetPauseTimeH_Responses(
                PauseTimeHSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetTotalVolume(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetTotalVolume_Responses:
        logging.debug(
            "GetTotalVolume called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}:\r\n')
        try:
            if self.properties.simulation_mode:
                read = 9999.0
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    # Todo: Check implementation and add reasonable response validation
                    # if read != '*':
                    #     raise SerialResponseError
                    read = float(read)
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetTotalVolume command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Total volume dispensed on channel %s since the last reset is: %s " % (Channel, read))
            return GetTotalVolume_Responses(
                CurrentTotalVolume=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SaveSettings(self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> SaveSettings_Responses:
        logging.debug(
            "SaveSettings called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Address}*\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    # Todo: Check implementation and add reasonable response validation
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SaveSettings command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('Saving the current pump settings values to memory: %s ' % read)
            return SaveSettings_Responses(
                Save=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SaveSetRoller(self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> SaveSetRoller_Responses:
        logging.debug(
            "SaveSetRoller called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Address}xs\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    # Todo: Check implementation and add reasonable response validation
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SaveSetRoller command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Saving set roller step settings: %s " % read)
            return SaveSetRoller_Responses(
                Save=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetFootSwitchStatus(
        self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetFootSwitchStatus_Responses:
        logging.debug(
            "GetFootSwitchStatus called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Address}C\r\n')
        try:
            if self.properties.simulation_mode:
                read = '+'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    # Todo: Check implementation and add reasonable response validation
                    if read not in ['+', '-']:
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetFootSwitchStatus command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            if read is '-':
                meaning = '-, open'
            else:
                meaning = '+, grounded'
            logging.debug("Current status of the foot switch: %s " % meaning)
            return GetFootSwitchStatus_Responses(
                FootSwitchStatus=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetRollersNumber(
        self, Channel: int, RollersNumber: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetRollersNumber_Responses:
        logging.debug(
            "SetRollersNumber called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = '{:0>4}'.format(RollersNumber)             # formats input to 4 digits
        command = str.encode(f'{Channel}xB{value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    # Todo: Check implementation and add reasonable response validation
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetRollersNumber command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Setting number of rollers for channel: %s rollers on channel %s: %s"
                          % (value, Channel, read))
            return SetRollersNumber_Responses(
                RollersNumber=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetRollersNumber(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetRollersNumber_Responses:
        logging.debug(
            "GetRollersNumber called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}xB\r\n')
        try:
            if self.properties.simulation_mode:
                read = 9999
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    # Todo: Check implementation and add reasonable response validation
                    if len(read) <= 0 or len(read) > 4 or not read.isdecimal():
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetRollersNumber command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Current number of rollers on channel %s is: %s " % (Channel, read))
            return GetRollersNumber_Responses(
                RollersNumber=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetPumpSerialNumber(
        self, Address: int, PumpSerialNumber: str, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetPumpSerialNumber_Responses:
        logging.debug(
            "SetPumpSerialNumber called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Address}xS{PumpSerialNumber}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    # Todo: Check implementation and add reasonable response validation
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetPumpSerialNumber command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Setting serial number of pump %s to %s : %s" % (Address, PumpSerialNumber, read))
            return SetPumpSerialNumber_Responses(
                PumpSerialNumberSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

from __future__ import annotations

import logging
import serial
from typing import Any, Dict

from sila2.framework import FullyQualifiedIdentifier

from ..generated.parametercontroller import (
    GetCurrentRunTime_Responses,
    GetCycleCount_Responses,
    GetDispenseTimeMlMin_Responses,
    GetDispenseTimeRPM_Responses,
    GetFlowRate_Responses,
    GetFlowRateAtModes_Responses,
    GetMaximumFlowRate_Responses,
    GetMaximumFlowRateWithCalibration_Responses,
    GetMode_Responses,
    GetPumpingPauseTime_Responses,
    GetSpeedSettingRPM_Responses,
    GetVolume_Responses,
    ParameterControllerBase,
    SetCurrentRunTime_Responses,
    SetCycleCount_Responses,
    SetFlowRate_Responses,
    SetFlowRateAtModes_Responses,
    SetFlowRateMode_Responses,
    SetPumpingPauseTime_Responses,
    SetRPMMode_Responses,
    SetSpeedSettingRPM_Responses,
    SetTimeMode_Responses,
    SetTimePauseMode_Responses,
    SetVolume_Responses,
    SetVolumePauseMode_Responses,
    SetVolumeRateMode_Responses,
    SetVolumeTimeMode_Responses,
)
from .serial_connector import SharedProperties
from ..generated.parametercontroller import (
    SerialConnectionError,
    SerialResponseError,
    ChannelSettingsError
)


class ParameterControllerImpl(ParameterControllerBase):
    def __init__(self, properties: SharedProperties):
        self.properties = properties

    def SetFlowRate(
        self, Channel: int, SetFlowRate: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetFlowRate_Responses:
        logging.debug(
            "SetFlowRate called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = format(SetFlowRate, '.4E')  # formats in scientific notation
        value = value[0:1]+value[2:5]+value[7:8]+value[9:]  # extracts 4 digits and +-E for pump input format
        command = str.encode(f'{Channel}f{value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = float(9999)
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if 'E' not in read:
                        raise SerialResponseError
                    read = float(read[:4]) * 0.001 * (10 ** (float(read[5:])))
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetFlowRate command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("\nSetting of flow rate: %s mL/min on channel %s" % (read, Channel))
            return SetFlowRate_Responses(
                SetFlowRateSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetRPMMode(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> SetRPMMode_Responses:
        logging.debug(
            "SetRPMMode called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}L\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetRPMMode command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Setting of RPM mode on channel %s: %s " % (Channel, read))
            return SetRPMMode_Responses(
                PumpModeSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetFlowRateMode(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetFlowRateMode_Responses:
        logging.debug(
            "SetFlowRateMode called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}M\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetFlowRateMode command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Setting of RPM mode on channel %s: %s " % (Channel, read))
            return SetFlowRateMode_Responses(
                PumpModeSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetVolumeRateMode(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetVolumeRateMode_Responses:
        logging.debug(
            "SetVolumeRateMode called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}O\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetVolumeRateMode command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Setting of Volume at Rate mode on channel %s: %s " % (Channel, read))
            return SetVolumeRateMode_Responses(
                PumpModeSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetVolumeTimeMode(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetVolumeTimeMode_Responses:
        logging.debug(
            "SetVolumeTimeMode called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}G\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read == '-':
                        raise ChannelSettingsError
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetVolumeTimeMode command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Setting of Volume over Time mode on channel %s: %s " % (Channel, read))
            return SetVolumeTimeMode_Responses(
                PumpModeSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetVolumePauseMode(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetVolumePauseMode_Responses:
        logging.debug(
            "SetVolumePauseMode called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}Q\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetVolumePauseMode command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Setting of Volume + Pause mode on channel %s: %s " % (Channel, read))
            return SetVolumePauseMode_Responses(
                PumpModeSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetTimeMode(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> SetTimeMode_Responses:
        logging.debug(
            "SetTimeMode called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}N\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetTimeMode command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Setting of Time mode on channel %s: %s " % (Channel, read))
            return SetTimeMode_Responses(
                PumpModeSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetTimePauseMode(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
                         ) -> SetTimePauseMode_Responses:
        logging.debug(
            "SetTimePauseMode called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}P\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetTimePauseMode command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Setting of Time + Pause mode on channel %s: %s " % (Channel, read))
            return SetTimePauseMode_Responses(
                PumpModeSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetVolume(
        self, Channel: int, Volume: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetVolume_Responses:
        logging.debug(
            "SetVolume called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = format(Volume, '.4E')                      # formats in scientific notation
        value = value[0:1]+value[2:5]+value[7:8]+value[9:]        # extracts 4 digits and +-E for pump input format
        command = str.encode(f'{Channel}v{value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = '9999E-2'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    # Todo: Check if below response validation is correct
                    # if len(read) != 7:
                    #     raise SerialResponseError
                    read = str(int(read[:4]) * 0.001 * (10 ** (float(read[5:]))))
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetVolume command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("\nSetting of volume: %s mL on channel %s" % (read, Channel))
            return SetVolume_Responses(
                VolumeSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetFlowRate(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> GetFlowRate_Responses:
        logging.debug(
            "GetFlowRate called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}f\r\n')
        try:
            if self.properties.simulation_mode:
                read = 999.9
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if len(read) != 7:
                        raise SerialResponseError
                    read = float(read[:4]) * 0.001 * (
                            10 ** (float(read[5:])))  # translates answer from pump format to "human"
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetFlowRate command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Current flow rate is: %s mL/min" % read)
            return GetFlowRate_Responses(
                CurrentFlowRate=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetMode(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> GetMode_Responses:
        logging.debug(
            "GetMode called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}xM\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'L'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if len(read) != 1 or read not in ['L', 'M', 'O', 'G', 'Q', 'N', 'P']:
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetMode command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Current channel/pump mode is: %s " % read)
            return GetMode_Responses(
                CurrentPumpMode=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetVolume(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> GetVolume_Responses:
        logging.debug(
            "GetVolume called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}v\r\n')
        try:
            if self.properties.simulation_mode:
                read = 999.9
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                    read = float(read[:4]) * 0.001 * (
                                10 ** (float(read[5:])))  # translates answer from pump format to "human"
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetVolume command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Current volume setting is: %s mL" % read)
            return GetVolume_Responses(
                CurrentVolume=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetMaximumFlowRate(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetMaximumFlowRate_Responses:
        logging.debug(
            "GetMaximumFlowRate called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}?\r\n')
        try:
            if self.properties.simulation_mode:
                read = 999.9
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    print(read)
                    value, unit = read.split(' ')
                    print(value, unit)
                    if 'ml/min' not in read: # Can there be another unit?
                        raise SerialResponseError
                    read = float(value)
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetMaximumFlowRate command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Maximum flow rate is: %s" % read)
            return GetMaximumFlowRate_Responses(
                MaximumFlowRate=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetMaximumFlowRateWithCalibration(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetMaximumFlowRateWithCalibration_Responses:
        logging.debug(
            "GetMaximumFlowRateWithCalibration called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}!\r\n')
        try:
            if self.properties.simulation_mode:
                read = 999.9
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    print(read)
                    if read == '#':
                        raise SerialResponseError
                    read = read.split(' ml')[0]
                    read = float(read)
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetMaximumFlowRateWithCalibration command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Maximum flow rate with current settings using calibration is: %s" % read)
            return GetMaximumFlowRateWithCalibration_Responses(
                MaximumFlowRateWithCalibration=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetSpeedSettingRPM(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetSpeedSettingRPM_Responses:
        logging.debug(
            "GetSpeedSettingRPM called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}S\r\n')
        try:
            if self.properties.simulation_mode:
                read = 999.9
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    # Todo: Implement reasonable response validation
                    if read == '#':
                        raise SerialResponseError
                    read = float(read)
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetSpeedSettingRPM command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Current speed setting is: %s RPM" % read)
            return GetSpeedSettingRPM_Responses(
                CurrentSpeedSetting=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetSpeedSettingRPM(
        self, Channel: int, Speed: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetSpeedSettingRPM_Responses:
        logging.debug(
            "SetSpeedSettingRPM called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = int(Speed*100) # elimination of '.'
        value = '{:0>6}'.format(value)   # formats the input to 6 digits
        command = str.encode(f'{Channel}S{value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    # Todo: Implement reasonable response validation
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetSpeedSettingRPM command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Set Speed Setting in RPM on channel %s to %s RPM: %s " % (Channel, value, read))
            return SetSpeedSettingRPM_Responses(
                CurrentSpeedSetting=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetCurrentRunTime(
        self, Channel: int, RunTime: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetCurrentRunTime_Responses:
        logging.debug(
            "SetCurrentRunTime called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = int(RunTime*10)              # elimination of '.'
        value = '{:0>8}'.format(value)            # formats the input to 8 digits
        command = str.encode(f'{Channel}xT{value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    # Todo: Implement reasonable response validation
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetCurrentRunTime command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Setting of Current Run Time  on channel %s to %s seconds: %s " % (Channel, value, read))
            return SetCurrentRunTime_Responses(
                SetCurrentRunTimeSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetCurrentRunTime(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetCurrentRunTime_Responses:
        logging.debug(
            "GetCurrentRunTime called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}xT\r\n')
        try:
            if self.properties.simulation_mode:
                read = int(9999)
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if not read.isdecimal():
                        raise SerialResponseError
                    read = int(read)  # answer is in s
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetCurrentRunTime command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Current run time on channel %s is: %s seconds" % (Channel, read))
            return GetCurrentRunTime_Responses(
                CurrentRunTime=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetPumpingPauseTime(
        self, Channel: int, PumpingPauseTime: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetPumpingPauseTime_Responses:
        logging.debug(
            "SetPumpingPauseTime called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = int(PumpingPauseTime*10)        # elimination of '.'
        value = '{:0>8}'.format(value)          # formats the input to 8 digits
        command = str.encode(f'{Channel}xP{value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    # Todo: Implement reasonable response validation
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetPumpingPauseTime command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Setting of Pumping Pause Time  on channel %s to %s seconds: %s " % (Channel, value, read))
            return SetPumpingPauseTime_Responses(
                SetPumpingPauseTimeSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetPumpingPauseTime(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetPumpingPauseTime_Responses:
        logging.debug(
            "SetPumpingPauseTime called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}xP\r\n')
        try:
            if self.properties.simulation_mode:
                read = 35964000
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    # Todo: Implement reasonable response validation
                    if len(read) < 1 or len(read) > 8 or not read.isdecimal():
                        raise SerialResponseError
                    read = float(read) * 0.1  # answer is in 0.1 s
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetPumpingPauseTime command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Current pause time on channel %s is: %s seconds" % (Channel, read))
            return GetPumpingPauseTime_Responses(
                CurrentPumpingPauseTime=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetCycleCount(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> GetCycleCount_Responses:
        logging.debug(
            "GetCycleCount called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}"\r\n')
        try:
            if self.properties.simulation_mode:
                read = 9999
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    # Todo: Implement reasonable response validation
                    if len(read) < 1 or len(read) > 4 or not read.isdecimal():
                        raise SerialResponseError
                    read = float(read) * 0.1    # answer is in 0.1s
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetCycleCount command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Current cycle count on channel %s is: %s " % (Channel, read))
            return GetCycleCount_Responses(
                CurrentCycleCount=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetCycleCount(
        self, Channel: int, CycleCount: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetCycleCount_Responses:
        logging.debug(
            "SetCycleCount called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = '{:0>4}'.format(CycleCount)        # formats the input to 4 digits
        command = str.encode(f'{Channel}"{value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    # Todo: Implement reasonable response validation
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetCycleCount command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Current cycle count on channel %s is: %s " % (Channel, read))
            return SetCycleCount_Responses(
                SetCycleCountSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetDispenseTimeMlMin(
        self, Channel: int, Volume: float, FlowRate: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetDispenseTimeMlMin_Responses:
        logging.debug(
            "GetDispenseTimeMlMin called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        volume_user = format(Volume, '.3E')
        flow_rate_user = format(FlowRate, '.3E')

        volume = volume_user[0:1] + volume_user[2:5] + volume_user[6:7] + volume_user[8:9]
        # extracts 4 digits and +-E into the pump input format
        flow_rate = flow_rate_user[0:1] + flow_rate_user[2:5] + flow_rate_user[6:7] + flow_rate_user[8:]
        # extracts 4 digits and +-E into the pump input format

        command = str.encode(f'{Channel}xv{volume}|{flow_rate}\r\n')
        try:
            if self.properties.simulation_mode:
                read = float(35964000)
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    # Todo: Implement reasonable response validation
                    if len(read) < 1 or len(read) > 8 or not read.isdecimal():
                        raise SerialResponseError
                    read = float(read) * 0.1  # answer in 0.1s
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetDispenseTimeMlMin command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Current time to dispense %s mL at rate of %s mL/min on channel %s is: %s seconds" % (
                volume, flow_rate, Channel, read))
            return GetDispenseTimeMlMin_Responses(
                CurrentDispenseTime=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetDispenseTimeRPM(
        self, Channel: int, Volume: float, FlowRate: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetDispenseTimeRPM_Responses:
        logging.debug(
            "GetDispenseTimeRPM called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        volume_user = format(Volume, '.3E')

        volume = volume_user[0:1]+volume_user[2:5]+volume_user[6:7]+volume_user[8:9]
        # extracts 4 digits and +-E into the pump input format
        flow_rate = '{:0>6}'.format(FlowRate)   # extracts 6 digits into the pump input format
        command = str.encode(f'{Channel}xw{volume}|{flow_rate}\r\n')
        try:
            if self.properties.simulation_mode:
                read = float(35964000)
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    # Todo: Implement reasonable response validation
                    if len(read) < 1 or len(read) > 8 or not read.isdecimal():
                        raise SerialResponseError
                    read = float(read) * 0.1  # answer in 0.1s
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetDispenseTimeRPM command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Current time to dispense is %s mL at rate of %s RPM on channel %s: %s seconds" % (
                volume_user, flow_rate, Channel, read))
            return GetDispenseTimeRPM_Responses(
                CurrentDispenseTime=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetFlowRateAtModes(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetFlowRateAtModes_Responses:
        logging.debug(
            "GetFlowRateAtModes called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}xf\r\n')
        try:
            if self.properties.simulation_mode:
                read = False
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    # Todo: Implement reasonable response validation
                    if len(read) < 1 or len(read) > 1 or not read.isdecimal():
                        raise SerialResponseError
                    read = bool(int(read))
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetFlowRateAtModes command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            if read is False:
                meaning = 'RPM'
            else:
                meaning = 'Flow Rate'
            logging.debug("Current flow rate is from: %s mode " % meaning)
            return GetFlowRateAtModes_Responses(
                CurrentFlowRate=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetFlowRateAtModes(
        self, Channel: int, FlowRate: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetFlowRateAtModes_Responses:
        logging.debug(
            "SetFlowRateAtModes called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        print(FlowRate)
        value = '{:0>6}'.format(FlowRate)                   # format is the input to 6 digits
        print(value)
        command = str.encode(f'{Channel}xf{value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    # Todo: Implement reasonable response validation
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetFlowRateAtModes command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Set RPM Flow Rate not in RPM or flow rate mode to %s: %s" % (FlowRate, read))
            return SetFlowRateAtModes_Responses(
                SetFlowRateSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

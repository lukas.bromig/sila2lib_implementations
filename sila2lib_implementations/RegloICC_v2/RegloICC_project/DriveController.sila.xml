<?xml version="1.0" encoding="utf-8" ?>
<Feature SiLA2Version="1.0" FeatureVersion="1.0" Originator="org.silastandard" Category="examples"
         xmlns="http://www.sila-standard.org"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://www.sila-standard.org https://gitlab.com/SiLA2/sila_features/raw/master/schema/FeatureDefinition.xsd">
    <Identifier>DriveController</Identifier>
    <DisplayName>Drive Controller</DisplayName>
    <Description>
		Set and retrieve information regarding the pump drive.
        By Valeryia Sidarava, Institute of Biochemical Engineering, Technical University of Munich, 31.07.2019,
        Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 15.04.2020.
        Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 28.01.2022.
    </Description>
    <Command>
        <Identifier>StartPump</Identifier>
        <DisplayName>Start Pump</DisplayName>
        <Description>
          Starts the pump out of stand-by mode.
        </Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>Channel</Identifier>
            <DisplayName>Channel</DisplayName>
            <Description>
                The channel (1-4).
            </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                    <Constraints>
                        <MaximalInclusive>4</MaximalInclusive>
                        <MinimalInclusive>1</MinimalInclusive>
                    </Constraints>
                </Constrained>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>StartStatus</Identifier>
            <DisplayName>Start Status</DisplayName>
            <Description>Start of pump succeeded.</Description>
            <DataType>
                <Basic>String</Basic>
            </DataType>
        </Response>
        <DefinedExecutionErrors>
            <Identifier>SerialResponseError</Identifier>
            <Identifier>SerialConnectionError</Identifier>
            <Identifier>ChannelSettingsError</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <Command>
        <Identifier>StopPump</Identifier>
        <DisplayName>StopPump</DisplayName>
        <Description>
            Stops the pump.
        </Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>Channel</Identifier>
            <DisplayName>Channel</DisplayName>
            <Description>
                The channel (1-4).
            </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                    <Constraints>
                        <MaximalInclusive>4</MaximalInclusive>
                        <MinimalInclusive>1</MinimalInclusive>
                    </Constraints>
                </Constrained>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>StopStatus</Identifier>
            <DisplayName>Stop Status</DisplayName>
            <Description>Shut-down of pump succeeded.</Description>
            <DataType>
                <Basic>String</Basic>
            </DataType>
        </Response>
        <DefinedExecutionErrors>
            <Identifier>SerialResponseError</Identifier>
            <Identifier>SerialConnectionError</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <Command>
        <Identifier>PausePump</Identifier>
        <DisplayName>PausePump</DisplayName>
        <Description>
            Pause pumping (STOP in RPM or flow rate mode).
        </Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>Channel</Identifier>
            <DisplayName>Channel</DisplayName>
            <Description>
                The channel (1-4).
            </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                    <Constraints>
                        <MaximalInclusive>4</MaximalInclusive>
                        <MinimalInclusive>1</MinimalInclusive>
                    </Constraints>
                </Constrained>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>PauseStatus</Identifier>
            <DisplayName>Pause Status</DisplayName>
            <Description>Pausing of pump succeeded.</Description>
            <DataType>
                <Basic>String</Basic>
            </DataType>
        </Response>
        <DefinedExecutionErrors>
            <Identifier>SerialResponseError</Identifier>
            <Identifier>SerialConnectionError</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <Command>
        <Identifier>GetPumpDirection</Identifier>
        <DisplayName>Get Pump Direction</DisplayName>
        <Description>Get pump direction (J for CW or K for CCW). </Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>Channel</Identifier>
            <DisplayName>Channel</DisplayName>
            <Description>
                The channel (1-4).
            </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                    <Constraints>
                        <MaximalInclusive>4</MaximalInclusive>
                        <MinimalInclusive>1</MinimalInclusive>
                    </Constraints>
                </Constrained>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>PumpDirection</Identifier>
            <DisplayName>Pump Direction</DisplayName>
            <Description>Pump direction query. J = clockwise/ K = counter-clockwise. </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>String</Basic>
                    </DataType>
                    <Constraints>
                        <Pattern>^[JK]{1}</Pattern>
                    </Constraints>
                </Constrained>
            </DataType>
        </Response>
        <DefinedExecutionErrors>
            <Identifier>SerialResponseError</Identifier>
            <Identifier>SerialConnectionError</Identifier>
        </DefinedExecutionErrors>
    </Command>
	<Command>
        <Identifier>SetDirectionClockwise</Identifier>
        <DisplayName>Set Direction to clockwise</DisplayName>
        <Description>Set the rotation direction of the pump to clockwise</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>Channel</Identifier>
            <DisplayName>Channel</DisplayName>
            <Description>
                The channel (1-4).
            </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                    <Constraints>
                        <MaximalInclusive>4</MaximalInclusive>
                        <MinimalInclusive>1</MinimalInclusive>
                    </Constraints>
                </Constrained>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>SetDirectionSet</Identifier>
            <DisplayName>Set Direction Set</DisplayName>
            <Description>Set Direction succeeded to Set.</Description>
            <DataType>
                <Basic>String</Basic>
            </DataType>
        </Response>
        <DefinedExecutionErrors>
            <Identifier>SerialResponseError</Identifier>
            <Identifier>SerialConnectionError</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <Command>
        <Identifier>SetDirectionCounterClockwise</Identifier>
        <DisplayName>Set Direction to counter-clockwise</DisplayName>
        <Description>Set the rotation direction of the pump to counter-clockwise</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>Channel</Identifier>
            <DisplayName>Channel</DisplayName>
            <Description>
                The channel (1-4).
            </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                    <Constraints>
                        <MaximalInclusive>4</MaximalInclusive>
                        <MinimalInclusive>1</MinimalInclusive>
                    </Constraints>
                </Constrained>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>SetDirectionSet</Identifier>
            <DisplayName>Set Direction Set</DisplayName>
            <Description>Set Direction succeeded to Set.</Description>
            <DataType>
                <Basic>String</Basic>
            </DataType>
        </Response>
        <DefinedExecutionErrors>
            <Identifier>SerialResponseError</Identifier>
            <Identifier>SerialConnectionError</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <Command>
        <Identifier>GetCauseResponse</Identifier>
        <DisplayName>Get Cause Response</DisplayName>
        <Description>Get cause of "-" cannot run response = Parameter 1 :
                      C=Cycle count of 0 / R=Max flow rate exceeded or flow rate is set to 0 / V=Max volume exceeded;
                      Limiting value that was exceeded = Parameter 2:
                    C=Value is undefined / R=Max flow (mL/min) / V=Max volume (mL)   .</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>Channel</Identifier>
            <DisplayName>Channel</DisplayName>
            <Description>
                The channel (1-4).
            </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Integer</Basic>
                    </DataType>
                    <Constraints>
                        <MaximalInclusive>4</MaximalInclusive>
                        <MinimalInclusive>1</MinimalInclusive>
                    </Constraints>
                </Constrained>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>Cause</Identifier>
            <DisplayName>Cause</DisplayName>
            <Description>Cause displayed.</Description>
            <DataType>
                <Basic>String</Basic>
            </DataType>
        </Response>
        <DefinedExecutionErrors>
            <Identifier>SerialResponseError</Identifier>
            <Identifier>SerialConnectionError</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <DefinedExecutionError>
        <Identifier>SerialResponseError</Identifier>
        <DisplayName>Serial Response Error</DisplayName>
        <Description>The serial response could not be parsed. Device errors are not defined.</Description>
    </DefinedExecutionError>
    <DefinedExecutionError>
        <Identifier>SerialConnectionError</Identifier>
        <DisplayName>Serial Connection Error</DisplayName>
        <Description>Serial connection is not available. Make sure the serial cable is connected and the right serial port used!</Description>
    </DefinedExecutionError>
    <DefinedExecutionError>
        <Identifier>ChannelSettingsError</Identifier>
        <DisplayName>Channel Settings Error</DisplayName>
        <Description>The channel setting(s) are not correct or unachievable</Description>
    </DefinedExecutionError>
</Feature>

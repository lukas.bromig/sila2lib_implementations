# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: DeviceServicer.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


import sila2lib.framework.SiLAFramework_pb2 as SiLAFramework__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='DeviceServicer.proto',
  package='sila2.org.silastandard.examples.deviceservicer.v1',
  syntax='proto3',
  serialized_options=None,
  serialized_pb=_b('\n\x14\x44\x65viceServicer.proto\x12\x31sila2.org.silastandard.examples.deviceservicer.v1\x1a\x13SiLAFramework.proto\"\x13\n\x11GetLog_Parameters\"\xc7\x01\n\x10GetLog_Responses\x12\x38\n\x0f\x43urrentLogLevel\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\x12>\n\x13\x43urrentLogTimestamp\x18\x02 \x01(\x0b\x32!.sila2.org.silastandard.Timestamp\x12\x39\n\x11\x43urrentLogMessage\x18\x03 \x01(\x0b\x32\x1e.sila2.org.silastandard.String\"\x18\n\x16GetSensorID_Parameters\"\x88\x02\n\x15GetSensorID_Responses\x12/\n\x06UnitID\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\x12.\n\x05\x43O2ID\x18\x02 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\x12-\n\x04O2ID\x18\x03 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\x12.\n\x05HUMID\x18\x04 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\x12/\n\x06PRESID\x18\x05 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"\x1a\n\x18GetSensorInfo_Parameters\"M\n\x17GetSensorInfo_Responses\x12\x32\n\nSensorInfo\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.String\"$\n\"Subscribe_CurrentStatus_Parameters\"Z\n!Subscribe_CurrentStatus_Responses\x12\x35\n\rCurrentStatus\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.String2\x9f\x07\n\x0e\x44\x65viceServicer\x12}\n\x06GetLog\x12\x44.sila2.org.silastandard.examples.deviceservicer.v1.GetLog_Parameters\x1a+.sila2.org.silastandard.CommandConfirmation\"\x00\x12\x66\n\x0bGetLog_Info\x12,.sila2.org.silastandard.CommandExecutionUUID\x1a%.sila2.org.silastandard.ExecutionInfo\"\x00\x30\x01\x12\x84\x01\n\rGetLog_Result\x12,.sila2.org.silastandard.CommandExecutionUUID\x1a\x43.sila2.org.silastandard.examples.deviceservicer.v1.GetLog_Responses\"\x00\x12\xa4\x01\n\x0bGetSensorID\x12I.sila2.org.silastandard.examples.deviceservicer.v1.GetSensorID_Parameters\x1aH.sila2.org.silastandard.examples.deviceservicer.v1.GetSensorID_Responses\"\x00\x12\xaa\x01\n\rGetSensorInfo\x12K.sila2.org.silastandard.examples.deviceservicer.v1.GetSensorInfo_Parameters\x1aJ.sila2.org.silastandard.examples.deviceservicer.v1.GetSensorInfo_Responses\"\x00\x12\xca\x01\n\x17Subscribe_CurrentStatus\x12U.sila2.org.silastandard.examples.deviceservicer.v1.Subscribe_CurrentStatus_Parameters\x1aT.sila2.org.silastandard.examples.deviceservicer.v1.Subscribe_CurrentStatus_Responses\"\x00\x30\x01\x62\x06proto3')
  ,
  dependencies=[SiLAFramework__pb2.DESCRIPTOR,])




_GETLOG_PARAMETERS = _descriptor.Descriptor(
  name='GetLog_Parameters',
  full_name='sila2.org.silastandard.examples.deviceservicer.v1.GetLog_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=96,
  serialized_end=115,
)


_GETLOG_RESPONSES = _descriptor.Descriptor(
  name='GetLog_Responses',
  full_name='sila2.org.silastandard.examples.deviceservicer.v1.GetLog_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentLogLevel', full_name='sila2.org.silastandard.examples.deviceservicer.v1.GetLog_Responses.CurrentLogLevel', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='CurrentLogTimestamp', full_name='sila2.org.silastandard.examples.deviceservicer.v1.GetLog_Responses.CurrentLogTimestamp', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='CurrentLogMessage', full_name='sila2.org.silastandard.examples.deviceservicer.v1.GetLog_Responses.CurrentLogMessage', index=2,
      number=3, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=118,
  serialized_end=317,
)


_GETSENSORID_PARAMETERS = _descriptor.Descriptor(
  name='GetSensorID_Parameters',
  full_name='sila2.org.silastandard.examples.deviceservicer.v1.GetSensorID_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=319,
  serialized_end=343,
)


_GETSENSORID_RESPONSES = _descriptor.Descriptor(
  name='GetSensorID_Responses',
  full_name='sila2.org.silastandard.examples.deviceservicer.v1.GetSensorID_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='UnitID', full_name='sila2.org.silastandard.examples.deviceservicer.v1.GetSensorID_Responses.UnitID', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='CO2ID', full_name='sila2.org.silastandard.examples.deviceservicer.v1.GetSensorID_Responses.CO2ID', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='O2ID', full_name='sila2.org.silastandard.examples.deviceservicer.v1.GetSensorID_Responses.O2ID', index=2,
      number=3, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='HUMID', full_name='sila2.org.silastandard.examples.deviceservicer.v1.GetSensorID_Responses.HUMID', index=3,
      number=4, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PRESID', full_name='sila2.org.silastandard.examples.deviceservicer.v1.GetSensorID_Responses.PRESID', index=4,
      number=5, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=346,
  serialized_end=610,
)


_GETSENSORINFO_PARAMETERS = _descriptor.Descriptor(
  name='GetSensorInfo_Parameters',
  full_name='sila2.org.silastandard.examples.deviceservicer.v1.GetSensorInfo_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=612,
  serialized_end=638,
)


_GETSENSORINFO_RESPONSES = _descriptor.Descriptor(
  name='GetSensorInfo_Responses',
  full_name='sila2.org.silastandard.examples.deviceservicer.v1.GetSensorInfo_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='SensorInfo', full_name='sila2.org.silastandard.examples.deviceservicer.v1.GetSensorInfo_Responses.SensorInfo', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=640,
  serialized_end=717,
)


_SUBSCRIBE_CURRENTSTATUS_PARAMETERS = _descriptor.Descriptor(
  name='Subscribe_CurrentStatus_Parameters',
  full_name='sila2.org.silastandard.examples.deviceservicer.v1.Subscribe_CurrentStatus_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=719,
  serialized_end=755,
)


_SUBSCRIBE_CURRENTSTATUS_RESPONSES = _descriptor.Descriptor(
  name='Subscribe_CurrentStatus_Responses',
  full_name='sila2.org.silastandard.examples.deviceservicer.v1.Subscribe_CurrentStatus_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentStatus', full_name='sila2.org.silastandard.examples.deviceservicer.v1.Subscribe_CurrentStatus_Responses.CurrentStatus', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=757,
  serialized_end=847,
)

_GETLOG_RESPONSES.fields_by_name['CurrentLogLevel'].message_type = SiLAFramework__pb2._INTEGER
_GETLOG_RESPONSES.fields_by_name['CurrentLogTimestamp'].message_type = SiLAFramework__pb2._TIMESTAMP
_GETLOG_RESPONSES.fields_by_name['CurrentLogMessage'].message_type = SiLAFramework__pb2._STRING
_GETSENSORID_RESPONSES.fields_by_name['UnitID'].message_type = SiLAFramework__pb2._INTEGER
_GETSENSORID_RESPONSES.fields_by_name['CO2ID'].message_type = SiLAFramework__pb2._INTEGER
_GETSENSORID_RESPONSES.fields_by_name['O2ID'].message_type = SiLAFramework__pb2._INTEGER
_GETSENSORID_RESPONSES.fields_by_name['HUMID'].message_type = SiLAFramework__pb2._INTEGER
_GETSENSORID_RESPONSES.fields_by_name['PRESID'].message_type = SiLAFramework__pb2._INTEGER
_GETSENSORINFO_RESPONSES.fields_by_name['SensorInfo'].message_type = SiLAFramework__pb2._STRING
_SUBSCRIBE_CURRENTSTATUS_RESPONSES.fields_by_name['CurrentStatus'].message_type = SiLAFramework__pb2._STRING
DESCRIPTOR.message_types_by_name['GetLog_Parameters'] = _GETLOG_PARAMETERS
DESCRIPTOR.message_types_by_name['GetLog_Responses'] = _GETLOG_RESPONSES
DESCRIPTOR.message_types_by_name['GetSensorID_Parameters'] = _GETSENSORID_PARAMETERS
DESCRIPTOR.message_types_by_name['GetSensorID_Responses'] = _GETSENSORID_RESPONSES
DESCRIPTOR.message_types_by_name['GetSensorInfo_Parameters'] = _GETSENSORINFO_PARAMETERS
DESCRIPTOR.message_types_by_name['GetSensorInfo_Responses'] = _GETSENSORINFO_RESPONSES
DESCRIPTOR.message_types_by_name['Subscribe_CurrentStatus_Parameters'] = _SUBSCRIBE_CURRENTSTATUS_PARAMETERS
DESCRIPTOR.message_types_by_name['Subscribe_CurrentStatus_Responses'] = _SUBSCRIBE_CURRENTSTATUS_RESPONSES
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

GetLog_Parameters = _reflection.GeneratedProtocolMessageType('GetLog_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GETLOG_PARAMETERS,
  '__module__' : 'DeviceServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.deviceservicer.v1.GetLog_Parameters)
  })
_sym_db.RegisterMessage(GetLog_Parameters)

GetLog_Responses = _reflection.GeneratedProtocolMessageType('GetLog_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GETLOG_RESPONSES,
  '__module__' : 'DeviceServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.deviceservicer.v1.GetLog_Responses)
  })
_sym_db.RegisterMessage(GetLog_Responses)

GetSensorID_Parameters = _reflection.GeneratedProtocolMessageType('GetSensorID_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GETSENSORID_PARAMETERS,
  '__module__' : 'DeviceServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.deviceservicer.v1.GetSensorID_Parameters)
  })
_sym_db.RegisterMessage(GetSensorID_Parameters)

GetSensorID_Responses = _reflection.GeneratedProtocolMessageType('GetSensorID_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GETSENSORID_RESPONSES,
  '__module__' : 'DeviceServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.deviceservicer.v1.GetSensorID_Responses)
  })
_sym_db.RegisterMessage(GetSensorID_Responses)

GetSensorInfo_Parameters = _reflection.GeneratedProtocolMessageType('GetSensorInfo_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GETSENSORINFO_PARAMETERS,
  '__module__' : 'DeviceServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.deviceservicer.v1.GetSensorInfo_Parameters)
  })
_sym_db.RegisterMessage(GetSensorInfo_Parameters)

GetSensorInfo_Responses = _reflection.GeneratedProtocolMessageType('GetSensorInfo_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GETSENSORINFO_RESPONSES,
  '__module__' : 'DeviceServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.deviceservicer.v1.GetSensorInfo_Responses)
  })
_sym_db.RegisterMessage(GetSensorInfo_Responses)

Subscribe_CurrentStatus_Parameters = _reflection.GeneratedProtocolMessageType('Subscribe_CurrentStatus_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _SUBSCRIBE_CURRENTSTATUS_PARAMETERS,
  '__module__' : 'DeviceServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.deviceservicer.v1.Subscribe_CurrentStatus_Parameters)
  })
_sym_db.RegisterMessage(Subscribe_CurrentStatus_Parameters)

Subscribe_CurrentStatus_Responses = _reflection.GeneratedProtocolMessageType('Subscribe_CurrentStatus_Responses', (_message.Message,), {
  'DESCRIPTOR' : _SUBSCRIBE_CURRENTSTATUS_RESPONSES,
  '__module__' : 'DeviceServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.deviceservicer.v1.Subscribe_CurrentStatus_Responses)
  })
_sym_db.RegisterMessage(Subscribe_CurrentStatus_Responses)



_DEVICESERVICER = _descriptor.ServiceDescriptor(
  name='DeviceServicer',
  full_name='sila2.org.silastandard.examples.deviceservicer.v1.DeviceServicer',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  serialized_start=850,
  serialized_end=1777,
  methods=[
  _descriptor.MethodDescriptor(
    name='GetLog',
    full_name='sila2.org.silastandard.examples.deviceservicer.v1.DeviceServicer.GetLog',
    index=0,
    containing_service=None,
    input_type=_GETLOG_PARAMETERS,
    output_type=SiLAFramework__pb2._COMMANDCONFIRMATION,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='GetLog_Info',
    full_name='sila2.org.silastandard.examples.deviceservicer.v1.DeviceServicer.GetLog_Info',
    index=1,
    containing_service=None,
    input_type=SiLAFramework__pb2._COMMANDEXECUTIONUUID,
    output_type=SiLAFramework__pb2._EXECUTIONINFO,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='GetLog_Result',
    full_name='sila2.org.silastandard.examples.deviceservicer.v1.DeviceServicer.GetLog_Result',
    index=2,
    containing_service=None,
    input_type=SiLAFramework__pb2._COMMANDEXECUTIONUUID,
    output_type=_GETLOG_RESPONSES,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='GetSensorID',
    full_name='sila2.org.silastandard.examples.deviceservicer.v1.DeviceServicer.GetSensorID',
    index=3,
    containing_service=None,
    input_type=_GETSENSORID_PARAMETERS,
    output_type=_GETSENSORID_RESPONSES,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='GetSensorInfo',
    full_name='sila2.org.silastandard.examples.deviceservicer.v1.DeviceServicer.GetSensorInfo',
    index=4,
    containing_service=None,
    input_type=_GETSENSORINFO_PARAMETERS,
    output_type=_GETSENSORINFO_RESPONSES,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='Subscribe_CurrentStatus',
    full_name='sila2.org.silastandard.examples.deviceservicer.v1.DeviceServicer.Subscribe_CurrentStatus',
    index=5,
    containing_service=None,
    input_type=_SUBSCRIBE_CURRENTSTATUS_PARAMETERS,
    output_type=_SUBSCRIBE_CURRENTSTATUS_RESPONSES,
    serialized_options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_DEVICESERVICER)

DESCRIPTOR.services_by_name['DeviceServicer'] = _DEVICESERVICER

# @@protoc_insertion_point(module_scope)

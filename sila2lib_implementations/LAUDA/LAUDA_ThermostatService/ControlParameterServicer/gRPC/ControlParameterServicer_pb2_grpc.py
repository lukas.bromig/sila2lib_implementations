# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

from . import ControlParameterServicer_pb2 as ControlParameterServicer__pb2


class ControlParameterServicerStub(object):
  """Feature: Control Parameter Servicer

  Set and retrieve the control parameter values of the in-built PID-controller of the LAUDA L250
  Thermostat.
  By Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 20.05.2019

  """

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.SetControlParamXp = channel.unary_unary(
        '/sila2.biovt.mw.tum.de.examples.controlparameterservicer.v1.ControlParameterServicer/SetControlParamXp',
        request_serializer=ControlParameterServicer__pb2.SetControlParamXp_Parameters.SerializeToString,
        response_deserializer=ControlParameterServicer__pb2.SetControlParamXp_Responses.FromString,
        )
    self.SetControlParamTn = channel.unary_unary(
        '/sila2.biovt.mw.tum.de.examples.controlparameterservicer.v1.ControlParameterServicer/SetControlParamTn',
        request_serializer=ControlParameterServicer__pb2.SetControlParamTn_Parameters.SerializeToString,
        response_deserializer=ControlParameterServicer__pb2.SetControlParamTn_Responses.FromString,
        )
    self.SetControlParamTv = channel.unary_unary(
        '/sila2.biovt.mw.tum.de.examples.controlparameterservicer.v1.ControlParameterServicer/SetControlParamTv',
        request_serializer=ControlParameterServicer__pb2.SetControlParamTv_Parameters.SerializeToString,
        response_deserializer=ControlParameterServicer__pb2.SetControlParamTv_Responses.FromString,
        )
    self.SetControlParamTd = channel.unary_unary(
        '/sila2.biovt.mw.tum.de.examples.controlparameterservicer.v1.ControlParameterServicer/SetControlParamTd',
        request_serializer=ControlParameterServicer__pb2.SetControlParamTd_Parameters.SerializeToString,
        response_deserializer=ControlParameterServicer__pb2.SetControlParamTd_Responses.FromString,
        )
    self.GetControlParamXp = channel.unary_unary(
        '/sila2.biovt.mw.tum.de.examples.controlparameterservicer.v1.ControlParameterServicer/GetControlParamXp',
        request_serializer=ControlParameterServicer__pb2.GetControlParamXp_Parameters.SerializeToString,
        response_deserializer=ControlParameterServicer__pb2.GetControlParamXp_Responses.FromString,
        )
    self.GetControlParamTn = channel.unary_unary(
        '/sila2.biovt.mw.tum.de.examples.controlparameterservicer.v1.ControlParameterServicer/GetControlParamTn',
        request_serializer=ControlParameterServicer__pb2.GetControlParamTn_Parameters.SerializeToString,
        response_deserializer=ControlParameterServicer__pb2.GetControlParamTn_Responses.FromString,
        )
    self.GetControlParamTv = channel.unary_unary(
        '/sila2.biovt.mw.tum.de.examples.controlparameterservicer.v1.ControlParameterServicer/GetControlParamTv',
        request_serializer=ControlParameterServicer__pb2.GetControlParamTv_Parameters.SerializeToString,
        response_deserializer=ControlParameterServicer__pb2.GetControlParamTv_Responses.FromString,
        )
    self.GetControlParamTd = channel.unary_unary(
        '/sila2.biovt.mw.tum.de.examples.controlparameterservicer.v1.ControlParameterServicer/GetControlParamTd',
        request_serializer=ControlParameterServicer__pb2.GetControlParamTd_Parameters.SerializeToString,
        response_deserializer=ControlParameterServicer__pb2.GetControlParamTd_Responses.FromString,
        )


class ControlParameterServicerServicer(object):
  """Feature: Control Parameter Servicer

  Set and retrieve the control parameter values of the in-built PID-controller of the LAUDA L250
  Thermostat.
  By Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 20.05.2019

  """

  def SetControlParamXp(self, request, context):
    """Set Control Parameter Xp Value

    Set the control parameter Xp of the PID controller in Kelvin, K.

    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def SetControlParamTn(self, request, context):
    """Set Control Parameter Tn Value

    Set the control parameter Tn of the PID controller in seconds, s.

    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def SetControlParamTv(self, request, context):
    """Set Control Parameter Tv Value

    Set the control parameter Tv of the PID controller in seconds, s.

    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def SetControlParamTd(self, request, context):
    """Set Control Parameter Td Value

    Set the control parameter Td of the PID controller in seconds, s.

    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetControlParamXp(self, request, context):
    """Get Control Param Xp
    Get control parameter of the proportional factor, Xp, of the PID-controller in K.
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetControlParamTn(self, request, context):
    """Get Control Param Tn
    Get control parameter of the integral factor, Tn, of the PID-controller in s.
    A value of Tn=181 is equivalent to "off".
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetControlParamTv(self, request, context):
    """Get Control Param Tv
    Get control parameter of the derivative factor, Tv, of the PID-controller in s.
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetControlParamTd(self, request, context):
    """Get Control Param Td
    Get control parameter dampening factor, Td, of the derivative factor, Tv, of the PID-controller in s.
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_ControlParameterServicerServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'SetControlParamXp': grpc.unary_unary_rpc_method_handler(
          servicer.SetControlParamXp,
          request_deserializer=ControlParameterServicer__pb2.SetControlParamXp_Parameters.FromString,
          response_serializer=ControlParameterServicer__pb2.SetControlParamXp_Responses.SerializeToString,
      ),
      'SetControlParamTn': grpc.unary_unary_rpc_method_handler(
          servicer.SetControlParamTn,
          request_deserializer=ControlParameterServicer__pb2.SetControlParamTn_Parameters.FromString,
          response_serializer=ControlParameterServicer__pb2.SetControlParamTn_Responses.SerializeToString,
      ),
      'SetControlParamTv': grpc.unary_unary_rpc_method_handler(
          servicer.SetControlParamTv,
          request_deserializer=ControlParameterServicer__pb2.SetControlParamTv_Parameters.FromString,
          response_serializer=ControlParameterServicer__pb2.SetControlParamTv_Responses.SerializeToString,
      ),
      'SetControlParamTd': grpc.unary_unary_rpc_method_handler(
          servicer.SetControlParamTd,
          request_deserializer=ControlParameterServicer__pb2.SetControlParamTd_Parameters.FromString,
          response_serializer=ControlParameterServicer__pb2.SetControlParamTd_Responses.SerializeToString,
      ),
      'GetControlParamXp': grpc.unary_unary_rpc_method_handler(
          servicer.GetControlParamXp,
          request_deserializer=ControlParameterServicer__pb2.GetControlParamXp_Parameters.FromString,
          response_serializer=ControlParameterServicer__pb2.GetControlParamXp_Responses.SerializeToString,
      ),
      'GetControlParamTn': grpc.unary_unary_rpc_method_handler(
          servicer.GetControlParamTn,
          request_deserializer=ControlParameterServicer__pb2.GetControlParamTn_Parameters.FromString,
          response_serializer=ControlParameterServicer__pb2.GetControlParamTn_Responses.SerializeToString,
      ),
      'GetControlParamTv': grpc.unary_unary_rpc_method_handler(
          servicer.GetControlParamTv,
          request_deserializer=ControlParameterServicer__pb2.GetControlParamTv_Parameters.FromString,
          response_serializer=ControlParameterServicer__pb2.GetControlParamTv_Responses.SerializeToString,
      ),
      'GetControlParamTd': grpc.unary_unary_rpc_method_handler(
          servicer.GetControlParamTd,
          request_deserializer=ControlParameterServicer__pb2.GetControlParamTd_Parameters.FromString,
          response_serializer=ControlParameterServicer__pb2.GetControlParamTd_Responses.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'sila2.biovt.mw.tum.de.examples.controlparameterservicer.v1.ControlParameterServicer', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))

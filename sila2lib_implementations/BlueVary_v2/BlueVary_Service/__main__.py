import logging
from argparse import ArgumentParser

from .server import Server
from .feature_implementations import serial_connector


def parse_args():
    parser = ArgumentParser(prog='BlueVary_Service', description="Start this SiLA 2 server")
    parser.add_argument("-a", "--ip-address", default="127.0.0.1", help="The IP address (default: '127.0.0.1')")
    parser.add_argument("-p", "--port", type=int, default=50052, help="The port (default: 50052)")
    parser.add_argument("--disable-discovery", action="store_true", help="Disable SiLA Server Discovery")
    parser.add_argument("-s", "--simulation-mode", action="store_true", help="Specify if you want to start in simulation mode.")
    parser.add_argument("-u", "--unit-id", type=str, default="",
                        help="Specify the unit that shall be connected to this server.")
    return parser.parse_args()


def start_server(ip_address: str, port: int, enable_discovery: bool, simulation_mode: bool, unit_id: str):
    if unit_id == '':
        logging.warning(f'Default unit-id ("") is used. You should specify a BlueVary unit!')
    server = Server(device_id=unit_id, simulation_mode=simulation_mode)
    try:
        server.start_insecure(ip_address, port, enable_discovery=enable_discovery)

        try:
            input("Press Enter to stop the server")
        except KeyboardInterrupt:
            pass
    finally:
        server.stop()


if __name__ == "__main__":
    # logging.basicConfig(format='%(levelname)-8s| %(module)s.%(funcName)s: %(message)s', level=logging.DEBUG)
    logging.basicConfig(format='%(asctime)s - %(levelname)s- %(module)s - %(message)s', level=logging.INFO)
    args = parse_args()
    start_server(args.ip_address, args.port, not args.disable_discovery, args.simulation_mode, args.unit_id)

import logging
import sys
from .generated.deviceservicer import DeviceServicerFeature
from .feature_implementations.deviceservicer_impl import DeviceServicerImpl
from .generated.deviceservicer import SerialConnectionError

from .generated.sensorservicer import SensorServicerFeature
from .feature_implementations.sensorservicer_impl import SensorServicerImpl

from .generated.calibrationservicer import CalibrationServicerFeature
from .feature_implementations.calibrationservicer_impl import CalibrationServicerImpl
from .feature_implementations.serial_connector import SharedProperties  # serial_connector

from sila2.server import SilaServer


class Server(SilaServer):
    def __init__(self, device_id: str, device_command: str = '&i\r\n', simulation_mode: bool = False):

        super().__init__(
            server_name="BlueVary-Service",
            server_type="OffgasAnalytics",
            server_version="0.1",
            server_description="A SiLA 2 Server for a off-gas analytics unit from BlueSens. "
                               "Created by Lukas Bromig at the Institute of Biochemical Engineering, "
                               "TU Munich, 21.12.2021",
            server_vendor_url="https://www.epe.ed.tum.de/biovt/startseite/",
        )
        self.properties = SharedProperties(
            device_id=device_id,
            device_command=device_command,
            simulation_mode=simulation_mode
        )
        logging.info(f'Server started in {"simulation" if self.properties.simulation_mode == True else "real"} mode.')
        try:
            self.properties.connect_serial()
        except (SerialConnectionError, Exception) as e:
            logging.error(e)
            self.stop()
            sys.exit(1)

        self.deviceservicer = DeviceServicerImpl(self.properties, self.child_task_executor)
        self.calibrationservicer = CalibrationServicerImpl(self.properties)
        self.sensorservicer = SensorServicerImpl(self.properties)

        self.set_feature_implementation(DeviceServicerFeature, self.deviceservicer)
        self.set_feature_implementation(CalibrationServicerFeature, self.calibrationservicer)
        self.set_feature_implementation(SensorServicerFeature, self.sensorservicer)

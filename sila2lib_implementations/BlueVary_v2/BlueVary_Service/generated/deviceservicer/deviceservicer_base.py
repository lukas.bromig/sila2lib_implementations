from __future__ import annotations

from abc import ABC
from sila2.server import FeatureImplementationBase

from abc import abstractmethod

from typing import Any, Dict
from sila2.framework import FullyQualifiedIdentifier

from queue import Queue

from .deviceservicer_types import GetSensorID_Responses

from .deviceservicer_types import GetSensorInfo_Responses


class DeviceServicerBase(FeatureImplementationBase, ABC):

    _CurrentStatus_producer_queue: Queue[str]

    def __init__(self):
        """
        General device settings of the BlueVary Off-Gas analysis device can be retrieved within this feature. 
        By Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 21.12.2021
        """
        self._CurrentStatus_producer_queue = Queue()

    def update_CurrentStatus(self, CurrentStatus: str):
        """
        Get the current status of the device from the internal state machine of the SiLA server.
        This method updates the observable property 'CurrentStatus'.
        """
        self._CurrentStatus_producer_queue.put(CurrentStatus)

    def CurrentStatus_on_subscription(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> None:
        """
        Get the current status of the device from the internal state machine of the SiLA server.
        This method is called when a client subscribes to the observable property 'CurrentStatus'
        :param metadata: The SiLA Client Metadata attached to the call
        :return:
        """
        pass

    @abstractmethod
    def GetSensorID(
        self,
        *,
        metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetSensorID_Responses:
        """
        Get the sensor IDs of the gas1, gas2, pressure and humidity sensor.
        :param metadata: The SiLA Client Metadata attached to the call
        :return:
            - UnitID: The unit ID of the sensor.
            - CO2ID: The sensor ID of the CO2 sensor.
            - O2ID: The sensor ID of the O2 sensor.
            - HUMID: The sensor ID of the humidity sensor.
            - PRESID: The sensor ID of the pressure sensor.
        """
        pass

    @abstractmethod
    def GetSensorInfo(
        self,
        *,
        metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetSensorInfo_Responses:
        """
        Get all relevant sensor information of the device.
        :param metadata: The SiLA Client Metadata attached to the call
        :return:
            - SensorInfo: Outputs a detailed string with sensor information.
        """
        pass

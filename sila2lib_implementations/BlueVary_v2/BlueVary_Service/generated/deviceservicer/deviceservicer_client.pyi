from __future__ import annotations

from sila2.client import ClientObservableProperty

from typing import Any, Dict, Optional, Union
from sila2.client import ClientMetadata

from .deviceservicer_types import GetSensorID_Responses

from .deviceservicer_types import GetSensorInfo_Responses


class DeviceServicerClient:
    """
        General device settings of the BlueVary Off-Gas analysis device can be retrieved within this feature. 
        By Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 21.12.2021
    """

    CurrentStatus: ClientObservableProperty[str]
    """
    Get the current status of the device from the internal state machine of the SiLA server.
    """

    def GetSensorID(
            self,
            *,
            metadata: Optional[Dict[Union[ClientMetadata, str], Any]] = None
    ) -> GetSensorID_Responses:
        """
        Get the sensor IDs of the gas1, gas2, pressure and humidity sensor.
        """
        ...
    
    def GetSensorInfo(
            self,
            *,
            metadata: Optional[Dict[Union[ClientMetadata, str], Any]] = None
    ) -> GetSensorInfo_Responses:
        """
        Get all relevant sensor information of the device.
        """
        ...

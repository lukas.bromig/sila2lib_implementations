from os.path import dirname, join

from sila2.framework import Feature

DeviceServicerFeature = Feature(open(join(dirname(__file__), "DeviceServicer.sila.xml")).read())

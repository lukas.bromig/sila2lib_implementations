from __future__ import annotations

from typing import NamedTuple


class GetSensorID_Responses(NamedTuple):
    UnitID: int
    """
    The unit ID of the sensor.
    """

    CO2ID: int
    """
    The sensor ID of the CO2 sensor.
    """

    O2ID: int
    """
    The sensor ID of the O2 sensor.
    """
    
    HUMID: int
    """
    The sensor ID of the humidity sensor.
    """
    
    PRESID: int
    """
    The sensor ID of the pressure sensor.
    """
    

class GetSensorInfo_Responses(NamedTuple):
    SensorInfo: str
    """
    Outputs a detailed string with sensor information.
    """

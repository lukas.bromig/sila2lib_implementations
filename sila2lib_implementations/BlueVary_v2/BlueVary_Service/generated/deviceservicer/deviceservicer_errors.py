from __future__ import annotations

from typing import Optional

from sila2.framework.errors.defined_execution_error import DefinedExecutionError
from .deviceservicer_feature import DeviceServicerFeature


class DeviceNotReadyError(DefinedExecutionError):
    def __init__(self, message: Optional[str] = None):
        if message is None:
            message = 'Device is not ready yet. No calibration or measurements can not be performed during the warm-up\n            phase. Device warm up can take up to 30 min.'
        super().__init__(DeviceServicerFeature.defined_execution_errors["DeviceNotReadyError"], message=message)


class SerialConnectionError(DefinedExecutionError):
    def __init__(self, message: Optional[str] = None):
        if message is None:
            message = 'Serial connection is not available. Make sure the serial cable is connected and the right serial port used!'
        super().__init__(DeviceServicerFeature.defined_execution_errors["SerialConnectionError"], message=message)

from __future__ import annotations

from typing import Any, Dict, Optional, Union
from sila2.client import ClientMetadata


from .sensorservicer_types import GetResults_Responses

from .sensorservicer_types import GetHumidity_Responses


class SensorServicerClient:
    """
		Read out the off-gas analytics sensor data for gas1 and gas2.Gas concentrations [vol.-%], pressure [mbar], humidity [%], temperature[C] and absolute humidity [vol.-%] are provided. 
        By Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 21.12.2021
    """

    def GetResults(
            self,
            *,
            metadata: Optional[Dict[Union[ClientMetadata, str], Any]] = None
    ) -> GetResults_Responses:
        """
        Get the current gas concentrations for CO2 and O2 in [vol.-%] and the pressure readings in [mbar].
        """
        ...
    
    def GetHumidity(
            self,
            *,
            metadata: Optional[Dict[Union[ClientMetadata, str], Any]] = None
    ) -> GetHumidity_Responses:
        """
        Get the current humidity [%], temperature [C] and absolute humidity [vol.-%].
        """
        ...

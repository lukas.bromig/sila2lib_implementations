from __future__ import annotations

from abc import ABC
from sila2.server import FeatureImplementationBase

from abc import abstractmethod

from typing import Any, Dict
from sila2.framework import FullyQualifiedIdentifier

from .sensorservicer_types import GetResults_Responses

from .sensorservicer_types import GetHumidity_Responses


class SensorServicerBase(FeatureImplementationBase, ABC):
    """
    Read out the off-gas analytics sensor data for gas1 and gas2.Gas concentrations [vol.-%], pressure [mbar], humidity [%], temperature[C] and absolute humidity [vol.-%] are provided.
    By Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 21.12.2021
    """

    @abstractmethod
    def GetResults(
        self,
        *,
        metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetResults_Responses:
        """
        Get the current gas concentrations for CO2 and O2 in [vol.-%] and the pressure readings in [mbar].
        :param metadata: The SiLA Client Metadata attached to the call
        :return:
            - CO2: Get the current CO2 concentration
            - O2: Get the current O2 concentration
            - Pressure: Get the current air pressure reading
        """
        pass

    @abstractmethod
    def GetHumidity(
        self,
        *,
        metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetHumidity_Responses:
        """
        Get the current humidity [%], temperature [C] and absolute humidity [vol.-%].
        :param metadata: The SiLA Client Metadata attached to the call
        :return:
            - Humidity: Get the current humidity in [%]
            - Temperature: Get the current temperature in [C]
            - AbsoluteHumidity: Get the current absolute humidity in [vol.-%]
        """
        pass

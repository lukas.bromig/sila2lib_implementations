@ lukas.bromig@tum.de, Institute of Biochemical Engineering, 22.12.2021
Raspberry Pi IP: '10.152.248.209'  
___________________________________  
  
Install the package from the BlueVary_v2 directory where the setup.py file is located:
$ pip3 install [--editable] .
The "--editable" flag can be used to synchronize changes between the installed version and the working repository.
This is a convenience function for developers.

Start a server:  
Change to correct directory (./sila2lib_implementations/BlueVary_v2)  
$ cd ~/Desktop/SiLA/sila2lib_implementations/sila2lib_implementations/BlueVary_v2 $  

In real mode:  
$python3 -m BlueVary_Service -a "10.152.248.209" -p 50052 -u "85 CO2_30753 O2_30700 HUM_30279 :I,BD" 

For sim mode, add the -s flag:  
$python3 -m BlueVary_Service -a "10.152.248.209" -p 50052 -u "85 CO2_30753 O2_30700 HUM_30279 :I,BD" -s  

Control Script
--------------------

The Control Script is used for data acquisition with multiple BlueVary units.
It can be started from the folder ./BlueVary_v2/BlueVary_Service/ with the follwoing command:
$ python3 ControlScript.py
An interactive dialogue is started in which the required BlueVary units can be 
specified. The units are identified by their unique ID. The IDs of the respective 
units are hard-coded for the setup in the DigInBio-Lab. The dictionary with the IDs
can be found in the BlueVary_Library in ./BlueVary_v2/BlueVary_Service/feature_implementations/
together with all other functions used by the Control Script.

The Control Script automatically starts the servers for the selected units.
Available BlueVary devices (Device IDs) are:
Unit 1:
    ID: '85 CO2_30753 O2_30700 HUM_30279 :I,BD'
    Port: 50001
Unit 2: 
    ID: '86 CO2_30752 O2_30703 HUM_30590 :I,BC'
    Port: 50002
Unit 3: 
    ID: '87 CO2_30754 O2_30702 HUM_30591 :I,BF' 
    Port: 50003
Unit 4:
    ID: '88 CO2_30773 O2_30701 HUM_30587 :I,C5'
    Port: 50004

The Log-output of the Servers can be adjusted in the __main__.py file in 
./BlueVary_v2/BlueVary_Service/

The data is stored in a .csv-file in the Service folder. Log statements are not saved.

If no BlueVary devices are available, this script can be executed in a 
simulation mode by setting the simulation parameter in the Control Script to True.

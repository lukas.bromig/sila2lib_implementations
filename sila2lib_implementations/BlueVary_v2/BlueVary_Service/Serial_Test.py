import logging
from serial import Serial, PARITY_NONE, STOPBITS_ONE, EIGHTBITS
from serial.tools import list_ports


class SerialConnector:
    def __init__(self,
                 baudrate: int = 19200,
                 parity: str = PARITY_NONE,
                 stopbits: int = STOPBITS_ONE,
                 bytesize: int = EIGHTBITS,
                 timeout: float = 3
                 ):
        self.baudrate = baudrate
        self.parity = parity
        self.stopbits = stopbits
        self.bytesize = bytesize
        self.timeout = timeout
        self.ser = None

    def connect(self, device_id: str, id_command: str):
        port_list: list = list_ports.comports(include_links=True)
        port = None
        serial_port = None
        self.ser = None

        if port_list == []:
            raise Exception('No serial ports found on this system. Make sure your device is connected!')
        else:
            for port in port_list:
                serial_port = Serial(port=port[0], baudrate=self.baudrate, parity=self.parity,
                                     stopbits=self.stopbits, bytesize=self.bytesize, timeout=self.timeout)
                try:
                    serial_port.write(str.encode(id_command))
                    read = str(bytes.decode(serial_port.readline().rstrip()))
                    if read == device_id:
                        logging.info(f'Connected to device ({device_id}) on serial port {port[0]}.')
                        self.ser = serial_port
                        return self.ser
                    else:
                        serial_port.close()
                except Exception as e:
                    raise Exception('SerialError')
            if self.ser is None:
                raise Exception(f'No device found matching the id \"{device_id}\"!')
            
serial_connector = SerialConnector()

if __name__ == '__main__':
    ser = serial_connector.connect(device_id='85 CO2_30753 O2_30700 HUM_30279 :I,BD', id_command='&i\r\n')  # 85 CO2_30753 O2_30700 HUM_30279 :I,BD
    ser.write(str.encode('&e\r\n'))
    read = str(bytes.decode(ser.readline().rstrip()))
    print(read)
    print(len(read))

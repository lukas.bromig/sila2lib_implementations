from setuptools import find_packages, setup

setup(
    name='BlueVary_Service',
    packages=find_packages(),
    install_requires=["sila2", "pyserial"],
    include_package_data=True,
)
